﻿<#+
[XmlType("opcodes")]
public class Root
{
    public Root()
    {
        OpCodes = new List<OpCode>();
    }

    [XmlElement("opcode")]
    public List<OpCode> OpCodes { get; private set; }
}

[XmlType("opcode")]
public class OpCode
{
    private string _Documentation;

    static string[] ReservedWords = new[]
    {
        "throw",
        "break",
        "sizeof",
        "typeof",
        "readonly",
        "switch",
        "volatile",
    };

    public OpCode()
    {
        Parameters = new List<Parameter>();
    }

    public string Name
    {
        get
        {
            string result = OpCodeName.ToLower();
            if (ReservedWords.Contains(result))
                result += "_";
            return result;
        }
    }

    [XmlAttribute("name")]
    public string OpCodeName { get; set; }

    [XmlElement("documentation")]
    public string Documentation
    {
        get
        {
            return _Documentation;
        }

        set
        {
            _Documentation = (value ?? string.Empty).Trim();
        }
    }

    [XmlElement("parameter")]
    public List<Parameter> Parameters { get; private set; }
}

[XmlType("parameter")]
public class Parameter
{
    private string _Name;
    private string _Documentation;

    [XmlAttribute("type")]
    public string Type { get; set; }

    [XmlAttribute("name")]
    public string Name
    {
        get
        {
            if (_Name != null)
                return _Name;

            switch (Type)
            {
                case "":
                case "method":
                case "type":
                case "index":
                case "field":
                case "ctor":
                    return Type;

                case "label":
                    return "target";

                case "local":
                    return "variable";

                case "labels":
                    return "targets";

                case "int":
                case "long":
                case "byte":
                case "single":
                case "double":
                case "string":
                    return "value";

                default:
                    throw new NotImplementedException(Type + " parameter type is not implemented");
            }
        }

        set
        {
            if (value.ToLower() != value)
                throw new InvalidOperationException("opcode method names must be lowercase: " + value);
            _Name = value;
        }
    }

    public string ParameterType
    {
        get
        {
            switch (Type)
            {
                case "":
                    return "";

                case "ctor":
                    return "ConstructorInfo";

                case "method":
                    return "MethodInfo";

                case "field":
                    return "FieldInfo";

                case "local":
                    return "LocalBuilder";

                case "label":
                    return "Label";

                case "labels":
                    return "Label[]";

                case "type":
                    return "Type";

                case "index":
                    return "short";

                case "single":
                    return "float";

                case "int":
                case "long":
                case "byte":
                case "double":
                case "string":
                    return Type;

                default:
                    throw new NotImplementedException(Type + " parameter type is not implemented");
            }
        }
    }

    public string ClassName
    {
        get
        {
            switch (Type)
            {
                case "":
                    return "ILInstruction";

                case "method":
                    return "ILInstructionWithMethod";

                case "field":
                    return "ILInstructionWithField";

                case "local":
                    return "ILInstructionWithLocal";

                case "label":
                    return "ILInstructionWithLabel";

                case "labels":
                    return "ILInstructionWithLabels";

                case "type":
                    return "ILInstructionWithType";

                case "index":
                    return "ILInstructionWithIndex";

                case "int":
                    return "ILInstructionWithInt32";

                case "byte":
                    return "ILInstructionWithByte";

                case "long":
                    return "ILInstructionWithInt64";

                case "single":
                    return "ILInstructionWithSingle";

                case "double":
                    return "ILInstructionWithDouble";

                case "string":
                    return "ILInstructionWithString";

                case "ctor":
                    return "ILInstructionWithConstructor";

                default:
                    throw new NotImplementedException(Type + " parameter type is not implemented");
            }
        }
    }

    public bool CanBeNull
    {
        get
        {
            switch (Type)
            {
                case "":
                case "index":
                case "label":
                case "int":
                case "long":
                case "byte":
                case "single":
                case "double":
                case "string":
                    return false;

                case "method":
                case "type":
                case "field":
                case "local":
                case "ctor":
                case "labels":
                    return true;

                default:
                    throw new NotImplementedException(Type + " parameter type is not implemented");
            }
        }
    }

    [XmlAttribute("constraint")]
    public string Constraint { get; set; }

    [XmlElement("documentation")]
    public string Documentation
    {
        get
        {
            if (_Documentation != null)
                return _Documentation;

            switch (Type)
            {
                case "":
                case "label":
                    return "The <see cref=\"Label\"/> to branch to.";

                case "labels":
                    return "The array of <see cref=\"Label\"/>s to branch to.";

                case "int":
                case "byte":
                case "long":
                case "single":
                case "double":
                case "string":
                    return "The value to load onto the stack.";

                default:
                    return "The <see cref=\"" + ParameterType + "\" /> to emit along with the opcode.";
            }
        }

        set
        {
            _Documentation = value;
        }
    }

    [XmlElement("emitcall")]
    public bool EmitCall { get; set; }
}
#>