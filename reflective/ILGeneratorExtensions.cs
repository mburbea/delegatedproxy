﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;

namespace Reflective
{
    /// <summary>
    /// This class holds extension methods for <see cref="ILGenerator"/>.
    /// </summary>
    public partial class ILGeneratorExtensions
    {
        /// <summary>
        /// <see cref="OpCodes.Call"/>: Call the method described by <paramref name="method"/>.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Call"/> instruction to.
        /// </param>
        /// <param name="method">
        /// The <see cref="MethodInfo"/> of the method to call.
        /// </param>
        /// <param name="optionalParameterTypes">
        /// An array of <see cref="Type"/> objects, for specifying the parameter list to use for a varargs-call. Leave
        /// <c>null</c> or empty to use a non-varargs call.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="generator"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.call.aspx"/> for more information.
        /// </remarks>
        public static ILGenerator call(this ILGenerator generator, MethodInfo method, params Type[] optionalParameterTypes)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (method == null)
                throw new ArgumentNullException("method");
           generator.EmitCall(OpCodes.Call, method, optionalParameterTypes);

            return generator;
        }

        /// <summary>
        /// <see cref="OpCodes.Calli"/>: Calls the method indicated on the evaluation stack (as a pointer to an entry point) with arguments described by a calling convention.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="unmanagedCallConv">
        /// The calling convention to use.
        /// </param>
        /// <param name="returnType">
        /// The <see cref="Type"/> object for the return value from the method.
        /// </param>
        /// <param name="parameterTypes">
        /// An array of <see cref="Type"/> objects for the arguments to pass to the method.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="returnType"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="parameterTypes"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.calli.aspx"/> for more information.
        /// </remarks>
        public static ILGenerator calli(this ILGenerator generator, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (returnType == null)
                throw new ArgumentNullException("returnType");
            if (parameterTypes == null)
                throw new ArgumentNullException("parameterTypes");

            generator.EmitCalli(OpCodes.Calli, unmanagedCallConv, returnType, parameterTypes);

            return generator;
        }

        /// <summary>
        /// <see cref="OpCodes.Calli"/>: Calls the method indicated on the evaluation stack (as a pointer to an entry point) with arguments described by a calling convention.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="callingConvention">
        /// The calling convention to use.
        /// </param>
        /// <param name="returnType">
        /// The <see cref="Type"/> object for the return value from the method.
        /// </param>
        /// <param name="parameterTypes">
        /// An array of <see cref="Type"/> objects for the arguments to pass to the method.
        /// </param>
        /// <param name="optionalParameterTypes">
        /// An array of <see cref="Type"/> objects for the varargs support.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="returnType"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="parameterTypes"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="optionalParameterTypes"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.calli.aspx"/> for more information.
        /// </remarks>
        public static ILGenerator calli(this ILGenerator generator, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (returnType == null)
                throw new ArgumentNullException("returnType");
            if (parameterTypes == null)
                throw new ArgumentNullException("parameterTypes");
            if (optionalParameterTypes == null)
                throw new ArgumentNullException("optionalParameterTypes");

            generator.EmitCalli(OpCodes.Calli, callingConvention, returnType, parameterTypes, optionalParameterTypes);

            return generator;
        }

        /// <summary>
        /// <see cref="OpCodes.Callvirt"/>: Calls a method <paramref name="method"/> associated with an object.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Callvirt"/> instruction to.
        /// </param>
        /// <param name="method">
        /// The <see cref="MethodInfo"/> of the virtual method to call.
        /// </param>
        /// <param name="optionalParameterTypes">
        /// An array of <see cref="Type"/> objects, for specifying the parameter list to use for a varargs-call. Leave
        /// <c>null</c> or empty to use a non-varargs call.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="generator"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.callvirt.aspx"/> for more information.
        /// </remarks>
        public static ILGenerator callvirt(this ILGenerator generator, MethodInfo method, params Type[] optionalParameterTypes)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (method == null)
                throw new ArgumentNullException("method");
			if(optionalParameterTypes == null || optionalParameterTypes.Length == 0)
			{
				generator.Emit(OpCodes.Callvirt, method);
			}
			else
			{
				generator.EmitCall(OpCodes.Callvirt, method, optionalParameterTypes);
			}
        	return generator;
        }

        /// <summary>
        /// Pushes the integer value of <paramref name="value"/> onto the evaluation stack as an int32, using the smallest instruction possible.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_0"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The <see cref="Int32"/> value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="generator"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator ldc_i4_opt(this ILGenerator generator, int value)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

            switch (value)
            {
                case -1:
                    return generator.ldc_i4_m1();

                case 0:
                    return generator.ldc_i4_0();

                case 1:
                    return generator.ldc_i4_1();

                case 2:
                    return generator.ldc_i4_2();

                case 3:
                    return generator.ldc_i4_3();

                case 4:
                    return generator.ldc_i4_4();

                case 5:
                    return generator.ldc_i4_5();

                case 6:
                    return generator.ldc_i4_6();

                case 7:
                    return generator.ldc_i4_7();

                case 8:
                    return generator.ldc_i4_8();
            }
			// values larger than 127 are treated as negative normally.
			// so we have to use the full 5 bytes.
            if (value >= sbyte.MinValue && value <= sbyte.MaxValue) 
                return generator.ldc_i4_s(unchecked((byte)value));

            return generator.ldc_i4(value);
        }
		private static MethodInfo GetFieldFromHandle = typeof(FieldInfo).GetMethod("GetFieldFromHandle", BindingFlags.Public | BindingFlags.Static, null, new[] { typeof(RuntimeFieldHandle) }, null);
		private static MethodInfo GetFieldFromHandleWithType = typeof(FieldInfo).GetMethod("GetFieldFromHandle", BindingFlags.Public | BindingFlags.Static, null, new[] { typeof(RuntimeFieldHandle), typeof(RuntimeTypeHandle) }, null);
		private static MethodInfo GetTypeFromHandle = typeof(Type).GetMethod("GetTypeFromHandle", BindingFlags.Public | BindingFlags.Static);
		private static MethodInfo GetMethodFromHandle = typeof(MethodBase).GetMethod("GetMethodFromHandle", BindingFlags.Public | BindingFlags.Static,null,new []{typeof(RuntimeMethodHandle)},null);
		private static MethodInfo GetMethodFromHandleWithType = typeof(MethodBase).GetMethod("GetMethodFromHandle", BindingFlags.Public | BindingFlags.Static, null, new[] { typeof(RuntimeMethodHandle), typeof(RuntimeTypeHandle) }, null);
		private static MethodInfo GetGenericMethodDefinition = typeof(MethodInfo).GetMethod("GetGenericMethodDefinition", BindingFlags.Instance | BindingFlags.Public, null, Type.EmptyTypes, null);
        /// <summary>
        /// Loads the specified constant onto the evaluation stack using the correct
        /// Ldc_X opcode. See remarks for supported types.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="value">
        /// The value to load onto the evaluation stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// <para><paramref name="value"/> contains a value of an unsupported type.</para>
        /// </exception>
        /// <remarks>
        /// The supported types are <see cref="Byte"/>, <see cref="SByte"/>, <see cref="UInt16"/>, <see cref="Int16"/>,
        /// <see cref="UInt32"/>, <see cref="Int32"/>, <see cref="UInt64"/>, <see cref="Int64"/>, <see cref="Single"/>,
        /// <see cref="Double"/>, <see cref="Boolean"/>,<see cref="Char"/> nullable versions of all those, and <c>null</c>,
        /// as well as <see cref="String"/> and <see cref="Decimal"/>
        /// </remarks>
		public static ILGenerator ldc<T>(this ILGenerator generator, T value)
        {
        	if (generator == null)
        		throw new ArgumentNullException("generator");

			if (!typeof(T).IsValueType)
			{
				Type type;
				MethodInfo method;
				ConstructorInfo ctor;
				FieldInfo field;
				if (object.ReferenceEquals(value, null))
					return generator.ldnull();

				if ((type=value as Type)!=null)
					return generator
						.ldtoken( type)
						.call(GetTypeFromHandle);
				if((ctor=value as ConstructorInfo)!=null)
					return generator
						.ldtoken( ctor)
						.call(GetMethodFromHandle)
						.castclass<ConstructorInfo>();
				
				if ((method=value as MethodInfo)!=null)
					return generator
						.ldtoken(method)
						.EmitIf(method.DeclaringType.IsGenericType,
							g => g
							 .ldtoken(method.DeclaringType)
							 .call(GetMethodFromHandleWithType),
							g => g.call(GetMethodFromHandle))
						.castclass<MethodInfo>()
						.EmitIf(method.IsGenericMethodDefinition, g => g.callvirt(GetGenericMethodDefinition));
					 
				if ((field = value as FieldInfo) != null)
					return generator
						.ldtoken(field)
						.EmitIf(field.DeclaringType.IsGenericType,
							g => g
							 .ldtoken(field.DeclaringType)
							 .call(GetFieldFromHandleWithType),
							g=> g.call(GetFieldFromHandle));

				if (typeof(T) == typeof(string))
					return generator.ldstr(value.ToString());
			}
			else
			{
				Type underlying = (typeof (T).IsGenericType && typeof (T).GetGenericTypeDefinition() == typeof (Nullable<>))
				                  	? typeof (T).GetGenericArguments()[0]
				                  	: typeof (T);
				bool isNullable = underlying != typeof (T);

				if (!isNullable || !EqualityComparer<T>.Default.Equals(value, default(T)))
				{
					TypeCode code = Type.GetTypeCode(underlying);
					switch (code)
					{
						case TypeCode.Single:
							return generator.ldc_r4(Convert.ToSingle(value));
						case TypeCode.Double:
							return generator.ldc_r8(Convert.ToDouble(value));
						case TypeCode.Decimal:
							var bits = Decimal.GetBits(Convert.ToDecimal(value));
							return generator
								.ldc_i4_opt(bits[0])
								.ldc_i4_opt(bits[1])
								.ldc_i4_opt(bits[2])
								.ldc_i4_opt(Math.Sign(bits[3] & 0x80000000))
								.ldc_i4_opt(bits[3] >> 16)
								.newobj<Decimal>(typeof(int), typeof(int), typeof(int), typeof(byte), typeof(byte))
								.ret();
						case TypeCode.Boolean:
						case TypeCode.Char:
						case TypeCode.SByte:
						case TypeCode.Byte:
						case TypeCode.Int16:
						case TypeCode.Int32:
						case TypeCode.Int64:
						case TypeCode.UInt16:
						case TypeCode.UInt32:
						case TypeCode.UInt64:

						{
							ulong? ulongValue = code == TypeCode.UInt64 ? Convert.ToUInt64(value) : default(ulong?);
							long longValue = ulongValue.HasValue ? unchecked((long)ulongValue.Value) : Convert.ToInt64(value);
							if (longValue >= int.MinValue && longValue <= uint.MaxValue)
							{
								generator.ldc_i4_opt(unchecked((int)longValue));
								if (code == TypeCode.Int64) // to avoid stack corruption.
								{
									return longValue > int.MaxValue && longValue <= uint.MaxValue
											? generator.conv_u8()
											: generator.conv_i8();
								}
								if (code == TypeCode.UInt64)
								{
									return generator.conv_u8();
								}
								return generator;
							}
							return generator.ldc_i8(longValue);
						}

					}
				}
				else
				{
					// should we allow emitting Nullable<T>?

				}
			}
        	throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture,
												  "Unsupported type '{0}' passed to method ldc", typeof(T)));
        }

    	/// <summary>
        /// Generates the default value for the type, pushes that value onto the evaluation stack, and emits a
        /// return instruction. See the <see cref="ldc"/> method for a list of supported types.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="type">
        /// The type of the value to push onto the evaluation stack, can be <c>null</c> if the
        /// method has no return type (only the ret instruction will be emitted).
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// <para><paramref name="type"/> specifies an unsupported type.</para>
        /// </exception>
        public static ILGenerator ret_default(this ILGenerator generator, Type type)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

            if (type == null || type==typeof(void))
                return generator.ret();
			if (type.IsValueType)
			{
				switch (Type.GetTypeCode(type))
				{
					case TypeCode.Boolean:
					case TypeCode.Char:
					case TypeCode.SByte:
					case TypeCode.Byte:
					case TypeCode.Int16:
					case TypeCode.Int32:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
						return generator.ldc_i4_0().ret();
					case TypeCode.UInt64:
					case TypeCode.Int64:
						return generator.ldc_i4_0().conv_i8().ret();
					case TypeCode.Double:
						return generator.ldc_i4_0().conv_r8().ret();
					case TypeCode.Single:
						return generator.ldc_i4_0().conv_r4().ret();
					case TypeCode.Decimal:
						return generator.ldc(0.0M).ret();
					default: // no choice here we have no constant.
						LocalBuilder def;
						return generator
							.variable(type, out def)
							.ldloca(def)
							.initobj(type)
							.ldloc(def)
							.ret();
				}
				
			}
			// For reference types you can just emit null. 
    		return generator
    			.ldnull()
    			.ret();
        }

		/// <summary>
		/// Generates the default value for the type, pushes that value onto the evaluation stack
		/// See the <see cref="ldc"/> method for a list of supported types.
		/// </summary>
		/// <param name="generator">
		/// The <see cref="ILGenerator"/> to emit the opcode through.
		/// </param>
		/// <param name="type">
		/// The type of the value to push onto the evaluation stack, can be <c>null</c> if the
		/// method has no return type (only the ret instruction will be emitted).
		/// </param>
		/// <returns>
		/// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
		/// </returns>
		/// <exception cref="ArgumentNullException">
		/// <para><paramref name="generator"/> is <c>null</c>.</para>
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// <para><paramref name="type"/> specifies an unsupported type.</para>
		/// </exception>
		public static ILGenerator ld_default(this ILGenerator generator, Type type)
		{
			if (generator == null)
				throw new ArgumentNullException("generator");

			if (type == null || type == typeof(void))
				return generator;
			if (type.IsValueType)
			{
				switch (Type.GetTypeCode(type))
				{
					case TypeCode.Boolean:
					case TypeCode.Char:
					case TypeCode.SByte:
					case TypeCode.Byte:
					case TypeCode.Int16:
					case TypeCode.Int32:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
						return generator.ldc_i4_0();
					case TypeCode.UInt64:
					case TypeCode.Int64:
						return generator.ldc_i4_0().conv_i8();
					case TypeCode.Double:
						return generator.ldc_i4_0().conv_r8();
					case TypeCode.Single:
						return generator.ldc_i4_0().conv_r4();
					case TypeCode.Decimal:
						return generator.ldc(0.0M);
					default: // no choice here we have no constant.
						LocalBuilder def;
						return generator
							.variable(type, out def)
							.ldloca(def)
							.initobj(type)
							.ldloc(def);
				}

			}
			// For reference types you can just emit null. 
			return generator
				.ldnull();
		}

        /// <summary>
        /// Figures out whether the specified method is a virtual or a non-virtual method, and emits the right method
        /// call opcodes.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="method">
        /// The <see cref="MethodInfo"/> to emit as an argument to the opcode.
        /// </param>
        /// <param name="varargsParameterTypes">
        /// Any optional varargs parameter types.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator call_smart(this ILGenerator generator, MethodInfo method, params Type[] varargsParameterTypes)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (method == null)
                throw new ArgumentNullException("method");

            if (!method.IsStatic && (method.IsVirtual || !method.DeclaringType.IsValueType))
            {
                if (varargsParameterTypes.Length > 0)
                    return generator.callvirt(method, varargsParameterTypes);

                return generator.callvirt(method);
            }

            if (varargsParameterTypes.Length > 0)
                return generator.call(method, varargsParameterTypes);

            return generator.call(method);
        }

        /// <summary>
        /// Marks the location in the instruction stream where the label should have its
        /// target.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="label">
        /// The label to emit the target for.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="label"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator mark_label(this ILGenerator generator, Label label)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

            generator.MarkLabel(label);
            return generator;
        }
		/// <summary>
		/// Defines a label in the instruction stream which can be a jump target
		/// </summary>
		/// <param name="generator">
		/// The <see cref="ILGenerator"/> to emit the opcode through.
		/// </param>
		/// <param name="label">
		/// The label to emit the target for.
		/// </param>
		/// <returns>
		/// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
		/// </returns>
		/// <exception cref="ArgumentNullException">
		/// <para><paramref name="generator"/> is <c>null</c>.</para>
		/// <para>- or -</para>
		/// <para><paramref name="label"/> is <c>null</c>.</para>
		/// </exception>
		public static ILGenerator define_label(this ILGenerator generator, out Label label)
		{
			if (generator == null)
				throw new ArgumentNullException("generator");
			label = generator.DefineLabel();
			return generator;
		}

    	/// <summary>
        /// Loads the specified local onto the stack, by index, emitting the optimal
        /// index instruction.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="index">
        /// The index of the local to load.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <para><paramref name="index"/> must be 65535 or less.</para>
        /// </exception>
        public static ILGenerator ldloc_opt(this ILGenerator generator, int index)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (index > UInt16.MaxValue)
                throw new ArgumentOutOfRangeException(String.Format(CultureInfo.InvariantCulture, "index can only be within 0..{0}", UInt16.MaxValue));

            switch (index)
            {
                case 0:
                    return generator.ldloc_0();
                case 1:
                    return generator.ldloc_1();
                case 2:
                    return generator.ldloc_2();
                case 3:
                    return generator.ldloc_3();
            }

            if (index <= Byte.MaxValue)
                return generator.ldloc_s((byte)index);

            return generator.ldloc((short)index);
        }

		/// <summary>
		/// Stores the specified local onto the stack, by index, emitting the optimal
		/// index instruction.
		/// </summary>
		/// <param name="generator">The <see cref="ILGenerator"/> to emit the opcode through.</param>
		/// <param name="index">The index of the local to load.</param>
		/// <returns>
		/// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
		/// </returns>
		/// <exception cref="ArgumentNullException"><paramref name="generator"/> is <c>null</c>.</exception>
		///   
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> must be 65535 or less.</exception>
		public static ILGenerator stloc_opt(this ILGenerator generator, int index)
		{
			if (generator == null)
				throw new ArgumentNullException("generator");
			if (index > UInt16.MaxValue)
				throw new ArgumentOutOfRangeException(String.Format(CultureInfo.InvariantCulture, "index can only be within 0..{0}", UInt16.MaxValue));

			switch (index)
			{
				case 0:
					return generator.stloc_0();
				case 1:
					return generator.stloc_1();
				case 2:
					return generator.stloc_2();
				case 3:
					return generator.stloc_3();
			}

			if (index <= Byte.MaxValue)
				return generator.stloc_s((byte)index);

			return generator.ldloc((short)index);
		}

        /// <summary>
        /// Emits one of the <see cref="OpCodes.Ldarg"/> opcodes, depending
        /// on the index.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="index">
        /// The <see cref="UInt16"/> to emit as an argument to the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <para><paramref name="index"/> is out of range</para>
        /// </exception>
        public static ILGenerator ldarg_opt(this ILGenerator generator, int index)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (index > UInt16.MaxValue)
                throw new ArgumentOutOfRangeException(String.Format(CultureInfo.InvariantCulture, "index can only be within 0..{0}", UInt16.MaxValue));

            switch (index)
            {
                case 0:
                    return generator.ldarg_0();
                case 1:
                    return generator.ldarg_1();
                case 2:
                    return generator.ldarg_2();
                case 3:
                    return generator.ldarg_3();
            }

            if (index <= Byte.MaxValue)
                return generator.ldarg_s((byte)index);

            return generator.ldarg((short)index);
        }

        /// <summary>
        /// Emits one of the <see cref="OpCodes.Ldarg"/> opcodes, depending
        /// on the index, one for each index.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="indices">
        /// An array of <see cref="UInt16"/> indices to emit as an argument to the opcodes.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator ldargs(this ILGenerator generator, params int[] indices)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

			return ldargs(generator, (IEnumerable<int>) indices);
        }

        /// <summary>
        /// Emits one of the <see cref="OpCodes.Ldarg"/> opcodes, depending
        /// on the index, one for each index.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="indices">
        /// A collection of <see cref="UInt16"/> indices to emit as an argument to the opcodes.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator ldargs(this ILGenerator generator, IEnumerable<int> indices)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
			
			foreach (int index in indices)
				generator.ldarg_opt(index);

			return generator;
        }

        /// <summary>
        /// Emits the <see cref="OpCodes.Ldarg_0"/> opcode, to
        /// load the "this" reference.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator ld_this(this ILGenerator generator)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

            return generator.ldarg_0();
        }

        /// <summary>
        /// Evaluates the <see cref="Boolean"/> expression, and if <c>true</c>,
        /// executes the <paramref name="ifThen"/> delegate to add
        /// the necessary IL code to the stream.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="expression">
        /// The <see cref="Boolean"/> expression used to determine if the
        /// code is to be added or not.
        /// </param>
        /// <param name="ifThen">
        /// The delegate to call if <paramref name="expression"/> is <c>true</c>.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator EmitIf(this ILGenerator generator, bool expression, Func<ILGenerator,ILGenerator> ifThen)
        {
            return EmitIf(generator, expression, ifThen, null);
        }

        /// <summary>
        /// Evaluates the <see cref="Boolean"/> expression, and if <c>true</c>,
        /// calls the <paramref name="ifThen"/> delegate to add
        /// the necessary IL code to the stream; or if <c>false</c>,
        /// calls the <paramref name="ifElse"/> delegate.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="expression">
        /// The <see cref="Boolean"/> expression used to determine if the
        /// code is to be added or not.
        /// </param>
        /// <param name="ifThen">
        /// The delegate to call if <paramref name="expression"/> is <c>true</c>.
        /// Can be <c>null</c> if no action is to be taken on <c>true</c>.
        /// </param>
        /// <param name="ifElse">
        /// The delegate to call if <paramref name="expression"/> is <c>false</c>.
        /// Can be <c>null</c> if no action is to be taken on <c>false</c>.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
		public static ILGenerator EmitIf(this ILGenerator generator, bool expression, Func<ILGenerator,ILGenerator> ifThen, Func<ILGenerator,ILGenerator> ifElse)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

			return expression ? ifThen(generator) : (ifElse ?? (i => i)).Invoke(generator);
        }

        /// <summary>
        /// Calls the specified <see cref="Action{ILGenerator,T}"/> delegate for
        /// each element of the collection of source values.
        /// </summary>
        /// <typeparam name="T">
        /// The type of element to emit the body for.
        /// </typeparam>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="source">
        /// The source values to call the action for.
        /// </param>
        /// <param name="body">
        /// The action to call for each value in the <paramref name="source"/>.
        /// objects.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="source"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="body"/> is <c>null</c></para>
        /// </exception>
        public static ILGenerator EmitFor<T>(this ILGenerator generator, IEnumerable<T> source, Action<ILGenerator, T> body)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (source == null)
                throw new ArgumentNullException("source");
            if (body == null)
                throw new ArgumentNullException("body");

            foreach (T element in source)
                body(generator, element);
            return generator;
        }

		/// <summary>
		/// Calls the specified <see cref="Action{ILGenerator,T,Int32}"/> delegate for
		/// each element of the collection of source values.
		/// </summary>
		/// <typeparam name="T">The type of element to emit the body for.</typeparam>
		/// <param name="generator">The <see cref="ILGenerator"/> to emit the opcode through.</param>
		/// <param name="source">The source values to call the action for.</param>
		/// <param name="body">The action to call for each value in the <paramref name="source"/>.
		/// objects.</param>
		/// <returns>
		/// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
		/// </returns>
		/// <exception cref="ArgumentNullException">
		///   <para><paramref name="generator"/> is <c>null</c>.</para>
		///   <para>- or -</para>
		///   <para><paramref name="source"/> is <c>null</c>.</para>
		///   <para>- or -</para>
		///   <para><paramref name="body"/> is <c>null</c></para>
		///   </exception>
		public static ILGenerator EmitFor<T>(this ILGenerator generator, IEnumerable<T> source, Action<ILGenerator, T, int> body)
		{
			if (generator == null)
				throw new ArgumentNullException("generator");
			if (source == null)
				throw new ArgumentNullException("source");
			if (body == null)
				throw new ArgumentNullException("body");
			using (var enumerator = source.GetEnumerator())
			{
				for (int i = 0; enumerator.MoveNext(); i++)
				{
					body(generator, enumerator.Current, i);
				}
			}

			return generator;
		}

    	/// <summary>
        /// Declares a local variable, which will start with an index of 0 for
        /// the first variable and increment by 1 each time.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to define the local variable for.
        /// </param>
        /// <param name="type">
        /// The type of the variable.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        [DebuggerHidden]
        public static ILGenerator variable(this ILGenerator generator, Type type)
        {
			LocalBuilder local;
            return generator.variable(type,out local);
        }

		/// <summary>
		/// Declares a local variable, which will start with an index of 0 for
		/// the first variable and increment by 1 each time.
		/// </summary>
		/// <param name="generator">
		/// The <see cref="ILGenerator"/> to define the local variable for.
		/// </param>
		/// <param name="type">
		/// The type of the variable.
		/// </param>
		/// <param name="local">a local builder to hold the variable</param>
		/// <returns>
		/// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
		/// </returns>
		/// <exception cref="ArgumentNullException">
		/// <para><paramref name="generator"/> is <c>null</c>.</para>
		/// <para>- or -</para>
		/// <para><paramref name="type"/> is <c>null</c>.</para>
		/// </exception>
				[DebuggerHidden]
		public static ILGenerator variable(this ILGenerator generator, Type type,out LocalBuilder local)
		{
			if (generator == null) throw new ArgumentNullException("generator");
			if (type == null) throw new ArgumentNullException("type");
			local = generator.DeclareLocal(type);
			return generator;
		}

        /// <summary>
        /// Declares a local variable, which will start with an index of 0 for
        /// the first variable and increment by 1 each time.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the variable.
        /// </typeparam>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to define the local variable for.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
      [DebuggerHidden]
        public static ILGenerator variable<T>(this ILGenerator generator)
        {
            return generator.variable(typeof(T));
        }

		/// <summary>
		/// Declares a local variable, which will start with an index of 0 for
		/// the first variable and increment by 1 each time.
		/// </summary>
		/// <typeparam name="T">
		/// The type of the variable.
		/// </typeparam>
		/// <param name="generator">
		/// The <see cref="ILGenerator"/> to define the local variable for.
		/// </param>
		/// <param name="local">a local builder to hold the variable</param>
		/// <returns>
		/// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
		/// </returns>
		/// <exception cref="ArgumentNullException">
		/// <para><paramref name="generator"/> is <c>null</c>.</para>
		/// </exception>
				[DebuggerHidden]
		public static ILGenerator variable<T>(this ILGenerator generator, out LocalBuilder local)
		{
			return generator.variable(typeof(T),out local);
		}

        /// <summary>
        /// Creates a new object or a new instance of a value type, pushing an object reference (type O) onto the evaluation stack. (<see cref="OpCodes.Newobj"/>).
        /// </summary>
        /// <typeparam name="T">
        /// The type of object to construct.
        /// </typeparam>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the opcode through.
        /// </param>
        /// <param name="parameterTypes">
        /// The parameter types to find a matching constructor for.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator newobj<T>(this ILGenerator generator, params Type[] parameterTypes)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");

            if (parameterTypes == null)
                parameterTypes = Type.EmptyTypes;

            return generator.newobj(typeof(T).GetConstructor(parameterTypes));
        }

        /// <summary>
        /// Executes the <paramref name="codeBlock"/>, passing in the specified
        /// <paramref name="generator"/>.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the using clause through.
        /// </param>
        /// <param name="codeBlock">
        /// The block of code to execute.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="codeBlock"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator EmitBlock(this ILGenerator generator, Func<ILGenerator, ILGenerator> codeBlock)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (codeBlock == null)
                throw new ArgumentNullException("codeBlock");

            return codeBlock(generator);
        }

        /// <summary>
        /// Emits a <c>try { ... }</c> clause.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the try clause through.
        /// </param>
        /// <param name="codeBlock">
        /// The block of code to execute inside the try clause.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="codeBlock"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator EmitTry(this ILGenerator generator, Func<ILGenerator, ILGenerator> codeBlock)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (codeBlock == null)
                throw new ArgumentNullException("codeBlock");

            generator.BeginExceptionBlock();
            generator.EmitBlock(codeBlock);
            generator.EndExceptionBlock();
            return generator;
        }

        /// <summary>
        /// Emits a <c>catch (T) { ... }</c> block.
        /// </summary>
        /// <typeparam name="T">
        /// The type of exception to catch.
        /// </typeparam>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the catch block through.
        /// </param>
        /// <param name="codeBlock">
        /// The block of code to execute inside the catch block.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="codeBlock"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator EmitCatch<T>(this ILGenerator generator, Func<ILGenerator, LocalBuilder, ILGenerator> codeBlock)
            where T : Exception
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (codeBlock == null)
                throw new ArgumentNullException("codeBlock");

            generator.BeginCatchBlock(typeof(T));
            LocalBuilder ex = generator.DeclareLocal(typeof(Exception));
            generator.stloc(ex);
            return codeBlock(generator, ex);
        }

        /// <summary>
        /// Emits a <c>finally { ... }</c> clause.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the using clause through.
        /// </param>
        /// <param name="codeBlock">
        /// The block of code to execute inside the using clause.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="codeBlock"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator EmitFinally(this ILGenerator generator, Func<ILGenerator, ILGenerator> codeBlock)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (codeBlock == null)
                throw new ArgumentNullException("codeBlock");

            generator.BeginFinallyBlock();
            return generator.EmitBlock(codeBlock).endfinally();
        }

        /// <summary>
        /// Implements a <c>using(topOfstack){}</c> section.
        /// </summary>
        /// <param name="generator">
        /// The <see cref="ILGenerator"/> to emit the using clause through.
        /// </param>
        /// <param name="codeBlock">
        /// The block of code to execute inside the using clause.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> instance passed through <paramref name="generator"/>, for method chaining.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="generator"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="codeBlock"/> is <c>null</c>.</para>
        /// </exception>
        public static ILGenerator EmitUsing(this ILGenerator generator, Func<ILGenerator, ILGenerator> codeBlock)
        {
            if (generator == null)
                throw new ArgumentNullException("generator");
            if (codeBlock == null)
                throw new ArgumentNullException("codeBlock");

            LocalBuilder disposable;
            Label exit = generator.DefineLabel();
            return generator
				.variable<IDisposable>(out disposable)
                .castclass(typeof(IDisposable))
                .stloc(disposable)
                .EmitTry(t => t
                    .EmitBlock(codeBlock)
                    .EmitFinally(f => f
                        .ldloc(disposable)
                        .ldnull()
                        .ceq()
                        .brtrue_s(exit)
                        .ldloc(disposable)
                        .call_smart(typeof(IDisposable).GetMethod("Dispose"))
                        .mark_label(exit)));
        }
    }
}