﻿using System;
using System.Diagnostics;
using System.Reflection.Emit;

namespace Reflective
{
    /// <summary>
    /// Extension methods for <see cref="FieldBuilder"/>.
    /// </summary>
    public static class FieldBuilderExtensions
    {
        /// <summary>
        /// Adds a <see cref="DebuggerBrowsableAttribute"/> attribute to
        /// the specified field.
        /// </summary>
        /// <param name="field">
        /// The <see cref="FieldBuilder"/> for the field that is
        /// being modified.
        /// </param>
        /// <param name="state">
        /// The <see cref="DebuggerBrowsableState"/> value to pass to the
        /// attribute.
        /// </param>
        /// <returns>
        /// The <see cref="FieldBuilder"/> passed in, to promote a
        /// fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        public static FieldBuilder DebuggerBrowsable(this FieldBuilder field, DebuggerBrowsableState state)
        {
            if (field == null)
                throw new ArgumentNullException("field");

            var attr = new CustomAttributeBuilder(
                typeof(DebuggerBrowsableAttribute).GetConstructor(new[] { typeof(DebuggerBrowsableState) }),
                new object[] { state });
            field.SetCustomAttribute(attr);
            return field;
        }
    }
}