﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Diagnostics;
namespace Reflective
{
    public static partial class ILGeneratorExtensions
    {
        /// <summary>
        /// <see cref="OpCodes.Add"/>: Adds two values and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Add"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.add.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator add(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Add);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Add_Ovf"/>: Adds two integers, performs an overflow check, and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Add_Ovf"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.add_ovf.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator add_ovf(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Add_Ovf);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Add_Ovf_Un"/>: Adds two unsigned integer values, performs an overflow check, and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Add_Ovf_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.add_ovf_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator add_ovf_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Add_Ovf_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.And"/>: Computes the bitwise AND of two values and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.And"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.and.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator and(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.And);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Arglist"/>: Returns an unmanaged pointer to the argument list of the current method.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Arglist"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.arglist.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator arglist(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Arglist);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Beq"/>: Transfers control to a target instruction if two values are equal.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Beq"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.beq.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator beq(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Beq, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Beq_S"/>: Transfers control to a target instruction (short form) if two values are equal.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Beq_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.beq_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator beq_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Beq_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bge"/>: Transfers control to a target instruction if the first value is greater than or equal to the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bge"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bge.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bge(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bge, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bge_S"/>: Transfers control to a target instruction (short form) if the first value is greater than or equal to the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bge_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bge_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bge_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bge_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bge_Un"/>: Transfers control to a target instruction if the first value is greater than the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bge_Un"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bge_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bge_un(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bge_Un, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bge_Un_S"/>: Transfers control to a target instruction (short form) if the first value is greater than the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bge_Un_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bge_un_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bge_un_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bge_Un_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bgt"/>: Transfers control to a target instruction if the first value is greater than the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bgt"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bgt.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bgt(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bgt, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bgt_S"/>: Transfers control to a target instruction (short form) if the first value is greater than the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bgt_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bgt_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bgt_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bgt_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bgt_Un"/>: Transfers control to a target instruction if the first value is greater than the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bgt_Un"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bgt_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bgt_un(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bgt_Un, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bgt_Un_S"/>: Transfers control to a target instruction (short form) if the first value is greater than the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bgt_Un_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bgt_un_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bgt_un_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bgt_Un_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ble"/>: Transfers control to a target instruction if the first value is less than or equal to the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ble"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ble.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ble(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ble, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ble_S"/>: Transfers control to a target instruction (short form) if the first value is less than or equal to the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ble_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ble_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ble_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ble_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ble_Un"/>: Transfers control to a target instruction if the first value is less than or equal to the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ble_Un"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ble_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ble_un(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ble_Un, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ble_Un_S"/>: Transfers control to a target instruction (short form) if the first value is less than or equal to the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ble_Un_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ble_un_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ble_un_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ble_Un_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Blt"/>: Transfers control to a target instruction if the first value is less than the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Blt"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.blt.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator blt(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Blt, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Blt_S"/>: Transfers control to a target instruction (short form) if the first value is less than the second value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Blt_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.blt_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator blt_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Blt_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Blt_Un"/>: Transfers control to a target instruction if the first value is less than the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Blt_Un"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.blt_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator blt_un(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Blt_Un, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Blt_Un_S"/>: Transfers control to a target instruction (short form) if the first value is less than the second value, when comparing unsigned integer values or unordered float values.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Blt_Un_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.blt_un_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator blt_un_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Blt_Un_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bne_Un"/>: Transfers control to a target instruction when two unsigned integer values or unordered float values are not equal.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bne_Un"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bne_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bne_un(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bne_Un, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Bne_Un_S"/>: Transfers control to a target instruction (short form) when two unsigned integer values or unordered float values are not equal.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Bne_Un_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.bne_un_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator bne_un_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Bne_Un_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Box"/>: Converts a value type to an object reference (type O).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Box"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.box.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator box(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Box, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Box"/>: Converts a value type to an object reference (type O).
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Box"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.box.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator box<T>(this ILGenerator il)
        {
            return box(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Br"/>: Unconditionally transfers control to a target instruction.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Br"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.br.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator br(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Br, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Br_S"/>: Unconditionally transfers control to a target instruction (short form).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Br_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.br_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator br_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Br_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Break"/>: Signals the Common Language Infrastructure (CLI) to inform the debugger that a break point has been tripped.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Break"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.break_.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator break_(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Break);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Brfalse"/>: Transfers control to a target instruction if value is false, a null reference (Nothing in Visual Basic), or zero.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Brfalse"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.brfalse.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator brfalse(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Brfalse, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Brfalse_S"/>: Transfers control to a target instruction if value is false, a null reference, or zero.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Brfalse_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.brfalse_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator brfalse_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Brfalse_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Brtrue"/>: Transfers control to a target instruction if value is true, not null, or non-zero.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Brtrue"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.brtrue.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator brtrue(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Brtrue, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Brtrue_S"/>: Transfers control to a target instruction (short form) if value is true, not null, or non-zero.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Brtrue_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.brtrue_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator brtrue_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Brtrue_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Call"/>: Calls the method indicated by the passed method descriptor.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Call"/> instruction to.
        /// </param>
        /// <param name="ctor">
        /// The &lt;see cref="ConstructorInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="ctor"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.call.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator call(this ILGenerator il, ConstructorInfo ctor)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (ctor == null)
                throw new ArgumentNullException("ctor");
            il.Emit(OpCodes.Call, ctor);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Call"/>: Calls the method indicated by the passed method descriptor.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Call"/> instruction to.
        /// </param>
        /// <param name="method">
        /// The &lt;see cref="MethodInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.call.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator call(this ILGenerator il, MethodInfo method)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (method == null)
                throw new ArgumentNullException("method");
            il.Emit(OpCodes.Call, method);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Castclass"/>: Attempts to cast an object passed by reference to the specified class.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Castclass"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.castclass.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator castclass(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Castclass, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Castclass"/>: Attempts to cast an object passed by reference to the specified class.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Castclass"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.castclass.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator castclass<T>(this ILGenerator il)
        {
            return castclass(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Ceq"/>: Compares two values. If they are equal, the integer value 1 (int32) is pushed onto the evaluation stack; otherwise 0 (int32) is pushed onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ceq"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ceq.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ceq(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ceq);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Cgt"/>: Compares two values. If the first value is greater than the second, the integer value 1 (int32) is pushed onto the evaluation stack; otherwise 0 (int32) is pushed onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Cgt"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.cgt.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator cgt(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Cgt);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Cgt_Un"/>: Compares two unsigned or unordered values. If the first value is greater than the second, the integer value 1 (int32) is pushed onto the evaluation stack; otherwise 0 (int32) is pushed onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Cgt_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.cgt_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator cgt_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Cgt_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ckfinite"/>: Throws ArithmeticException if value is not a finite number.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ckfinite"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ckfinite.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ckfinite(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ckfinite);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Clt"/>: Compares two values. If the first value is less than the second, the integer value 1 (int32) is pushed onto the evaluation stack; otherwise 0 (int32) is pushed onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Clt"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.clt.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator clt(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Clt);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Clt_Un"/>: Compares the unsigned or unordered values value1 and value2. If value1 is less than value2, then the integer value 1 (int32) is pushed onto the evaluation stack; otherwise 0 (int32) is pushed onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Clt_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.clt_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator clt_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Clt_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Constrained"/>: Constrains the type on which a virtual method call is made.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Constrained"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.constrained.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator constrained(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Constrained, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Constrained"/>: Constrains the type on which a virtual method call is made.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Constrained"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.constrained.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator constrained<T>(this ILGenerator il)
        {
            return constrained(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_I"/>: Converts the value on top of the evaluation stack to native int.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_I"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_i.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_i(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_I);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_I1"/>: Converts the value on top of the evaluation stack to int8, then extends (pads) it to int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_I1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_i1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_i1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_I1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_I2"/>: Converts the value on top of the evaluation stack to int16, then extends (pads) it to int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_I2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_i2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_i2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_I2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_I4"/>: Converts the value on top of the evaluation stack to int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_I4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_i4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_i4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_I4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_I8"/>: Converts the value on top of the evaluation stack to int64.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_I8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_i8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_i8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_I8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I"/>: Converts the signed value on top of the evaluation stack to signed native int, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I_Un"/>: Converts the unsigned value on top of the evaluation stack to signed native int, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I1"/>: Converts the signed value on top of the evaluation stack to signed int8 and extends it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I1_Un"/>: Converts the unsigned value on top of the evaluation stack to signed int8 and extends it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I1_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i1_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i1_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I1_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I2"/>: Converts the signed value on top of the evaluation stack to signed int16 and extending it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I2_Un"/>: Converts the unsigned value on top of the evaluation stack to signed int16 and extends it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I2_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i2_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i2_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I2_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I4"/>: Converts the signed value on top of the evaluation stack to signed int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I4_Un"/>: Converts the unsigned value on top of the evaluation stack to signed int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I4_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i4_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i4_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I4_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I8"/>: Converts the signed value on top of the evaluation stack to signed int64, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_I8_Un"/>: Converts the unsigned value on top of the evaluation stack to signed int64, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_I8_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_i8_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_i8_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_I8_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U"/>: Converts the signed value on top of the evaluation stack to unsigned native int, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U_Un"/>: Converts the unsigned value on top of the evaluation stack to unsigned native int, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U1"/>: Converts the signed value on top of the evaluation stack to unsigned int8 and extends it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U1_Un"/>: Converts the unsigned value on top of the evaluation stack to unsigned int8 and extends it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U1_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u1_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u1_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U1_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U2"/>: Converts the signed value on top of the evaluation stack to unsigned int16 and extends it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U2_Un"/>: Converts the unsigned value on top of the evaluation stack to unsigned int16 and extends it to int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U2_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u2_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u2_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U2_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U4"/>: Converts the signed value on top of the evaluation stack to unsigned int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U4_Un"/>: Converts the unsigned value on top of the evaluation stack to unsigned int32, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U4_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u4_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u4_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U4_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U8"/>: Converts the signed value on top of the evaluation stack to unsigned int64, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_Ovf_U8_Un"/>: Converts the unsigned value on top of the evaluation stack to unsigned int64, throwing OverflowException on overflow.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_Ovf_U8_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_ovf_u8_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_ovf_u8_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_Ovf_U8_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_R_Un"/>: Converts the unsigned integer value on top of the evaluation stack to float32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_R_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_r_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_r_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_R_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_R4"/>: Converts the value on top of the evaluation stack to float32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_R4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_r4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_r4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_R4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_R8"/>: Converts the value on top of the evaluation stack to float64.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_R8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_r8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_r8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_R8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_U"/>: Converts the value on top of the evaluation stack to unsigned native int, and extends it to native int.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_U"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_u.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_u(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_U);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_U1"/>: Converts the value on top of the evaluation stack to unsigned int8, and extends it to int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_U1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_u1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_u1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_U1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_U2"/>: Converts the value on top of the evaluation stack to unsigned int16, and extends it to int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_U2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_u2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_u2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_U2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_U4"/>: Converts the value on top of the evaluation stack to unsigned int32, and extends it to int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_U4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_u4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_u4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_U4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Conv_U8"/>: Converts the value on top of the evaluation stack to unsigned int64, and extends it to int64.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Conv_U8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.conv_u8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator conv_u8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Conv_U8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Cpblk"/>: Copies a specified number bytes from a source address to a destination address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Cpblk"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.cpblk.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator cpblk(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Cpblk);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Cpobj"/>: Copies the value type located at the address of an object (type &amp;, * or native int) to the address of the destination object (type &amp;, * or native int).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Cpobj"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.cpobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator cpobj(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Cpobj, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Cpobj"/>: Copies the value type located at the address of an object (type &amp;, * or native int) to the address of the destination object (type &amp;, * or native int).
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Cpobj"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.cpobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator cpobj<T>(this ILGenerator il)
        {
            return cpobj(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Div"/>: Divides two values and pushes the result as a floating-point (type F) or quotient (type int32) onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Div"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.div.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator div(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Div);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Div_Un"/>: Divides two unsigned integer values and pushes the result (int32) onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Div_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.div_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator div_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Div_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Dup"/>: Copies the current topmost value on the evaluation stack, and then pushes the copy onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Dup"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.dup.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator dup(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Dup);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Endfilter"/>: Transfers control from the filter clause of an exception back to the Common Language Infrastructure (CLI) exception handler.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Endfilter"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.endfilter.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator endfilter(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Endfilter);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Endfinally"/>: Transfers control from the fault or finally clause of an exception block back to the Common Language Infrastructure (CLI) exception handler.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Endfinally"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.endfinally.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator endfinally(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Endfinally);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Initblk"/>: Initializes a specified block of memory at a specific address to a given size and initial value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Initblk"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.initblk.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator initblk(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Initblk);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Initobj"/>: Initializes each field of the value type at a specified address to a null reference or a 0 of the appropriate primitive type.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Initobj"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.initobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator initobj(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Initobj, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Initobj"/>: Initializes each field of the value type at a specified address to a null reference or a 0 of the appropriate primitive type.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Initobj"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.initobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator initobj<T>(this ILGenerator il)
        {
            return initobj(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Isinst"/>: Tests whether an object reference (type O) is an instance of a particular class.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Isinst"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.isinst.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator isinst(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Isinst, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Isinst"/>: Tests whether an object reference (type O) is an instance of a particular class.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Isinst"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.isinst.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator isinst<T>(this ILGenerator il)
        {
            return isinst(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Jmp"/>: Exits current method and jumps to specified method.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Jmp"/> instruction to.
        /// </param>
        /// <param name="method">
        /// The &lt;see cref="MethodInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.jmp.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator jmp(this ILGenerator il, MethodInfo method)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (method == null)
                throw new ArgumentNullException("method");
            il.Emit(OpCodes.Jmp, method);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarg"/>: Loads an argument (referenced by a specified index value) onto the stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarg"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The &lt;see cref="short" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarg.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarg(this ILGenerator il, short index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarg, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarg_0"/>: Loads the argument at index 0 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarg_0"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarg_0.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarg_0(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarg_0);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarg_1"/>: Loads the argument at index 1 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarg_1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarg_1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarg_1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarg_1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarg_2"/>: Loads the argument at index 2 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarg_2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarg_2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarg_2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarg_2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarg_3"/>: Loads the argument at index 3 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarg_3"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarg_3.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarg_3(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarg_3);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarg_S"/>: Loads the argument (referenced by a specified short form index) onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarg_S"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarg_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarg_s(this ILGenerator il, byte index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarg_S, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarga"/>: Load an argument address onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarga"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The &lt;see cref="short" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarga.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarga(this ILGenerator il, short index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarga, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldarga_S"/>: Load an argument address, in short form, onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldarga_S"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldarga_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldarga_s(this ILGenerator il, byte index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldarga_S, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4"/>: Pushes a supplied value of type int32 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4(this ILGenerator il, int value)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4, value);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_0"/>: Pushes the integer value of 0 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_0"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_0.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_0(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_0);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_1"/>: Pushes the integer value of 1 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_2"/>: Pushes the integer value of 2 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_3"/>: Pushes the integer value of 3 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_3"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_3.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_3(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_3);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_4"/>: Pushes the integer value of 4 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_5"/>: Pushes the integer value of 5 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_5"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_5.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_5(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_5);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_6"/>: Pushes the integer value of 6 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_6"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_6.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_6(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_6);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_7"/>: Pushes the integer value of 7 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_7"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_7.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_7(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_7);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_8"/>: Pushes the integer value of 8 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_M1"/>: Pushes the integer value of -1 onto the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_M1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_m1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_m1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_M1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I4_S"/>: Pushes the supplied int8 value onto the evaluation stack as an int32, short form.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I4_S"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i4_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i4_s(this ILGenerator il, byte value)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I4_S, value);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_I8"/>: Pushes a supplied value of type int64 onto the evaluation stack as an int64.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_I8"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_i8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_i8(this ILGenerator il, long value)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_I8, value);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_R4"/>: Pushes a supplied value of type float32 onto the evaluation stack as type F (float).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_R4"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_r4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_r4(this ILGenerator il, float value)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_R4, value);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldc_R8"/>: Pushes a supplied value of type float64 onto the evaluation stack as type F (float).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldc_R8"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldc_r8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldc_r8(this ILGenerator il, double value)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldc_R8, value);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem"/>: Loads the element at a specified array index onto the top of the evaluation stack as the type specified in the instruction.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Ldelem, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem"/>: Loads the element at a specified array index onto the top of the evaluation stack as the type specified in the instruction.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem<T>(this ILGenerator il)
        {
            return ldelem(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_I"/>: Loads the element with type native int at a specified array index onto the top of the evaluation stack as a native int.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_I"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_i.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_i(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_I);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_I1"/>: Loads the element with type int8 at a specified array index onto the top of the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_I1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_i1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_i1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_I1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_I2"/>: Loads the element with type int16 at a specified array index onto the top of the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_I2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_i2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_i2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_I2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_I4"/>: Loads the element with type int32 at a specified array index onto the top of the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_I4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_i4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_i4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_I4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_I8"/>: Loads the element with type int64 at a specified array index onto the top of the evaluation stack as an int64.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_I8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_i8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_i8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_I8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_R4"/>: Loads the element with type float32 at a specified array index onto the top of the evaluation stack as type F (float).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_R4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_r4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_r4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_R4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_R8"/>: Loads the element with type float64 at a specified array index onto the top of the evaluation stack as type F (float).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_R8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_r8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_r8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_R8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_Ref"/>: Loads the element containing an object reference at a specified array index onto the top of the evaluation stack as type O (object reference).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_Ref"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_ref.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_ref(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_Ref);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_U1"/>: Loads the element with type unsigned int8 at a specified array index onto the top of the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_U1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_u1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_u1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_U1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_U2"/>: Loads the element with type unsigned int16 at a specified array index onto the top of the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_U2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_u2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_u2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_U2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelem_U4"/>: Loads the element with type unsigned int32 at a specified array index onto the top of the evaluation stack as an int32.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelem_U4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelem_u4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelem_u4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldelem_U4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelema"/>: Loads the address of the array element at a specified array index onto the top of the evaluation stack as type &amp; (managed pointer).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelema"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelema.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelema(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Ldelema, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldelema"/>: Loads the address of the array element at a specified array index onto the top of the evaluation stack as type &amp; (managed pointer).
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldelema"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldelema.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldelema<T>(this ILGenerator il)
        {
            return ldelema(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Ldfld"/>: Finds the value of a field in the object whose reference is currently on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldfld"/> instruction to.
        /// </param>
        /// <param name="field">
        /// The &lt;see cref="FieldInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldfld.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldfld(this ILGenerator il, FieldInfo field)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (field == null)
                throw new ArgumentNullException("field");
            il.Emit(OpCodes.Ldfld, field);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldflda"/>: Finds the address of a field in the object whose reference is currently on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldflda"/> instruction to.
        /// </param>
        /// <param name="field">
        /// The &lt;see cref="FieldInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldflda.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldflda(this ILGenerator il, FieldInfo field)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (field == null)
                throw new ArgumentNullException("field");
            il.Emit(OpCodes.Ldflda, field);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldftn"/>: Pushes an unmanaged pointer (type native int) to the native code implementing a specific method onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldftn"/> instruction to.
        /// </param>
        /// <param name="method">
        /// The &lt;see cref="MethodInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldftn.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldftn(this ILGenerator il, MethodInfo method)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (method == null)
                throw new ArgumentNullException("method");
            il.Emit(OpCodes.Ldftn, method);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_I"/>: Loads a value of type native int as a native int onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_I"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_i.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_i(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_I);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_I1"/>: Loads a value of type int8 as an int32 onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_I1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_i1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_i1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_I1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_I2"/>: Loads a value of type int16 as an int32 onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_I2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_i2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_i2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_I2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_I4"/>: Loads a value of type int32 as an int32 onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_I4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_i4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_i4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_I4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_I8"/>: Loads a value of type int64 as an int64 onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_I8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_i8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_i8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_I8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_R4"/>: Loads a value of type float32 as a type F (float) onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_R4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_r4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_r4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_R4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_R8"/>: Loads a value of type float64 as a type F (float) onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_R8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_r8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_r8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_R8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_Ref"/>: Loads an object reference as a type O (object reference) onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_Ref"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_ref.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_ref(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_Ref);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_U1"/>: Loads a value of type unsigned int8 as an int32 onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_U1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_u1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_u1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_U1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_U2"/>: Loads a value of type unsigned int16 as an int32 onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_U2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_u2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_u2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_U2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldind_U4"/>: Loads a value of type unsigned int32 as an int32 onto the evaluation stack indirectly.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldind_U4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldind_u4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldind_u4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldind_U4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldlen"/>: Pushes the number of elements of a zero-based, one-dimensional array onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldlen"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldlen.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldlen(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldlen);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloc"/>: Loads the local variable at a specific index onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloc"/> instruction to.
        /// </param>
        /// <param name="variable">
        /// The &lt;see cref="LocalBuilder" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="variable"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloc.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloc(this ILGenerator il, LocalBuilder variable)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (variable == null)
                throw new ArgumentNullException("variable");
            il.Emit(OpCodes.Ldloc, variable);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloc"/>: Loads the local variable at a specific index onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloc"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The &lt;see cref="short" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloc.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloc(this ILGenerator il, short index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloc, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloc_0"/>: Loads the local variable at index 0 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloc_0"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloc_0.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloc_0(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloc_0);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloc_1"/>: Loads the local variable at index 1 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloc_1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloc_1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloc_1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloc_1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloc_2"/>: Loads the local variable at index 2 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloc_2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloc_2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloc_2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloc_2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloc_3"/>: Loads the local variable at index 3 onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloc_3"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloc_3.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloc_3(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloc_3);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloc_S"/>: Loads the local variable at a specific index onto the evaluation stack, short form.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloc_S"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloc_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloc_s(this ILGenerator il, byte index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloc_S, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloca"/>: Loads the address of the local variable at a specific index onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloca"/> instruction to.
        /// </param>
        /// <param name="variable">
        /// The &lt;see cref="LocalBuilder" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="variable"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloca.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloca(this ILGenerator il, LocalBuilder variable)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (variable == null)
                throw new ArgumentNullException("variable");
            il.Emit(OpCodes.Ldloca, variable);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloca"/>: Loads the address of the local variable at a specific index onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloca"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The &lt;see cref="short" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloca.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloca(this ILGenerator il, short index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloca, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldloca_S"/>: Loads the address of the local variable at a specific index onto the evaluation stack, short form.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldloca_S"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldloca_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldloca_s(this ILGenerator il, byte index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldloca_S, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldnull"/>: Pushes a null reference (type O) onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldnull"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldnull.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldnull(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldnull);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldobj"/>: Copies the value type object pointed to by an address to the top of the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldobj"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldobj(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Ldobj, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldobj"/>: Copies the value type object pointed to by an address to the top of the evaluation stack.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldobj"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldobj<T>(this ILGenerator il)
        {
            return ldobj(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Ldsfld"/>: Pushes the value of a static field onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldsfld"/> instruction to.
        /// </param>
        /// <param name="field">
        /// The &lt;see cref="FieldInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldsfld.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldsfld(this ILGenerator il, FieldInfo field)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (field == null)
                throw new ArgumentNullException("field");
            il.Emit(OpCodes.Ldsfld, field);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldsflda"/>: Pushes the address of a static field onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldsflda"/> instruction to.
        /// </param>
        /// <param name="field">
        /// The &lt;see cref="FieldInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldsflda.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldsflda(this ILGenerator il, FieldInfo field)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (field == null)
                throw new ArgumentNullException("field");
            il.Emit(OpCodes.Ldsflda, field);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldstr"/>: Pushes a new object reference to a string literal stored in the metadata.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldstr"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldstr.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldstr(this ILGenerator il, string value)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ldstr, value);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldtoken"/>: Converts a metadata token to its runtime representation, pushing it onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldtoken"/> instruction to.
        /// </param>
        /// <param name="ctor">
        /// The &lt;see cref="ConstructorInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="ctor"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldtoken.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldtoken(this ILGenerator il, ConstructorInfo ctor)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (ctor == null)
                throw new ArgumentNullException("ctor");
            il.Emit(OpCodes.Ldtoken, ctor);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldtoken"/>: Converts a metadata token to its runtime representation, pushing it onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldtoken"/> instruction to.
        /// </param>
        /// <param name="method">
        /// The &lt;see cref="MethodInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldtoken.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldtoken(this ILGenerator il, MethodInfo method)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (method == null)
                throw new ArgumentNullException("method");
            il.Emit(OpCodes.Ldtoken, method);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldtoken"/>: Converts a metadata token to its runtime representation, pushing it onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldtoken"/> instruction to.
        /// </param>
        /// <param name="field">
        /// The &lt;see cref="FieldInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldtoken.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldtoken(this ILGenerator il, FieldInfo field)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (field == null)
                throw new ArgumentNullException("field");
            il.Emit(OpCodes.Ldtoken, field);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldtoken"/>: Converts a metadata token to its runtime representation, pushing it onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldtoken"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldtoken.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldtoken(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Ldtoken, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ldtoken"/>: Converts a metadata token to its runtime representation, pushing it onto the evaluation stack.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldtoken"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldtoken.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldtoken<T>(this ILGenerator il)
        {
            return ldtoken(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Ldvirtftn"/>: Pushes an unmanaged pointer (type native int) to the native code implementing a particular virtual method associated with a specified object onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ldvirtftn"/> instruction to.
        /// </param>
        /// <param name="method">
        /// The &lt;see cref="MethodInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="method"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ldvirtftn.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ldvirtftn(this ILGenerator il, MethodInfo method)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (method == null)
                throw new ArgumentNullException("method");
            il.Emit(OpCodes.Ldvirtftn, method);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Leave"/>: Exits a protected region of code, unconditionally transferring control to a specific target instruction.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Leave"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.leave.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator leave(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Leave, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Leave_S"/>: Exits a protected region of code, unconditionally transferring control to a target instruction (short form).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Leave_S"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.leave_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator leave_s(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Leave_S, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Localloc"/>: Allocates a certain number of bytes from the local dynamic memory pool and pushes the address (a transient pointer, type *) of the first allocated byte onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Localloc"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.localloc.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator localloc(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Localloc);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Mkrefany"/>: Pushes a typed reference to an instance of a specific type onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Mkrefany"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.mkrefany.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator mkrefany(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Mkrefany, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Mkrefany"/>: Pushes a typed reference to an instance of a specific type onto the evaluation stack.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Mkrefany"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.mkrefany.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator mkrefany<T>(this ILGenerator il)
        {
            return mkrefany(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Mul"/>: Multiplies two values and pushes the result on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Mul"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.mul.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator mul(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Mul);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Mul_Ovf"/>: Multiplies two integer values, performs an overflow check, and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Mul_Ovf"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.mul_ovf.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator mul_ovf(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Mul_Ovf);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Mul_Ovf_Un"/>: Multiplies two unsigned integer values, performs an overflow check, and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Mul_Ovf_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.mul_ovf_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator mul_ovf_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Mul_Ovf_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Neg"/>: Negates a value and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Neg"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.neg.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator neg(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Neg);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Newarr"/>: Pushes an object reference to a new zero-based, one-dimensional array whose elements are of a specific type onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Newarr"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.newarr.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator newarr(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Newarr, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Newarr"/>: Pushes an object reference to a new zero-based, one-dimensional array whose elements are of a specific type onto the evaluation stack.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Newarr"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.newarr.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator newarr<T>(this ILGenerator il)
        {
            return newarr(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Newobj"/>: Creates a new object or a new instance of a value type, pushing an object reference (type O) onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Newobj"/> instruction to.
        /// </param>
        /// <param name="ctor">
        /// The &lt;see cref="ConstructorInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="ctor"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.newobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator newobj(this ILGenerator il, ConstructorInfo ctor)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (ctor == null)
                throw new ArgumentNullException("ctor");
            il.Emit(OpCodes.Newobj, ctor);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Nop"/>: Fills space if opcodes are patched. No meaningful operation is performed although a processing cycle can be consumed.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Nop"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.nop.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator nop(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Nop);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Not"/>: Computes the bitwise complement of the integer value on top of the stack and pushes the result onto the evaluation stack as the same type.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Not"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.not.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator not(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Not);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Or"/>: Compute the bitwise complement of the two integer values on top of the stack and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Or"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.or.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator or(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Or);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Pop"/>: Removes the value currently on top of the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Pop"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.pop.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator pop(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Pop);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Readonly"/>: Specifies that the subsequent array address operation performs no type check at run time, and that it returns a managed pointer whose mutability is restricted.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Readonly"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.readonly_.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator readonly_(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Readonly);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Refanytype"/>: Retrieves the type token embedded in a typed reference.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Refanytype"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.refanytype.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator refanytype(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Refanytype);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Refanyval"/>: Retrieves the address (type &amp;) embedded in a typed reference.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Refanyval"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.refanyval.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator refanyval(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Refanyval, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Refanyval"/>: Retrieves the address (type &amp;) embedded in a typed reference.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Refanyval"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.refanyval.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator refanyval<T>(this ILGenerator il)
        {
            return refanyval(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Rem"/>: Divides two values and pushes the remainder onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Rem"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.rem.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator rem(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Rem);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Rem_Un"/>: Divides two unsigned values and pushes the remainder onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Rem_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.rem_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator rem_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Rem_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Ret"/>: Returns from the current method, pushing a return value (if present) from the callee's evaluation stack onto the caller's evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Ret"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.ret.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator ret(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Ret);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Rethrow"/>: Rethrows the current exception.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Rethrow"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.rethrow.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator rethrow(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Rethrow);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Shl"/>: Shifts an integer value to the left (in zeroes) by a specified number of bits, pushing the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Shl"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.shl.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator shl(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Shl);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Shr"/>: Shifts an integer value (in sign) to the right by a specified number of bits, pushing the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Shr"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.shr.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator shr(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Shr);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Shr_Un"/>: Shifts an unsigned integer value (in zeroes) to the right by a specified number of bits, pushing the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Shr_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.shr_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator shr_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Shr_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Sizeof"/>: Pushes the size, in bytes, of a supplied value type onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Sizeof"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.sizeof_.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator sizeof_(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Sizeof, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Sizeof"/>: Pushes the size, in bytes, of a supplied value type onto the evaluation stack.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Sizeof"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.sizeof_.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator sizeof_<T>(this ILGenerator il)
        {
            return sizeof_(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Starg"/>: Stores the value on top of the evaluation stack in the argument slot at a specified index.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Starg"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The &lt;see cref="short" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.starg.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator starg(this ILGenerator il, short index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Starg, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Starg_S"/>: Stores the value on top of the evaluation stack in the argument slot at a specified index, short form.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Starg_S"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.starg_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator starg_s(this ILGenerator il, byte index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Starg_S, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem"/>: Replaces the array element at a given index with the value on the evaluation stack, whose type is specified in the instruction.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Stelem, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem"/>: Replaces the array element at a given index with the value on the evaluation stack, whose type is specified in the instruction.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem<T>(this ILGenerator il)
        {
            return stelem(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_I"/>: Replaces the array element at a given index with the native int value on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_I"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_i.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_i(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_I);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_I1"/>: Replaces the array element at a given index with the int8 value on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_I1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_i1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_i1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_I1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_I2"/>: Replaces the array element at a given index with the int16 value on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_I2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_i2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_i2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_I2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_I4"/>: Replaces the array element at a given index with the int32 value on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_I4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_i4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_i4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_I4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_I8"/>: Replaces the array element at a given index with the int64 value on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_I8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_i8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_i8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_I8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_R4"/>: Replaces the array element at a given index with the float32 value on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_R4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_r4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_r4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_R4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_R8"/>: Replaces the array element at a given index with the float64 value on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_R8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_r8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_r8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_R8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stelem_Ref"/>: Replaces the array element at a given index with the object ref value (type O) on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stelem_Ref"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stelem_ref.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stelem_ref(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stelem_Ref);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stfld"/>: Replaces the value stored in the field of an object reference or pointer with a new value.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stfld"/> instruction to.
        /// </param>
        /// <param name="field">
        /// The &lt;see cref="FieldInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stfld.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stfld(this ILGenerator il, FieldInfo field)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (field == null)
                throw new ArgumentNullException("field");
            il.Emit(OpCodes.Stfld, field);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_I"/>: Stores a value of type native int at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_I"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_i.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_i(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_I);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_I1"/>: Stores a value of type int8 at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_I1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_i1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_i1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_I1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_I2"/>: Stores a value of type int16 at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_I2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_i2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_i2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_I2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_I4"/>: Stores a value of type int32 at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_I4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_i4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_i4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_I4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_I8"/>: Stores a value of type int64 at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_I8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_i8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_i8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_I8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_R4"/>: Stores a value of type float32 at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_R4"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_r4.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_r4(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_R4);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_R8"/>: Stores a value of type float64 at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_R8"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_r8.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_r8(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_R8);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stind_Ref"/>: Stores a object reference value at a supplied address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stind_Ref"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stind_ref.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stind_ref(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stind_Ref);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at a specified index.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The &lt;see cref="short" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc(this ILGenerator il, short index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stloc, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at a specified index.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc"/> instruction to.
        /// </param>
        /// <param name="variable">
        /// The &lt;see cref="LocalBuilder" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="variable"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc(this ILGenerator il, LocalBuilder variable)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (variable == null)
                throw new ArgumentNullException("variable");
            il.Emit(OpCodes.Stloc, variable);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc_0"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at index 0.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc_0"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc_0.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc_0(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stloc_0);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc_1"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at index 1.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc_1"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc_1.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc_1(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stloc_1);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc_2"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at index 2.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc_2"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc_2.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc_2(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stloc_2);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc_3"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at index 3.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc_3"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc_3.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc_3(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stloc_3);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc_S"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at index (short form).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc_S"/> instruction to.
        /// </param>
        /// <param name="index">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc_s(this ILGenerator il, byte index)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Stloc_S, index);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stloc_S"/>: Pops the current value from the top of the evaluation stack and stores it in a the local variable list at index (short form).
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stloc_S"/> instruction to.
        /// </param>
        /// <param name="variable">
        /// The &lt;see cref="LocalBuilder" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="variable"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stloc_s.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stloc_s(this ILGenerator il, LocalBuilder variable)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (variable == null)
                throw new ArgumentNullException("variable");
            il.Emit(OpCodes.Stloc_S, variable);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stobj"/>: Copies a value of a specified type from the evaluation stack into a supplied memory address.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stobj"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stobj(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Stobj, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Stobj"/>: Copies a value of a specified type from the evaluation stack into a supplied memory address.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stobj"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stobj.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stobj<T>(this ILGenerator il)
        {
            return stobj(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Stsfld"/>: Replaces the value of a static field with a value from the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Stsfld"/> instruction to.
        /// </param>
        /// <param name="field">
        /// The &lt;see cref="FieldInfo" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="field"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.stsfld.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator stsfld(this ILGenerator il, FieldInfo field)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (field == null)
                throw new ArgumentNullException("field");
            il.Emit(OpCodes.Stsfld, field);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Sub"/>: Subtracts one value from another and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Sub"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.sub.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator sub(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Sub);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Sub_Ovf"/>: Subtracts one integer value from another, performs an overflow check, and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Sub_Ovf"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.sub_ovf.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator sub_ovf(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Sub_Ovf);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Sub_Ovf_Un"/>: Subtracts one unsigned integer value from another, performs an overflow check, and pushes the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Sub_Ovf_Un"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.sub_ovf_un.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator sub_ovf_un(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Sub_Ovf_Un);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Switch"/>: Implements a jump table.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Switch"/> instruction to.
        /// </param>
        /// <param name="targets">
        /// The array of &lt;see cref="Label"/gt;s to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="targets"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.switch_.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator switch_(this ILGenerator il, Label[] targets)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (targets == null)
                throw new ArgumentNullException("targets");
            il.Emit(OpCodes.Switch, targets);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Tailcall"/>: Performs a postfixed method call instruction such that the current method's stack frame is removed before the actual call instruction is executed.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Tailcall"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.tailcall.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator tailcall(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Tailcall);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Throw"/>: Throws the exception object currently on the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Throw"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.throw_.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator throw_(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Throw);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Unaligned"/>: Indicates that an address currently atop the evaluation stack might not be aligned to the natural size of the immediately following ldind, stind, ldfld, stfld, ldobj, stobj, initblk, or cpblk instruction.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Unaligned"/> instruction to.
        /// </param>
        /// <param name="target">
        /// The &lt;see cref="Label"/gt; to branch to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.unaligned.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator unaligned(this ILGenerator il, Label target)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Unaligned, target);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Unaligned"/>: Indicates that an address currently atop the evaluation stack might not be aligned to the natural size of the immediately following ldind, stind, ldfld, stfld, ldobj, stobj, initblk, or cpblk instruction.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Unaligned"/> instruction to.
        /// </param>
        /// <param name="value">
        /// The value to load onto the stack.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.unaligned.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator unaligned(this ILGenerator il, long value)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Unaligned, value);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Unbox"/>: Converts the boxed representation of a value type to its unboxed form.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Unbox"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.unbox.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator unbox(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Unbox, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Unbox"/>: Converts the boxed representation of a value type to its unboxed form.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Unbox"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.unbox.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator unbox<T>(this ILGenerator il)
        {
            return unbox(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Unbox_Any"/>: Converts the boxed representation of a type specified in the instruction to its unboxed form.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Unbox_Any"/> instruction to.
        /// </param>
        /// <param name="type">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// <para>- or -</para>
        /// <para><paramref name="type"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.unbox_any.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator unbox_any(this ILGenerator il, Type type)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            if (type == null)
                throw new ArgumentNullException("type");
            il.Emit(OpCodes.Unbox_Any, type);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Unbox_Any"/>: Converts the boxed representation of a type specified in the instruction to its unboxed form.
        /// </summary>
        /// <typeparam name="T">
        /// The &lt;see cref="Type" /gt; to emit along with the opcode.
        /// </typeparam>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Unbox_Any"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.unbox_any.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator unbox_any<T>(this ILGenerator il)
        {
            return unbox_any(il, typeof(T));
        }

        /// <summary>
        /// <see cref="OpCodes.Volatile"/>: Specifies that an address currently atop the evaluation stack might be volatile, and the results of reading that location cannot be cached or that multiple stores to that location cannot be suppressed.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Volatile"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.volatile_.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator volatile_(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Volatile);

            return il;
        }

        /// <summary>
        /// <see cref="OpCodes.Xor"/>: Computes the bitwise XOR of the top two values on the evaluation stack, pushing the result onto the evaluation stack.
        /// </summary>
        /// <param name="il">
        /// The <see cref="ILGenerator"/> to emit the <see cref="OpCodes.Xor"/> instruction to.
        /// </param>
        /// <returns>
        /// The <see cref="ILGenerator"/> passed to the method in <paramref name="il"/>, to support a fluent interface.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <para><paramref name="il"/> is <c>null</c>.</para>
        /// </exception>
        /// <remarks>
        /// See <see href="http://msdn.microsoft.com/en-us/library/system.reflection.emit.opcodes.xor.aspx"/> for more information.
        /// </remarks>
		[DebuggerStepThrough]
        public static ILGenerator xor(this ILGenerator il)
        {
            if (il == null)
                throw new ArgumentNullException("il");
            il.Emit(OpCodes.Xor);

            return il;
        }
    }
}
