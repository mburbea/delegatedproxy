﻿using DelegatedProxy;
using DelegatedProxy.InvocationHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestData;

namespace TestData
{
	public class TestClassProxy<pR> : TestClass<pR>
	{
		private InvocationHandler<Func<TestClass<pR>, object>> _0_Test;
		private InvocationHandler<Func<TestClass<pR>, pR, GenericToken, object>> _1_Test;
		private static readonly InvocationHandler[] _proxy_handlers = new InvocationHandler[] { 
			new InvocationHandler<Func<TestClass<pR>, object>>(new Func<TestClass<pR>, object>(TestClassProxy<pR>.s_0_Test)), 
		new GenericInvocationHandler<Func<TestClass<pR>, pR, GenericToken, object>>(typeof(TestClassProxy<pR>), "s_1_Test") };



		public TestClassProxy(InvocationHandler[] handlers)
		{
			if (handlers == null)
			{
				throw new ArgumentNullException("handlers");
			}
			if (handlers.Length != 2)
			{
				throw new ArgumentException("Handlers needs to be an array of 2 parameters.", "handlers");
			}
			this._0_Test = (InvocationHandler<Func<TestClass<pR>, object>>)(handlers[0] ?? _proxy_handlers[0]);
			this._1_Test = (InvocationHandler<Func<TestClass<pR>, pR, GenericToken, object>>)(handlers[1] ?? _proxy_handlers[1]);
		}


		private object __0__Test()
		{
			return base.Test();
		}

		private object __1__Test<T>(pR local1) where T:IConvertible
		{
			return base.Test<T>(local1);
		}

		public static object s_0_Test(TestClass<pR> class1)
		{
			return ((TestClassProxy<pR>)class1).__0__Test();
		}

		public static object s_1_Test<T>(TestClass<pR> class1, pR local1) where T:IConvertible
		{
			return ((TestClassProxy<pR>)class1).__1__Test<T>(local1);
		}

		public override object Test()
		{
			return this._0_Test.Target(this);
		}

		public override object Test<T>(pR local1)
		{
			 return this._1_Test.Target(this, local1, GenericToken<T>.Token);
		}
	}
}
