﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestData
{
	public class TestClass<R>
	{
		public virtual object Test()
		{
			return default(object);
		}

		public virtual object Test<T>(R r) where T:IConvertible
		{
			return default(object);
		}
	}
}
