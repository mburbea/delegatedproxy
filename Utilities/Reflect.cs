﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Utilities
{
	/// <summary>
	/// Type-safe reflection utility class. 
	/// </summary>
	public static class Reflect
	{
		public static string GetMemberName<TRet>(Expression<Func<TRet>> expression)
		{
			return Reflect.GetMemberName((LambdaExpression)expression);
		}

		public static string GetMemberName<T, TRet>(Expression<Func<T, TRet>> expression)
		{
			return Reflect.GetMemberName((LambdaExpression)expression);
		}

		private static string GetMemberName(LambdaExpression expression)
		{
			MemberExpression memberExpression;
			ArgumentValidator.AssertIsNotNull(()=>expression);
			if ((memberExpression = expression.Body as MemberExpression) == null)
			{
				throw new ArgumentException(
					string.Format(CultureInfo.InvariantCulture, "Expected {0} with body of type {1} but got {2}.",
					              typeof (LambdaExpression), typeof (MemberExpression), expression.Body.GetType()),
					Reflect.GetMemberName(() => expression));
			}
			return memberExpression.Member.Name;
		}

 


	}
}
