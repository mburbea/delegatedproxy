﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reflection;

namespace Utilities
{
	/// <summary>
	/// A class to do typesafe argument validation, with a low invocation cost. 
	/// Using this class adds on average 1 extra nullity check and 1 delegate invocation.
	/// This is much more effecient then any method involving expression trees.
	/// </summary>
	public static class ArgumentValidator
	{
		#region Private Static Methods 
		/// <summary>
		/// Gets the name of the argument field.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="argument">The argument.</param>
		/// <returns>Argument field.</returns>
		private static string GetArgumentFieldName<TValue>(Func<TValue> argument)
		{
			if (argument.Target == null)
			{
				throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The specified argument of type {0} is not a function containing a method parameter.", argument.GetType()), Reflect.GetMemberName(() => argument));
			}
			try
			{
				int fieldHandle = BitConverter.ToInt32(argument.Method.GetMethodBody().GetILAsByteArray(), 2);
				return argument.Target.GetType().Module.ResolveField(fieldHandle, argument.Target.GetType().GetGenericArguments(), argument.Method.GetGenericArguments()).Name;
			}
			catch (ArgumentOutOfRangeException e)
			{
				throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The specified argument of type {0} is not a function containing a method parameter.", argument.GetType()), Reflect.GetMemberName(() => argument), e);
			}
		}
		#endregion Private Static Methods 

		#region Public Static Methods
		/// <summary>
		/// Asserts that the argument is positive.
		/// </summary>
		/// <param name="argument">The argument.</param>
		[DebuggerHidden]
		public static void AssertIsPositive(Func<int> argument)
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			if (argument() < 1 )
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} is not a positve number.", argument()), ArgumentValidator.GetArgumentFieldName(argument));
			}
		}
		/// <summary>
		/// Asserts that the argument is positive.
		/// </summary>
		/// <param name="argument">The argument.</param>
		[DebuggerHidden]
		public static void AssertIsPositive(Func<long> argument)
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			if (argument() < 1L)
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} is not a positve number.", argument()), ArgumentValidator.GetArgumentFieldName(argument));
			}
		}

		/// <summary>
		/// Asserts that the array is non-empty and that none of its elements are null. 
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="argument">The argument.</param>
		[DebuggerHidden]
		public static void AssertAllArrayElementsAreNonNull<T>(Func<T[]> argument) where T:class
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			var array = argument();
			int i;
			for (i = 0; i < array.Length; i++)
			{
				if (array[i] == default(T))
				{
					var name = ArgumentValidator.GetArgumentFieldName(argument);
					throw new ArgumentNullException(string.Format(CultureInfo.CurrentCulture, "{0}[{1}]",name,i));
				}
			}
			if (i == 1)
			{
				var name = ArgumentValidator.GetArgumentFieldName(argument);
				throw new ArgumentException("A non-empty array is required", name);
			}

		}

		/// <summary>
		/// Asserts that the array is non-empty and that none of its elements are null. 
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="argument">The argument.</param>
		[DebuggerHidden]
		public static void AssertAllArrayElementsAreNonNull<T>(Func<T?[]> argument) where T : struct
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			var array = argument();
			int i;
			for (i = 0; i < array.Length; i++)
			{
				if (!array[i].HasValue)
				{
					var name = ArgumentValidator.GetArgumentFieldName(argument);
					throw new ArgumentNullException(string.Format(CultureInfo.CurrentCulture, "{0}[{1}]", name, i));
				}
			}
			if (i == 1)
			{
				var name = ArgumentValidator.GetArgumentFieldName(argument);
				throw new ArgumentException("A non-empty array is required", name);
			}
		}

		/// <summary>
		/// Asserts the type is delegate.
		/// </summary>
		/// <param name="argument">The argument.</param>
		[DebuggerHidden]
		public static void AssertIsDelegateType(Func<Type> argument)
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			if (argument().BaseType != typeof(MulticastDelegate))
			{
				throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "{0} is not a delegate type.", argument()),ArgumentValidator.GetArgumentFieldName(argument));
			}
		}

		/// <summary>
		/// Asserts the generic method argument is a <see cref="System.Delegate"/>. 
		/// The lambda passed must be ()=&gt;typeof(T), where T is the generic argument. 
		/// </summary>
		/// <param name="argument">The argument.</param>
		/// <exception cref="ArgumentException">If the generic is not a <see cref="System.Delegate"/></exception>
		/// <exception cref="ArgumentNullException">If <paramref name="argument"/> is null or evaluates to null.</exception>
		[DebuggerHidden]
		public static void AssertGenericIsDelegateType(Func<Type> argument)
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			Type type;
			if ((type=argument()).BaseType != typeof(MulticastDelegate))
			{
				if (!argument.Method.IsGenericMethod)
				{
					var name = argument.Method.DeclaringType.GetGenericTypeDefinition().GetGenericArguments().Where((t,i)=>
						argument.Method.DeclaringType.GetGenericArguments()[i] == type).First().Name;
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} is not a delegate type.", type), name);
				}
					throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "{0} is not a delegate type.", type),
					                            argument.Method.GetGenericMethodDefinition().GetGenericArguments()[0].Name);
			}
		}
		/// <summary>
		/// Asserts that the argument is derived from the baseType.
		/// </summary>
		/// <param name="argument">The argument.</param>
		/// <param name="baseType">Type of the base.</param>
		[DebuggerHidden]
		public static void AssertIsDerivedFrom(Func<Type> argument,Type baseType)
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			ArgumentValidator.AssertIsNotNull(() => baseType);
			if(!baseType.IsAssignableFrom(argument()))
			{
				throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "{0} does not derive from  {1}.", argument(), baseType), ArgumentValidator.GetArgumentFieldName(argument));
			}
		}
		/// <summary>
		/// Asserts that the specified argument does not evaluate to null.
		/// Throws an exception of <see cref="T:System.ArgumentNullException"/> if this is not true.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="argument">The argument.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="argument"/> is null or evaluates to null.</exception>
		[DebuggerHidden]
		public static void AssertIsNotNull<TValue>(Func<TValue> argument)
			where TValue : class
		{
			if (argument == null)
			{
				throw new ArgumentNullException(Reflect.GetMemberName(() => argument));
			}
			if (argument() == default(TValue))
			{
				throw new ArgumentNullException(ArgumentValidator.GetArgumentFieldName(argument));
			}
		}

		/// <summary>
		/// Asserts that the specified parameter value is not null.
		/// Throws an exception of <see cref="T:System.ArgumentNullException"/> if this is not true.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="parameterValue">The parameter value.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="parameterValue"/> is null.</exception>
		[DebuggerHidden]
		public static void AssertIsNotNull<TValue>(TValue parameterValue, string parameterName)
			where TValue : class
		{
			if (parameterValue == default(TValue))
			{
				throw new ArgumentNullException(parameterName);
			}
		}

		/// <summary>
		/// Asserts that the specified argument does not evaluate to null.
		/// Throws an exception of <see cref="T:System.ArgumentNullException"/> if this is not true.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="argument">The argument.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="argument"/> is null or evaluates to null.</exception>
		[SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures"), DebuggerHidden]
		public static void AssertIsNotNull<TValue>(Func<TValue?> argument)
			where TValue : struct
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			if (!argument().HasValue)
			{
				throw new ArgumentNullException(ArgumentValidator.GetArgumentFieldName(argument));
			}
		}

		/// <summary>
		/// Asserts that the specified parameter value is not null.
		/// Throws an exception of <see cref="T:System.ArgumentNullException"/> if this is not true.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="parameterValue">The parameter value.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="parameterValue"/> is null.</exception>
		[DebuggerHidden]
		public static void AssertIsNotNull<TValue>(TValue? parameterValue, string parameterName)
			where TValue : struct
		{
			if (!parameterValue.HasValue)
			{
				throw new ArgumentNullException(parameterName);
			}
		}

		/// <summary>
		/// Asserts that the specified argument does not evaluate to null or the empty string.
		/// Throws an exception of either <see cref="T:System.ArgumentNullException"/> or <see cref="T:System.ArgumentException"/> if this is not true.
		/// </summary>
		/// <param name="argument">The argument.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="argument"/> is null or evaluates to null.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="argument"/> evaluates to a string of zero length.</exception>
		[DebuggerHidden]
		public static void AssertIsNotNullOrEmpty(Func<string> argument)
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			string parameterValue = argument();
			if (parameterValue == null)
			{
				throw new ArgumentNullException(ArgumentValidator.GetArgumentFieldName(argument));
			}
			if (parameterValue.Length == 0)
			{
				throw new ArgumentException("{0} cannot be empty.", ArgumentValidator.GetArgumentFieldName(argument));
			}
		}

		/// <summary>
		/// Asserts that the specified string value is not null or empty.
		/// Throws an exception of either <see cref="T:System.ArgumentNullException"/> or <see cref="T:System.ArgumentException"/> if this is not true.
		/// </summary>
		/// <param name="parameterValue">The parameter value.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="parameterValue"/> is null.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="parameterValue"/> is a string of zero length.</exception>
		[DebuggerHidden]
		public static void AssertIsNotNullOrEmpty(string parameterValue, string parameterName)
		{
			ArgumentValidator.AssertIsNotNull(parameterValue, parameterName);
			if (parameterValue.Length == 0)
			{
				throw new ArgumentException("{0} cannot be empty.", parameterName);
			}
		}

		/// <summary>
		/// Asserts that the specified string value is not null or a whitespace.
		/// Throws an exception of either <see cref="T:System.ArgumentNullException"/> or <see cref="T:System.ArgumentException"/> if this is not true.
		/// </summary>
		/// <param name="argument">The argument.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="parameterValue"/> is null.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="argument"/> evaluates to a whitespace string.</exception>
		[DebuggerHidden]
		public static void AssertIsNotNullOrWhiteSpace(Func<string> argument)
		{
			ArgumentValidator.AssertIsNotNull(() => argument);
			string parameterValue = argument();
			if (parameterValue == null)
			{
				throw new ArgumentNullException(ArgumentValidator.GetArgumentFieldName(argument));
			}
			for (int i = 0; i < parameterValue.Length; i++)
			{
				if (!char.IsWhiteSpace(parameterValue[i])) return;
			}
			throw new ArgumentException("{0} cannot be whitespace.", ArgumentValidator.GetArgumentFieldName(argument));
		}

		/// <summary>
		/// Asserts that the specified string value is not null or a whitespace.
		/// Throws an exception of either <see cref="T:System.ArgumentNullException"/> or <see cref="T:System.ArgumentException"/> if this is not true.
		/// </summary>
		/// <param name="parameterValue">The parameter value.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="parameterValue"/> is null.</exception>
		/// <exception cref="T:System.ArgumentException">The <paramref name="parameterValue"/> is whitespace.</exception>
		[DebuggerHidden]
		public static void AssertIsNotNullOrWhiteSpace(string parameterValue, string parameterName)
		{
			if (parameterValue == null)
			{
				throw new ArgumentNullException(parameterName);
			}
			for (int i = 0; i < parameterValue.Length; i++)
			{
				if (!char.IsWhiteSpace(parameterValue[i])) return;
			}
			throw new ArgumentException("{0} cannot be whitespace.", parameterName);
		}

		/// <summary>
		/// Asserts that the specified <paramref name="projection"/> applied to the specified <paramref name="item"/> will yield the specified <paramref name="key"/>.
		/// Throws an exception of <see cref="T:System.ArgumentNullException"/> if this is not true.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TItem">The type of the item.</typeparam>
		/// <param name="projection">The projection.</param>
		/// <param name="item">The item.</param>
		/// <param name="key">The key.</param>
		/// <param name="comparer">The comparer.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="projection"/> is null.</exception>
		[DebuggerHidden]
		public static void AssertProjection<TKey, TItem>(Func<TItem, TKey> projection, TItem item, TKey key, IEqualityComparer<TKey> comparer = null)
		{
			ArgumentValidator.AssertIsNotNull(() => projection);
			if (!(comparer ?? EqualityComparer<TKey>.Default).Equals(key, projection(item)))
			{
				throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The specified key \"{0}\" does not match the key produced by the projection.", key), Reflect.GetMemberName(()=>key));
			}
		}
		#endregion Public Static Methods 
	}
}
