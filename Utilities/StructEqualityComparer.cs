﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Reflective;

namespace Utilities
{
	internal static class AssemblyGen
	{
		internal static readonly ModuleBuilder ModuleBuilder = AppDomain.CurrentDomain
			   .DefineDynamicAssembly(new AssemblyName(MethodBase.GetCurrentMethod().DeclaringType.Name), AssemblyBuilderAccess.RunAndCollect)
			   .DefineDynamicModule(MethodBase.GetCurrentMethod().DeclaringType.Name);
	}

	static class StructEqualityComparer
	{
		internal static readonly Type OpenGenericType = StructEqualityComparer.CreateStructEqualityComparer();
		/// <summary>
		/// Builds the bool Equals(T,T) method. The method uses the built in equality comparer
		/// for value types (ceq). The code might look like::
		/// (T x,T y)=> x==y;
		/// </summary>
		/// <param name="typeBuilder">The type builder.</param>
		/// <param name="genericArg">The generic arg.</param>
		private static void BuildEqualsMethod(TypeBuilder typeBuilder, Type genericArg)
		{
			typeBuilder.DefineMethod(
				"Equals",
				MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.NewSlot | MethodAttributes.HideBySig | MethodAttributes.Final,
				CallingConventions.HasThis,
				typeof(bool),
				new[] { genericArg, genericArg }
			).GetILGenerator()
				.ldarg_1()
				.ldarg_2()
				.ceq()
				.ret();
		}

		/// <summary>
		/// Builds the Int32 GetHashCode(T,T). The method will look like::
		/// (T x)=&gt;x.GetHashCode()
		/// </summary>
		/// <param name="typeBuilder">The type builder.</param>
		/// <param name="genericArg">The generic arg.</param>
		private static void BuildGetHashCodeMethod(TypeBuilder typeBuilder, Type genericArg)
		{
			typeBuilder.DefineMethod(
				"GetHashCode",
				MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.NewSlot | MethodAttributes.HideBySig,
				CallingConventions.HasThis,
				typeof(Int32),
				new[] { genericArg }
			).GetILGenerator()
				.ldarga_s(1)
				.constrained(genericArg)
				.callvirt(typeof(object).GetMethod("GetHashCode"))
				.ret();
		}

		/// <summary>
		/// Creates a struct comparer type, for use by the GetEqualityComparer&lt;TEnum&gt; method. 
		/// Since we use a generic type we get the benefit of reusability. Since it's dynamic we only build it by need.
		/// </summary>
		/// <returns></returns>
		private static Type CreateStructEqualityComparer()
		{
			TypeBuilder typeBuilder = AssemblyGen.ModuleBuilder.DefineType("StructEqualityComparer`1",
					TypeAttributes.AnsiClass | TypeAttributes.AutoClass | TypeAttributes.Class | TypeAttributes.Public
			);
			var genericArg = typeBuilder.DefineGenericParameters("T")[0];
			genericArg.SetGenericParameterAttributes(GenericParameterAttributes.NotNullableValueTypeConstraint);
			typeBuilder.AddInterfaceImplementation(typeof(IEqualityComparer<>).MakeGenericType(genericArg));
			StructEqualityComparer.BuildEqualsMethod(typeBuilder, genericArg);
			StructEqualityComparer.BuildGetHashCodeMethod(typeBuilder, genericArg);
			return typeBuilder.CreateType();
		}
	}
	public static class StructEqualityComparer<T> where T:struct
	{
		public static readonly IEqualityComparer<T> Default = Activator.CreateInstance(StructEqualityComparer.OpenGenericType.MakeGenericType(typeof(T)))
			as IEqualityComparer<T>;
	}
}


