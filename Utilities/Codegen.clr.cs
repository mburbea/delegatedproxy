﻿// Silverlight doesn't support private reflection so the IMPL does nothing in SL. 
#if !SILVERLIGHT
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Reflective;

namespace Utilities
{
	public static partial class CodeGen
	{

		private static Action<ILGenerator> _updateMaxStacksize;
		static partial void UpdateMaxStackImpl(ILGenerator generator)
		{
			if (_updateMaxStacksize == null)
			{
				Label exit;
				var fieldinfo = typeof(ILGenerator).GetField("m_maxStackSize", BindingFlags.NonPublic | BindingFlags.Instance);
				_updateMaxStacksize = CodeGen.CreateDelegate<Action<ILGenerator>>(
					gen => gen
						.define_label(out exit)
						.ldarg_1()
						.ldfld(fieldinfo)
						.ldc_i4_8()
						.bge_s(exit)
						.ldarg_1()
						.ldc_i4_8()
						.stfld(fieldinfo)
						.mark_label(exit)
						.ret(), restrictedSkipVisibility: true,
						name: "UpdateMaxStackSize"
				);
			}
			_updateMaxStacksize(generator);
		}
	}
}
#endif
