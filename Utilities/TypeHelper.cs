﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities.Extensions
{
	public static class TypeHelper
	{
		internal static Type GetRootElementType(this Type @this)
		{
			Type elementType = @this;
			while (elementType.HasElementType)
			{
				elementType = elementType.GetElementType();
			}
			return elementType;
		}

		private static bool ContainsMethodOrTypeGenericParametersImpl(Type type, bool searchMethod)
		{
			if (type == null) throw new ArgumentNullException("type");
			if (type.IsGenericParameter)
			{
				return searchMethod ? type.DeclaringMethod != null : type.DeclaringType != null;
			}
			else if (type.HasElementType)
			{
				return ContainsMethodOrTypeGenericParametersImpl(type.GetRootElementType(), searchMethod);
			}
			else if (type.IsGenericType)
			{
				var genericArguments = type.GetGenericArguments();
				for (int i = 0; i < genericArguments.Length; i++)
				{
					if (ContainsMethodOrTypeGenericParametersImpl(genericArguments[i], searchMethod)) return true;
				}
			}
			return false;
		}
		public static bool ContainsMethodGenericParameters(this Type type)
		{
			return ContainsMethodOrTypeGenericParametersImpl(type, true);
		}
		public static bool ContainsTypeGenericParameters(this Type type)
		{
			return ContainsMethodOrTypeGenericParametersImpl(type, false);
		}
		public static bool IsAssignmentCompatibleWith(this Type current, Type target, bool allowEnumToBaseType = false)
		{
			Type currentUnderlying = null;
			Type targetUnderlying = null;
			return (current.IsAssignableFrom(target) &&
					current.IsValueType == target.IsValueType)
					|| (allowEnumToBaseType && // this appears to be a bug in .NET, but we should support/abuse bugs too.
							(
								(target.IsEnum && (targetUnderlying = target.GetEnumUnderlyingType()) == current) ||
								(current.IsEnum && (currentUnderlying =current.GetEnumUnderlyingType()) == target) ||
								(currentUnderlying != null && currentUnderlying == targetUnderlying)
							)
					);
		}
	}
}
