﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using Reflective;

namespace Utilities
{
	/// <summary>
	/// A class that hold extension methods for Enums.
	/// </summary>
	public static class EnumHelper
	{
		#region Private Static Variables

		#pragma warning disable 169
		private static Func<Byte, Byte, bool> Byte;
		private static Func<SByte, SByte, bool> SByte;
		private static Func<Int16, Int16, bool> Int16;
		private static Func<UInt16, UInt16, bool> UInt16;
		private static Func<Int32, Int32, bool> Int32;
		private static Func<UInt32, UInt32, bool> UInt32;
		private static Func<Int64, Int64, bool> Int64;
		private static Func<UInt64, UInt64, bool> UInt64;
		#pragma warning restore 169

        internal static readonly Type OpenEqualityComparer = EnumHelper.CreateEqualityComparerType();

		#endregion Private Static Variables

		#region Private Static Methods
		/// <summary>
		/// Builds the int GetHashCode(TEnum) method. The method generated will roughly look like this
		/// (TEnum value)=&gt;((TUnderlying)TEnum).GetHashCode();
		/// </summary>
		/// <param name="typeBuilder">The type builder.</param>
		/// <param name="enumType">Type of the enum.</param>
		/// <param name="underlyingType">The t underlying.</param>
		private static void BuildGetHashCodeMethod(TypeBuilder typeBuilder, Type enumType,Type underlyingType)
		{
			var ilgen = typeBuilder.DefineMethod(
				"GetHashCode",
				MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.Final,
				CallingConventions.HasThis,
				typeof(Int32),
				new[] {enumType }
			).GetILGenerator()
				.ldarga_s(1)
				.constrained(underlyingType)
				.callvirt(typeof(object).GetMethod("GetHashCode", Type.EmptyTypes))
				.ret();
		}

		/// <summary>
		/// Creates the EnumEqualityComparer type, for use by the GetEqualityComparer&lt;TEnum&gt; method. 
		/// Since we use a generic type we get the benefit of reusability. Since it's dynamic we only build it by need.
		/// </summary>
		/// <returns></returns>
		private static Type CreateEqualityComparerType()
		{
			var typeBuilder = AssemblyGen.ModuleBuilder.DefineType("EnumEqualityComparer`2",
							TypeAttributes.AnsiClass | TypeAttributes.AutoClass | TypeAttributes.Class | TypeAttributes.Public
				);
			var genericArgs = typeBuilder.DefineGenericParameters("TEnum","TUnderlying");
			var enumType = genericArgs[0];
			var underlyingType = genericArgs[1];
			enumType.SetBaseTypeConstraint(typeof(Enum));
			enumType.SetGenericParameterAttributes(GenericParameterAttributes.NotNullableValueTypeConstraint);

			underlyingType.SetGenericParameterAttributes(GenericParameterAttributes.NotNullableValueTypeConstraint);
			
			typeBuilder.SetParent(StructEqualityComparer.OpenGenericType.MakeGenericType(enumType));
			BuildGetHashCodeMethod(typeBuilder, enumType,underlyingType);
			return typeBuilder.CreateType();
		}
		#endregion Private Static Methods

		#region Public Static Methods
		/// <summary>
		/// Determines whether the specified value has the flag mentioned. Note this method is up to 60 times faster
		/// than the one that comes with .NET 4 as it avoids any explict boxing or unboxing. 
		/// </summary>
		/// <typeparam name="TEnum">The type of the enum.</typeparam>
		/// <param name="value">The value.</param>
		/// <param name="flag">The flag.</param>
		/// <returns>
		/// 	<c>true</c> if the specified value has flags; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="ArgumentException">If TEnum is not an enum.</exception>
		public static bool ContainsFlag<TEnum>(this TEnum value, TEnum flag)
			where TEnum : struct,IComparable, IConvertible, IFormattable
		{
			return Cache<TEnum>.ContainsFlagsDelegate(value, flag);
		}
		#endregion Public Static Methods

		#region Nested Classes

		public static class Cache<TEnum>
			where TEnum : struct,IComparable, IConvertible, IFormattable
		{
			#region Public Static Variables
			public static readonly Func<TEnum, TEnum, bool> ContainsFlagsDelegate = CreateContainsFlagsDelegate();
			#endregion Public Static Variables

			#region Private Static Methods
			/// <summary>
			/// Creates the contains flags delegate. This is done by using the fact that Delegate.CreateDelegate
			/// allows the conversion from Enum to underlying type. We generate one delegate for each of the underlying
			/// types lazily. Then for every new delegate we discover we use Delegate.CreateDelegate to simply convert
			/// from underlying type to enum. Fairly straightforward, we get the benefits of lazy implementation, without
			/// creating a large amount of non-reusable deadcode. 
			/// </summary>
			/// <returns>Delegate</returns>
			private static Func<TEnum, TEnum, bool> CreateContainsFlagsDelegate()
			{
				var field = typeof(EnumHelper).GetField(
					Type.GetTypeCode(typeof(TEnum)).ToString(),
					BindingFlags.NonPublic | BindingFlags.Static
				);
				var @delegate = field.GetValue(null) as Delegate;
				if (@delegate == null)
				{
					var underlyingType = Enum.GetUnderlyingType(typeof (TEnum));
					var target = Expression.Parameter(underlyingType, "target");
					var flag = Expression.Parameter(underlyingType, "flag");
					bool convertToInt = Marshal.SizeOf(underlyingType) < 4;
					Converter<Expression, Expression> convertIfNeeded = exp =>
					                                                    convertToInt
					                                                    	? Expression.Convert(exp, typeof (int))
					                                                    	: exp;

					var expression = Expression.Lambda(
						Expression.Equal(
							Expression.And(
								convertIfNeeded(target),
								convertIfNeeded(flag)
								),
							convertIfNeeded(flag)
							),
						target,
						flag
						);
					@delegate = expression.Compile();
					field.SetValue(null, @delegate);
				}
				return DelegateHelper.ConvertDelegate<Func<TEnum, TEnum, bool>>(@delegate);
			}
			#endregion Private Static Methods
		}
		#endregion Nested Classes
	}
	public static class EnumEqualityComparer<TEnum>
	{
		public static readonly IEqualityComparer<TEnum> Default = Activator.CreateInstance(EnumHelper.OpenEqualityComparer.MakeGenericType(typeof(TEnum),Enum.GetUnderlyingType(typeof(TEnum)))) as IEqualityComparer<TEnum>;
	}
}