﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Linq.Expressions;
using Reflective;
namespace Utilities
{
	public static class ConverterCache<TIn,TOut>
	{
		private static readonly Func<TIn, TOut> _converter;
		public static Func<TIn, TOut> Converter {get;set;}
		/// <summary>
		/// Builds the converter.
		/// </summary>
		/// <returns></returns>
		private static Func<TIn,TOut> BuildConverter()
		{
			if (!typeof(IConvertible).IsAssignableFrom(typeof(TIn)))
			{
				return ThrowInvalidCastDelegate; // we don't care!
			}
			if(typeof(TIn).IsPrimitive)
			{
				var methodinfo = typeof (Convert).GetMethods(BindingFlags.Static | BindingFlags.Public).FirstOrDefault(
					m => m.GetParameters()[0].ParameterType == typeof (TIn) && m.ReturnType == typeof (TOut));
			}
			//if(methodinfo != null)
			//{
			//    return DelegateHelper.CreateDelegate<Func<TIn,TOut>>(methodinfo);
			//}
			//Func<TIn, TOut> func =			var parameter = Expression.Parameter(typeof(TIn));
			
			throw new NotImplementedException();
		}

		private static Func<TIc,TOut> CallConvert<TIc>() where TIc:IConvertible
		{
			Expression<Func<TIc,TOut>> expr= c=>(TOut)c.ToType(typeof(TOut),null);
			return expr.Compile();
		}

		/// <summary>
		/// Throws the invalid cast delegate. If we don't do this here we get a type init exception and I'd rather get an InvalidCastException.
		/// </summary>
		/// <returns></returns>
		private static TOut ThrowInvalidCastDelegate(TIn parameter)
		{
			throw new InvalidCastException(string.Format("{0} is not an IConvertible", typeof(TIn).Name));
		}
	}
}
