﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using Utilities.Extensions;
namespace Utilities
{
	/// <summary>
	/// A class to facilitate using the factory methods on the static class Delegate. Also includes
	/// helper methods for delegates.
	/// </summary>
	public static class DelegateHelper
	{
		#region Private Static Variables 
		private static Type s_RTDynamicMethodType = Type.GetType("System.Reflection.Emit.DynamicMethod+RTDynamicMethod",false,true);
		private static Func<MethodInfo, DynamicMethod> s_GetDynamicMethodDelegate = CreateGetDynamicMethodDelegate();
		private static Func<Delegate, object, Delegate> ChangeDelegateTargetDelegate = CreateChangeDelegateTargetDelegate();
		#endregion Private Static Variables 

		#region Private Static Methods 
		/// <summary>
		/// Determines if the delegate invocation method are compatible with each other. Note supports the conversion
		/// from Enum->BaseType that delegate.CreateDelegate supports.
		/// </summary>
		/// <param name="currentMethod">The current method.</param>
		/// <param name="targetMethod">The target method.</param>
		/// <param name="allowEnumToBaseType">if set to <c>true</c> allow enum to base type.</param>
		/// <returns></returns>
		private static bool AreDelegateInvokeMethodsCompatible(MethodInfo currentMethod, MethodInfo targetMethod, bool allowEnumToBaseType)
		{
			var currentParams = currentMethod.GetParameters();
			var targetParams = targetMethod.GetParameters();
			if (targetParams.Length != currentParams.Length ||
				!targetMethod.ReturnType.IsAssignmentCompatibleWith(currentMethod.ReturnType, allowEnumToBaseType))
			{
				return false;
			}
			for (int i = 0; i < targetParams.Length; i++)
			{
				if (!DelegateHelper.AreParameterInfosCompatible(currentParams[i], targetParams[i], allowEnumToBaseType)) return false;
			}
			return true;

		}

		/// <summary>
		/// Ares the parameter infos compatible. Supports the implicit Enum->BaseType conversion that Delegate.CreateDelegate
		/// supports.
		/// </summary>
		/// <param name="current">The current.</param>
		/// <param name="target">The target.</param>
		/// <param name="allowEnumToBaseType">if set to <c>true</c> allow enum to base type.</param>
		/// <returns></returns>
		private static bool AreParameterInfosCompatible(ParameterInfo current, ParameterInfo target, bool allowEnumToBaseType)
		{
			return current.ParameterType.IsAssignmentCompatibleWith(target.ParameterType, allowEnumToBaseType)
				&& (target.IsOut == current.IsOut); 
		}

		/// <summary>
		/// Creates the change delegate target delegate.
		/// </summary>
		/// <returns></returns>
		private static Func<Delegate, object, Delegate> CreateChangeDelegateTargetDelegate()
		{
			var delegateExpression = Expression.Parameter(typeof(Delegate), "delegate");
			var targetExpression = Expression.Parameter(typeof(object), "target");
			var expression = Expression.Lambda<Func<Delegate, object, Delegate>>(
				Expression.Block(
					Expression.Assign(
						delegateExpression,
						Expression.TypeAs(
							Expression.Call(
								delegateExpression,
								typeof(Object).GetMethod("MemberwiseClone", BindingFlags.NonPublic | BindingFlags.Instance)
							),
							typeof(Delegate)
						)
					),
					Expression.Assign(
						Expression.Field(
							delegateExpression,
							typeof(Delegate).GetField("_target", BindingFlags.NonPublic | BindingFlags.Instance)
						),
						targetExpression
					),
					delegateExpression
				),
				delegateExpression,
				targetExpression
			);
			return expression.Compile();
		}

		/// <summary>
		/// Creates the get dynamic method delegate.
		/// </summary>
		/// <returns></returns>
		private static Func<MethodInfo, DynamicMethod> CreateGetDynamicMethodDelegate()
		{
			var param = Expression.Parameter(
							typeof(MethodInfo),
							typeof(MethodInfo).Name
			);
			var expression = Expression.Lambda<Func<MethodInfo, DynamicMethod>>(
				Expression.Field(
					Expression.Convert(
						param,
						s_RTDynamicMethodType
					),
					s_RTDynamicMethodType.GetField("m_owner", BindingFlags.NonPublic | BindingFlags.Instance)
				),
				param
				);
			return expression.Compile();
		}
		#endregion Private Static Methods 

		#region Public Static Methods 
		/// <summary>
		/// Changes the delegate target.
		/// </summary>
		/// <param name="delegate">The @delegate.</param>
		/// <param name="newTarget">The new target.</param>
		/// <returns></returns>
		public static Delegate ChangeDelegateTarget(Delegate @delegate, object newTarget)
		{
			ArgumentValidator.AssertIsNotNull(() => @delegate);
			ArgumentValidator.AssertIsNotNull(() => newTarget);
			if (@delegate.Target == null)
			{
				throw new ArgumentException("Must be a closed delegate.",Reflect.GetMemberName(()=>@delegate));
			}
			return ChangeDelegateTargetDelegate(@delegate,newTarget);
		}

		/// <summary>
		/// Changes the delegate target.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="delegate">The @delegate.</param>
		/// <param name="newTarget">The new target.</param>
		/// <returns></returns>
		public static TDelegate ChangeDelegateTarget<TDelegate>(TDelegate @delegate, object newTarget) where TDelegate:class
		{
			ArgumentValidator.AssertGenericIsDelegateType(() => typeof(TDelegate));
			return ChangeDelegateTarget(@delegate as Delegate, newTarget) as TDelegate;
		}

		/// <summary>
		/// Combines the invocations of the two delegates.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="arg1">The arg1.</param>
		/// <param name="arg2">The arg2.</param>
		/// <returns></returns>
		public static TDelegate Combine<TDelegate>(TDelegate arg1, TDelegate arg2) where TDelegate : class
		{
			return Delegate.Combine(arg1 as Delegate, arg2 as Delegate) as TDelegate;
		}

		/// <summary>
		/// Combines the invocation lists of the array of delegates.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public static TDelegate Combine<TDelegate>(params TDelegate[] args) where TDelegate : class
		{
			return Delegate.Combine(args as Delegate[]) as TDelegate;
		}

		/// <summary>
		/// Converts the delegate to <see cref="TDelegate"/>, provided the types are compatible. Can use
		/// variance of delegate typing in .NET 4 for a speedy conversion. Otherwise, uses Delegate.CreateDelegate
		/// to create a new delegate with the appropriate signature. 
		/// </summary>
		/// <typeparam name="TDelegate">The target delegate type.</typeparam>
		/// <param name="delegate">The @delegate.</param>
		/// <returns>TDelegate</returns>
		public static TDelegate ConvertDelegate<TDelegate>(Delegate @delegate) where TDelegate : class
		{
			ArgumentValidator.AssertIsNotNull(() => @delegate);
			var targetType = typeof(TDelegate);
			ArgumentValidator.AssertIsDelegateType(() => targetType);
			var currentType = @delegate.GetType();
			if (targetType.IsAssignableFrom(currentType))
			{
				return @delegate as TDelegate; // let's skip as much of this as we can.
			}
			
			var currentMethod = currentType.GetMethod("Invoke");
			var targetMethod = targetType.GetMethod("Invoke");
			if (!AreDelegateInvokeMethodsCompatible(currentMethod, targetMethod, true))
			{
				throw new ArgumentException(string.Format("{0} is incompatible with {1}.", currentType, targetType), Reflect.GetMemberName(() => @delegate));
			}
			return DelegateHelper.Combine(@delegate.GetInvocationList()
				.ConvertAll(d => IsMethodRunTimeGenerated(d.Method) ?
							GetDynamicMethodFromMethodInfo(d.Method).CreateDelegate<TDelegate>(d.Target) :
							DelegateHelper.CreateDelegate<TDelegate>(d.Target, d.Method)).ToArray());
		}

		/// <summary>
		/// Creates the delegate, binding to the method.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="method">The method.</param>
		/// <returns></returns>
		public static TDelegate CreateDelegate<TDelegate>(MethodInfo method) where TDelegate: class
		{
			return Delegate.CreateDelegate(typeof(TDelegate), method) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="method">The method.</param>
		/// <param name="throwOnBindFailure">if set to <c>true</c> [throw on bind failure].</param>
		/// <returns>TDelegate</returns>
		public static TDelegate CreateDelegate<TDelegate>(MethodInfo method, bool throwOnBindFailure) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate),method, throwOnBindFailure) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="firstArgument">The first argument.</param>
		/// <param name="method">The method.</param>
		/// <returns>TDelegate</returns>
		public static TDelegate CreateDelegate<TDelegate>(object firstArgument, MethodInfo method) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate),firstArgument,method) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="target">The target.</param>
		/// <param name="method">The method.</param>
		/// <returns>TDelegate</returns>
		public static TDelegate CreateDelegate<TDelegate>(object target, string method) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate),target,method) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="target">The target.</param>
		/// <param name="method">The method.</param>
		/// <returns>TDelegate</returns>
		public static TDelegate CreateDelegate<TDelegate>(Type target, string method) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate),target,method) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="firstArgument">The first argument.</param>
		/// <param name="method">The method.</param>
		/// <param name="throwOnBindFailure">if set to <c>true</c> [throw on bind failure].</param>
		/// <returns>TDelegate</returns>
		public static TDelegate CreateDelegate<TDelegate>(object firstArgument, MethodInfo method, bool throwOnBindFailure) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate), firstArgument,method,throwOnBindFailure) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="target">The target.</param>
		/// <param name="method">The method.</param>
		/// <param name="ignoreCase">if set to <c>true</c> [ignore case].</param>
		/// <returns></returns>
		public static TDelegate CreateDelegate<TDelegate>(object target, string method, bool ignoreCase) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate),target,method,ignoreCase) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="target">The target.</param>
		/// <param name="method">The method.</param>
		/// <param name="ignoreCase">if set to <c>true</c> [ignore case].</param>
		/// <returns></returns>
		public static TDelegate CreateDelegate<TDelegate>(Type target, string method, bool ignoreCase) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate),target, method, ignoreCase) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="target">The target.</param>
		/// <param name="method">The method.</param>
		/// <param name="ignoreCase">if set to <c>true</c> [ignore case].</param>
		/// <param name="throwOnBindFailure">if set to <c>true</c> [throw on bind failure].</param>
		/// <returns></returns>
		public static TDelegate CreateDelegate<TDelegate>(object target, string method, bool ignoreCase, bool throwOnBindFailure) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate),target,method,ignoreCase,throwOnBindFailure) as TDelegate;
		}

		/// <summary>
		/// Creates the delegate.
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="target">The target.</param>
		/// <param name="method">The method.</param>
		/// <param name="ignoreCase">if set to <c>true</c> [ignore case].</param>
		/// <param name="throwOnBindFailure">if set to <c>true</c> [throw on bind failure].</param>
		/// <returns></returns>
		public static TDelegate CreateDelegate<TDelegate>(Type target, string method, bool ignoreCase, bool throwOnBindFailure) where TDelegate : class
		{
			return Delegate.CreateDelegate(typeof(TDelegate), target, method, ignoreCase, throwOnBindFailure) as TDelegate;
		}

#if !SILVERLIGHT
		/// <summary>
		/// Gets the dynamic method from method info.
		/// </summary>
		/// <param name="method">The method.</param>
		/// <returns></returns>
		internal static DynamicMethod GetDynamicMethodFromMethodInfo(MethodInfo method)
		{
			if (!IsMethodRunTimeGenerated(method))
			{
				throw new ArgumentException(string.Format("{0} is not a {1}.", method.GetType(), s_RTDynamicMethodType), Reflect.GetMemberName(() => method));
			}
			return s_GetDynamicMethodDelegate(method);
		}

		/// <summary>
		/// Determines if the method is run time generated
		/// </summary>
		/// <param name="method">The method.</param>
		/// <returns>
		///   <c>true</c> if runtime generated; otherwise, <c>false</c>.
		/// </returns>
		internal static bool IsMethodRunTimeGenerated(MethodInfo method)
		{
			return s_RTDynamicMethodType.IsInstanceOfType(method);
		}
#endif
		#endregion Public Static Methods 
 	}
}
