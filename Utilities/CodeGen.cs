﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Reflective;
using Utilities.Extensions;

namespace Utilities
{
	public static partial class CodeGen
	{
		static string SanitizeName(string name)
		{
			return FORBIDDEN_CHARS.Aggregate(name, (n, c) => n.Replace(c, SAFE_CHAR));
		}
		const string FORBIDDEN_CHARS = @"+[]*&,\";
		const char SAFE_CHAR = '_';
		/// <summary>
		/// Creates an anonymously hosted Delegate using the <typeparamref name="TDelegate"/> invoke method's signature to
		/// determine the parameters closed over a target. 
		/// </summary>
		/// <typeparam name="TDelegate">The type of the delegate.</typeparam>
		/// <param name="genFunc">The generating function.</param>
		/// <param name="name">The name of the method useful for debugging purposes.</param>
		/// <param name="target">The target.</param>
		/// <param name="restrictedSkipVisibility">toggles the restricted skip visibility flag</param>
		/// <returns>The delegate</returns>
		public static TDelegate CreateDelegate<TDelegate>(Action<ILGenerator> genFunc, string name = "", object target = null,
		   bool restrictedSkipVisibility = false) where TDelegate : class
		{
			return CreateDelegate(typeof(TDelegate), genFunc, name, target, restrictedSkipVisibility) as TDelegate;
		}

		/// <summary>
		/// Creates an anonymously hosted Delegate using the <paramref name="delegateType"/> invoke method's signature to
		/// determine the parameters closed over a target.
		/// </summary>
		/// <param name="delegateType">Type of the delegate.</param>
		/// <param name="genFunc">The generating function.</param>
		/// <param name="name">The name of the method useful for debugging purposes.</param>
		/// <param name="target">The target.</param>
		/// <param name="restrictedSkipVisibility">toggles the restricted skip visibility flag</param>
		/// <returns>The delegate</returns>
		public static Delegate CreateDelegate(Type delegateType, Action<ILGenerator> genFunc, string name = "",
			object target = null, bool restrictedSkipVisibility = false)
		{
			if (delegateType == null) throw new ArgumentNullException(Reflect.GetMemberName(() => delegateType));
			if (genFunc == null) throw new ArgumentNullException(Reflect.GetMemberName(() => genFunc));
			if (delegateType.BaseType != typeof(MulticastDelegate))
			{
				throw new ArgumentException(
					String.Format(CultureInfo.InvariantCulture, "{0} is not a delegateType", delegateType),
					Reflect.GetMemberName(() => delegateType)
					);
			}

			var invokeMethod = delegateType.GetMethod("Invoke");
			var @params = invokeMethod.GetParameters();
			var paramTypes = new Type[@params.Length + 1];
			paramTypes[0] = target != null ? 
								target.GetType().IsValueType ? 
									target.GetType().IsEnum ? typeof(Enum) : typeof(ValueType)
								: target.GetType() 
							: typeof(object);

			@params.Select(p => p.ParameterType).CopyTo(paramTypes, 1);
			var method = new DynamicMethod(name = (name ?? ""), invokeMethod.ReturnType, paramTypes, restrictedSkipVisibility);
			genFunc(method.GetILGenerator());
			BuildGlobalMethodForGenerator(delegateType, genFunc, name.Length == 0 ? _assemblyCount++.ToString() : name  , invokeMethod.ReturnType, paramTypes);
			return method.CreateDelegate(delegateType, target);
		}

		private static int _assemblyCount = 0;

		[Conditional("DEBUG")]
		public static void BuildGlobalMethodForGenerator(Type delegateType, Action<ILGenerator> genFunc, string name,
			Type returnType, Type[] parameters)
		{
			var assemblyGen = AppDomain.CurrentDomain.DefineDynamicAssembly(
				new AssemblyName("CGA_" + SanitizeName(name)),
				AssemblyBuilderAccess.RunAndSave
				);
			var moduleBuilder = assemblyGen.DefineDynamicModule(
				assemblyGen.GetName().Name,
				assemblyGen.GetName().Name + ".dll",
				true
				);
			var globalMethod = moduleBuilder.DefineGlobalMethod(
				assemblyGen.GetName().Name+".Impl",
					MethodAttributes.Public | MethodAttributes.Static | MethodAttributes.HideBySig,
					CallingConventions.Standard,
					returnType,
					parameters
				);
			genFunc(
				globalMethod.GetILGenerator()
				);
			moduleBuilder.CreateGlobalFunctions();
			assemblyGen.Save(assemblyGen.GetName().Name + ".dll");
		}

		/// <summary>
		/// Updates the max stack to the default of 8. ILGenerator tends to be
		/// a little too conservative and anything less than 8 causes some performance
		/// implications. 
		/// </summary>
		/// <param name="generator">The generator.</param>
		public static void UpdateMaxStack(ILGenerator generator)
		{
			UpdateMaxStackImpl(generator);
		}

		static partial void UpdateMaxStackImpl(ILGenerator generator);


	}

}
