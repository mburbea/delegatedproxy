﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities.Extensions
{
	public static partial class CollectionExtensions
	{
		/// <summary>
		/// Removes the last element in an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array">The array.</param>
		/// <returns></returns>
		public static T[] RemoveLast<T>(this T[] array)
		{
			var targetArray = new T[array.Length - 1];
			Array.Copy(array, 0, targetArray, 0, targetArray.Length);
			return targetArray;
		}

		public static void CopyTo<T>(this IEnumerable<T> enumerable, T[] array,int startIndex)
		{
			ICollection<T> collection; 
			ICollection collectionbase;
			if ((collection = enumerable as ICollection<T>) != null)
			{
				collection.CopyTo(array, startIndex);
			}
			else if ((collectionbase=enumerable as ICollection) != null)
			{
				collectionbase.CopyTo(array, startIndex);
			}
			else
			{
				using (var enumerator = enumerable.GetEnumerator())
				{
					for (; enumerator.MoveNext(); startIndex++)
					{
						array[startIndex] = enumerator.Current;
					}
				}
			}
		}

		/// <summary>
		/// Converts an array of one type to another type. 
		/// </summary>
		/// <typeparam name="TIn">The type of the input array.</typeparam>
		/// <typeparam name="TOut">The type of the output array.</typeparam>
		/// <param name="array">The array.</param>
		/// <param name="converter">The converter function</param>
		/// <returns>The converted array</returns>
		public static TOut[] ConvertAll<TIn, TOut>(this TIn[] array, Converter<TIn, TOut> converter)
		{
			return Array.ConvertAll(array, converter);
		}

		/// <summary>
		/// Checks to see if any element match a condition. 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array">The array.</param>
		/// <param name="match">The match.</param>
		/// <returns></returns>
		public static bool Exists<T>(this T[] array, Predicate<T> match)
		{
			return Array.Exists(array, match);
		}

		/// <summary>
		/// Converts an array of one type to another type. 
		/// </summary>
		/// <typeparam name="TIn">The type of the input array.</typeparam>
		/// <typeparam name="TOut">The type of the output array.</typeparam>
		/// <param name="array">The array.</param>
		/// <param name="converter">The converter function</param>
		/// <returns>The converted array</returns>
		public static TOut[] ConvertAll<TIn, TOut>(this TIn[] array, Func<TIn, int, TOut> converter)
		{
			ArgumentValidator.AssertIsNotNull(() => array);
			TOut[] outValue = new TOut[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				outValue[i] = converter(array[i], i);
			}
			return outValue;
		}

		public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> range)
		{
			ArgumentValidator.AssertIsNotNull(() => collection);
			ArgumentValidator.AssertIsNotNull(() => range);
			if (collection.IsReadOnly)
				throw new ArgumentException(collection.GetType()+" is readonly.", Reflect.GetMemberName(() => collection));
			foreach (T value in range)
			{
				collection.Add(value);
			}
		}

		/// <summary>
		/// Applies an <see cref="Action`1"/> against a passed <see cref="IEnumerable{T}"/>.
		/// </summary>
		/// <typeparam name="T">The type of the IEnumerable</typeparam>
		/// <param name="enumerable">The enumerable.</param>
		/// <param name="action">The action.</param>
		/// <exception cref="ArgumentNullException">Throws if either enumerable or action are null.</exception>
		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			ArgumentValidator.AssertIsNotNull(() => enumerable);
			ArgumentValidator.AssertIsNotNull(() => action);
			foreach (var item in enumerable)
			{
				action(item);
			}
		}

		/// <summary>
		/// Applies an <see cref="Action`2"/> against a passed <see cref="IEnumerable{T}"/>.
		/// Also gives access to the index of enumeration.
		/// </summary>
		/// <typeparam name="T">The type of the IEnumerable</typeparam>
		/// <param name="enumerable">The enumerable.</param>
		/// <param name="action">The action.</param>
		/// <exception cref="ArgumentNullException">Throws if either enumerable or action are null.</exception>
		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T, int> action)
		{
			ArgumentValidator.AssertIsNotNull(() => enumerable);
			ArgumentValidator.AssertIsNotNull(() => action);

			using (var enumerator = enumerable.GetEnumerator())
			{
				for (int i = 0; enumerator.MoveNext(); i++)
				{
					action(enumerator.Current, i);
				}
			}
		}
	}
}
