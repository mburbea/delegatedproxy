﻿using DelegatedProxy.InvocationHandlers;
using Reflective;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using TestData;
using Utilities;
using Utilities.Extensions;


namespace ConsoleApplication1
{
   public class Program
    {
        private static Func<object>[] Tests = new Func<object>[20];

        private static void RunTests(int count = 1 << 24, bool displayResults = true)
        {
            var tests = Array.FindAll(Tests, t => t != null);
            var maxLength = tests.ConvertAll(x => GetMethodName(x.Method).Length).Max();

            for (int j = 0; j < tests.Length; j++)
            {
                var action = tests[j];
                Stopwatch sw = Stopwatch.StartNew();
                for (int i = 0; i < count; i++)
                {
                    action();
                }
                sw.Stop();
                if (displayResults)
                {
                    Console.WriteLine("{2}  {0}: {1}ms", GetMethodName(action.Method).PadRight(maxLength),
                                      ((int)sw.ElapsedMilliseconds).ToString(), j);
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private static string GetMethodName(MethodInfo method)
        {
            return method.IsGenericMethod
                    ? string.Format(@"{0}<{1}>", method.Name, string.Join<Type>(",", method.GetGenericArguments()))
                    : method.Name;
        }

        private static void Main(string[] args)
        {
			var type = new DelegatedProxy.ProxiedTypeBuilder(typeof(TestClass<>)).Type.MakeGenericType(typeof(string));
			var proxiedTestClass = (TestClass<string>)Activator.CreateInstance(type, new[] { new InvocationHandler[2] });
			Console.WriteLine(CodeGen.CreateDelegate<Func<MethodInfo>>(il => il.ldc(typeof(List<>).GetMethod("ConvertAll")).ret())());
			object o = 6;
			TestClass<string> handClass = new TestClassProxy<string>(new InvocationHandler[2]);
			Tests[0] = () => proxiedTestClass.Test();
			Tests[1] = () => proxiedTestClass.Test<int>("2");
			Tests[2] = () => handClass.Test();
			Tests[3] = () => handClass.Test<int>("3");
			RunTests(100, false);
			RunTests();
        }

    }
}