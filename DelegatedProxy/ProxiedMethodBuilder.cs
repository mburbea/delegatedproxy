﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using DelegatedProxy.InvocationHandlers;
using Reflective;
using Utilities;
using Utilities.Extensions;
using System.Collections.Generic;

namespace DelegatedProxy
{
	public partial class ProxiedTypeBuilder
	{

		public class ProxiedMethodBuilder
		{

			private static readonly FieldInfo TokenField = typeof(GenericToken<>).GetField("Token", BindingFlags.Public | BindingFlags.Static);

			private static readonly ConstructorInfo StrongBoxCtor = typeof(StrongTypeBox<>).GetConstructors()[1];
			internal static readonly FieldInfo StrongBoxValue = typeof(StrongTypeBox<>).GetField("Value");

			private readonly ProxiedTypeBuilder _typeBuilder;
			private readonly InternalMethodData _methodData;
			private readonly FieldBuilder _handlerField;
			private readonly MethodInfo _invokeMethod;
			private readonly MethodBuilder _overrideMethod;
			private readonly MethodBuilder _callBaseMethod;
			private readonly MethodBuilder _callBaseVirtMethod;
			internal ProxiedMethodBuilder(ProxiedTypeBuilder typeBuilder, InternalMethodData methodData)
			{
				this._typeBuilder = typeBuilder;
				this._methodData = methodData;
				this._methodData.DelegateType = this._methodData.DelegateType.MakeGenericType(this._methodData.DelegateGenericArguments.ConvertAll(g => ReplaceGenericArgument(g, null, typeBuilder.genericTypes)));
				// declare a field for this thing.
				this._handlerField = typeBuilder.typeBuilder.DefineField("h$" + this._methodData.MethodIndex + "$" + this._methodData.Method.Name,
					typeof(InvocationHandler<>).MakeGenericType(this._methodData.DelegateType), FieldAttributes.Private);
				this._invokeMethod = methodData.DeclaringTypeIsGeneric ? 
					TypeBuilder.GetMethod(this._methodData.DelegateType,this._methodData.DelegateType.GetGenericTypeDefinition().GetMethod("Invoke")) :
					this._methodData.DelegateType.GetMethod("Invoke");

				// build up to 3 methods for it.
				this._overrideMethod = BuildOverrideMethod();
				this._callBaseVirtMethod = BuildVirtCallBaseMethod();
				this._callBaseMethod = BuildStaticCallBaseMethod();

			}

			/// <summary>
			/// Updates the generics constraints.
			/// </summary>
			/// <param name="generics">The generics.</param>
			private void UpdateGenericsConstraints(GenericTypeParameterBuilder[] generics)
			{
				ProxiedTypeBuilder.UpdateGenericsConstraints(_methodData.GenericArguments, generics, _typeBuilder.genericTypes);
			}

			/// <summary>
			/// Builds the proxied method. 
			/// </summary>
			private MethodBuilder BuildOverrideMethod()
			{
				var methodattr = _methodData.Method.Attributes & ~(MethodAttributes.NewSlot | MethodAttributes.Abstract);
				var overrideMethod = _typeBuilder.typeBuilder.DefineMethod(this._methodData.Method.Name, methodattr, CallingConventions.HasThis);
				var generics = this._methodData.Method.IsGenericMethod
								? overrideMethod.DefineGenericParameters(_methodData.GenericArguments.ConvertAll(p => p.Name))
								: EmptyGenerics;

				Type[] paramTypes = _methodData.Parameters.ConvertAll(p => ReplaceGenericArgument(p.ParameterType, generics, _typeBuilder.genericTypes));

				overrideMethod.SetSignature(
					ReplaceGenericArgument(
						_methodData.ReturnParameter.ParameterType,
						generics,
						_typeBuilder.genericTypes
					),
					this._methodData.ReturnModReqs,
					this._methodData.ReturnModOpts,
					paramTypes,
					this._methodData.ParametersModReqs,
					this._methodData.ParametersModOpts
				);

				// load the handler's delegate
				var generator = overrideMethod.GetILGenerator()
					.ldarg_0()
					.ldfld(_handlerField)
					.ldfld(this._methodData.DeclaringTypeIsGeneric ?
						TypeBuilder.GetField(_handlerField.FieldType,
						_handlerField.FieldType.GetGenericTypeDefinition().GetField("Target")) :
						_handlerField.FieldType.GetField("Target"));
				// get all them parameters.
				var localByRefs = EmptyGenerics.Select(_ => new
															{
																local = default(LocalBuilder),
																parameterIndex = 0,
																varType = default(Type)
															}).ToList(); // creating a list of anonymous objects is hard :(
				//var localByRefs = new List<Tuple<LocalBuilder,int,Type>>();
				for (int i = 0; i < paramTypes.Length + 1; i++)
				{
					generator.ldarg_opt(i);
					if (i != 0 && paramTypes[i - 1].ContainsMethodGenericParameters())
					{
						if (paramTypes[i - 1].IsByRef)
						{
							LocalBuilder strongBoxLocal;
							var elemType = paramTypes[i - 1].GetElementType();
							generator
								.variable(typeof(StrongTypeBox<>).MakeGenericType(elemType), out strongBoxLocal)
								.EmitIf(elemType.IsValueType || elemType.IsGenericParameter, gen => gen.ldobj(elemType), gen => gen.ldind_ref())
								.newobj(TypeBuilder.GetConstructor(typeof(StrongTypeBox<>).MakeGenericType(elemType), StrongBoxCtor))
								.dup()
								.stloc(strongBoxLocal);
							localByRefs.Add(new { local = strongBoxLocal, parameterIndex = i, varType = elemType });
						}
						else if (paramTypes[i - 1].IsValueType || paramTypes[i - 1].IsGenericParameter)
						{
							// we must box a T, or a Nullable<T> but we don't have to box a List<T> or ISomething<T>, as its already an object ref. 
							generator.box(paramTypes[i - 1]);
						}
					}
				}
				// Foreach generic load a token.
				LocalBuilder retVal = null;
				generator
					.EmitFor(generics, (gen, g) => gen
						.ldsfld(TypeBuilder.GetField(typeof(GenericToken<>).MakeGenericType(g), TokenField))
					)
					.callvirt(_invokeMethod)
					.EmitIf(_methodData.Method.ReturnType.ContainsMethodGenericParameters(), gen => gen
						.unbox_any(_methodData.Method.ReturnType)
					)
					.EmitIf(localByRefs.Count > 0, gen => gen
						.EmitIf(_methodData.Method.ReturnType != typeof(void), ilg => ilg
							.variable(_methodData.Method.ReturnType, out retVal)
							.stloc(retVal)
						)
						.EmitFor(localByRefs, (ilg, varinfo) =>
							ilg
								.ldarg_opt(varinfo.parameterIndex)
								.ldloc(varinfo.local)
								.ldfld(TypeBuilder.GetField(typeof(StrongTypeBox<>).MakeGenericType(varinfo.varType), StrongBoxValue))
								.EmitIf(varinfo.varType.IsValueType || varinfo.varType.IsGenericParameter, arg => arg.stobj(varinfo.varType), arg => arg.stind_ref())
								.EmitIf(retVal != null, arg => arg.ldloc(retVal))
						)
					).ret();
				CodeGen.UpdateMaxStack(generator);
				UpdateGenericsConstraints(generics);
				return overrideMethod;
			}
			/// <summary>
			/// Builds the initializer for the method. This is usually just assigning the delegate from the parameter.
			/// </summary>
			/// <param name="generator">The generator.</param>
			/// <param name="handlersField">The handlers field.</param>
			/// <param name="handlerArgumentIndex">Index of the handler argument.</param>
			internal void Initializer(ILGenerator generator, FieldInfo handlersField, int handlerArgumentIndex)
			{
				Label storeIt;
				generator
					.define_label(out storeIt)
					.ldarg_0()
					.ldarg_opt(handlerArgumentIndex)
					.ldc_i4_opt(this._methodData.MethodIndex)
					.ldelem_ref()
					.dup()
					.brtrue_s(storeIt)

					.pop() // we had null on the stack
					.ldsfld(handlersField)
					.ldc_i4_opt(this._methodData.MethodIndex)
					.ldelem_ref()

					.mark_label(storeIt)
					.castclass(this._handlerField.FieldType)

					.stfld(this._handlerField);
			}

			/// <summary>
			/// Builds the static init for the methodinfo. This is building the call base delegate.
			/// </summary>
			/// <returns></returns>
			internal void StaticInitializer(ILGenerator generator)
			{
				generator
					.ldloc_0()
					.ldc_i4_opt(this._methodData.MethodIndex);
				if (!this._methodData.Method.IsGenericMethod || this._methodData.Method.IsAbstract)
				{
					generator
						.ldnull()
						.ldftn(this._methodData.Method.IsAbstract ? BuildBlowUpOnAbstract() : this._callBaseMethod)
						.newobj(this._methodData.DeclaringTypeIsGeneric ? 
							TypeBuilder.GetConstructor(this._methodData.DelegateType,this._methodData.DelegateType.GetGenericTypeDefinition().GetConstructors()[0]) :
							this._methodData.DelegateType.GetConstructors()[0]
						)
						.newobj(this._methodData.DeclaringTypeIsGeneric ? 
							TypeBuilder.GetConstructor(
							this._handlerField.FieldType, 
							this._handlerField.FieldType.GetGenericTypeDefinition().GetConstructor(new[]{this._handlerField.FieldType.GetGenericTypeDefinition().GetGenericArguments()[0]}))
							: this._handlerField.FieldType.GetConstructor(new[] { this._methodData.DelegateType }))
						.stelem_ref();
				}
				else
				{
					generator
						.ldc(this._typeBuilder.typeBuilder.MakeGenericType(this._typeBuilder.genericTypes))
						.ldc(this._callBaseMethod.Name)
						.newobj(this._methodData.DeclaringTypeIsGeneric ?
						TypeBuilder.GetConstructor(
							typeof(GenericInvocationHandler<>).MakeGenericType(this._methodData.DelegateType),
							typeof(GenericInvocationHandler<>).GetConstructor(new[] { typeof(Type), typeof(string) })
						)
						: typeof(GenericInvocationHandler<>).MakeGenericType(this._methodData.DelegateType).GetConstructor(new[] { typeof(Type), typeof(string) }))
						.stelem_ref();
				}
			}

			/// <summary>
			/// Builds the blow up on abstract.
			/// </summary>
			/// <returns></returns>
			private MethodBuilder BuildBlowUpOnAbstract()
			{
				var methodattr = (_methodData.Method.Attributes & ~(MethodAttributes.Abstract | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.Final | MethodAttributes.NewSlot))
											 | (MethodAttributes.Static);
				var helper = _typeBuilder.typeBuilder.DefineMethod("<OnAbstract>" + _methodData.Method.Name,
							methodattr,
							CallingConventions.Standard,
							this._invokeMethod.ReturnType,
							this._invokeMethod.GetParameters().ConvertAll(p => p.ParameterType)
				);
				helper.GetILGenerator()
					.ldstr("Cannot call an abstract method.")
					.newobj<InvalidOperationException>(typeof(string))
					.throw_();
				return helper;
			}

			private MethodBuilder BuildVirtCallBaseMethod()
			{
				if (_methodData.Method.IsAbstract) return null;
				var methodattr = (_methodData.Method.Attributes & ~(MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.Final | MethodAttributes.Public | MethodAttributes.NewSlot) | MethodAttributes.Private);
				var callBase = _typeBuilder.typeBuilder.DefineMethod("v$"+this._methodData.MethodIndex+"$"+this._methodData.Method.Name, methodattr, CallingConventions.HasThis);
				var generics = this._methodData.Method.IsGenericMethod
								? callBase.DefineGenericParameters(_methodData.GenericArguments.ConvertAll(p => p.Name))
								: EmptyGenerics;

				Type[] paramTypes = _methodData.Parameters.ConvertAll(p => ReplaceGenericArgument(p.ParameterType, generics, _typeBuilder.genericTypes));
				var generator = callBase.GetILGenerator();
				callBase.SetSignature(
					ReplaceGenericArgument(
						_methodData.ReturnParameter.ParameterType,
						generics,
						_typeBuilder.genericTypes
					),
					this._methodData.ReturnModReqs,
					this._methodData.ReturnModOpts,
					paramTypes,
					this._methodData.ParametersModReqs,
					this._methodData.ParametersModOpts
				);

				generator
					.ldarg_0()
					.EmitFor(
						paramTypes,
						(gen, p, i) => gen
							.ldarg_opt(i+1)
					)
					.call(generics.Length > 0 ? this._methodData.Method.MakeGenericMethod(generics) : this._methodData.Method)
					.ret();
				UpdateGenericsConstraints(generics);
				CodeGen.UpdateMaxStack(callBase.GetILGenerator());
				return callBase;
			}
			/// <summary>
			/// Builds the call base method.
			/// </summary>
			private MethodBuilder BuildStaticCallBaseMethod()
			{
				if (_methodData.Method.IsAbstract) return BuildBlowUpOnAbstract();

				var methodattr = (_methodData.Method.Attributes & ~(MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.Final | MethodAttributes.NewSlot))
								 | (MethodAttributes.Static);
				var callBase = _typeBuilder.typeBuilder.DefineMethod("s$"+_methodData.MethodIndex+"$" + _methodData.Method.Name, methodattr, CallingConventions.Standard);
				var generator = callBase.GetILGenerator();
				var generics = this._methodData.Method.IsGenericMethod ?
					callBase.DefineGenericParameters(this._methodData.GenericArguments.ConvertAll(g => g.Name))
					: EmptyGenerics;
				var paramTypes = this._methodData.CallBaseParameterTypes.ConvertAll(g => ReplaceGenericArgument(g, generics, _typeBuilder.genericTypes));
				callBase.SetSignature(
					ReplaceGenericArgument(this._methodData.ReturnParameter.ParameterType, generics, _typeBuilder.genericTypes),
					this._methodData.ReturnModReqs,
					this._methodData.ReturnModOpts,
					paramTypes,
					new[] { Type.EmptyTypes }.Concat(this._methodData.ParametersModReqs).ToArray(),
					new[] { Type.EmptyTypes }.Concat(this._methodData.ParametersModOpts).ToArray()
				);

				generator
					.ldarg_0()
					.castclass(this._typeBuilder.typeBuilder)
					.EmitFor(
						paramTypes.Skip(1),
						(gen, p, i) => gen.ldarg_opt(i+1)
					)
					.callvirt(generics.Length > 0 ? _callBaseVirtMethod.MakeGenericMethod(generics) : _callBaseVirtMethod)
					.ret();
				UpdateGenericsConstraints(generics);
				CodeGen.UpdateMaxStack(callBase.GetILGenerator());
				return callBase;
			}
		}
	}
}
