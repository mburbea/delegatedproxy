﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.InteropServices;

namespace DelegatedProxy
{
	/// <summary>
	/// Treats an integer as a compressed boolean array. <see cref="BitVector32"/>
	/// expects you to send in bitmasks, which I have no intention of doing at callsite.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct SimpleBitVector32
	{
		private int data;
		/// <summary>
		/// Gets or sets the <see cref="System.Boolean"/> at the specified offset.
		/// </summary>
		public bool this[int offset]
		{
			get
			{
				unchecked
				{
					return (this.data & (1 << offset)) != 0;
				}
			}
			set
			{
				unchecked
				{
					this.data = value ? (this.data | (1 << offset)) : (this.data & ~(1 << offset));
				}
			}
		}

		private int ToStringHelper(int index)
		{
			return Math.Sign(this.data & (1 << index));
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// the representation is going to be 32 0 or 1s dictating which bits are on or off.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return "{" + string.Concat(Enumerable.Range(0, 32).Select(ToStringHelper)) + "}";
		}

		/// <summary>
		/// Implements the operator &lt;&lt;.
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <param name="shift">The shift.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static  SimpleBitVector32 operator <<(SimpleBitVector32 vector,int shift)
		{
			return new SimpleBitVector32(vector.data << shift);
		}

		/// <summary>
		/// Implements the operator &gt;&gt;.
		/// </summary>
		/// <param name="vector">The vector.</param>
		/// <param name="shift">The shift.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static SimpleBitVector32 operator >>(SimpleBitVector32 vector, int shift)
		{
			return new SimpleBitVector32(vector.data >> shift);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SimpleBitVector32"/> struct.
		/// </summary>
		/// <param name="value">The value.</param>
		public SimpleBitVector32(int value)
		{
			this.data = value;
		}


		/// <summary>
		/// Performs an implicit conversion from <see cref="System.Int32"/> to <see cref="SimpleBitVector32"/>.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// The result of the conversion.
		/// </returns>
		public static implicit operator SimpleBitVector32(int value)
		{
			return new SimpleBitVector32(value);
		}

		/// <summary>
		/// Performs an implicit conversion from <see cref="SimpleBitVector32"/> to <see cref="System.Int32"/>.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>
		/// The result of the conversion.
		/// </returns>
		public static implicit operator int(SimpleBitVector32 value)
		{
			return value.data; 
		}
	}
}
