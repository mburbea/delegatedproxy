﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DelegatedProxy
{
	public abstract class GenericToken
	{
		/// <summary>
		/// Represents the type parameters for a non-generic method. 
		/// </summary>
		public static readonly GenericToken[] Empty = new GenericToken[0];
		/// <summary>
		/// Gets the type associated with this token.
		/// </summary>
		public readonly Type Type;
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string ToString()
		{
			return Type.ToString();
		}
		/// <summary>
		/// Initializes a new instance of the <see cref="GenericToken"/> class.
		/// internal to avoid external construction.
		/// </summary>
		internal GenericToken(Type type) { this.Type = type; }

 
	}

	public sealed class GenericToken<T> : GenericToken
	{
		/// <summary>
		/// Gets the only instance of this token. 
		/// </summary>
		public static readonly GenericToken<T> Token = new GenericToken<T>();

		/// <summary>
		/// Prevents a default instance of the <see cref="GenericToken&lt;T&gt;"/> class from being created.
		/// </summary>
		private GenericToken(): base(typeof(T)) {}
	}
}
