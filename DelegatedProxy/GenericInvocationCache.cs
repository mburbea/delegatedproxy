﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Reflective;
using Utilities;
using Utilities.Extensions;
namespace DelegatedProxy
{
	public interface IInvocation
	{
		object Instance {get;}
		object GetArgument(int index);
		void SetArgument(int index, object value);
		int Count { get; }
		object RetVal { get; set; }
		bool HasReturnValue { get; }
		GenericToken[] TypeTokens { get; }
	}

	public partial class ProxyGenerator
	{

			internal static Type GetInvocationType(InternalMethodData method)
			{
				ArgumentValidator.AssertIsNotNull(() => method);
				return GenericInvocationCache.GetInvocationType(method);
			}
			static class GenericInvocationCache
			{
				private static readonly ModuleBuilder ModuleBuilder = AssemblyGen.GenModule;
				private static readonly ConcurrentDictionary<SignatureEncoder, Type> Cache = new ConcurrentDictionary<SignatureEncoder, Type>();

				private const MethodAttributes InterfaceMethodAttribute = MethodAttributes.HideBySig |
									  MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.Final;
				private static readonly FieldInfo TypeTokensEmptyField = typeof(GenericToken).GetField("Empty", BindingFlags.Static | BindingFlags.Public);

				/// <summary>
				/// Gets an Invocation type for a method.
				/// </summary>
				/// <param name="method">The method.</param>
				/// <returns></returns>
				public static Type GetInvocationType(InternalMethodData method)
				{
					return null;
					//var parameters = method.GetParameters();
					//var count = parameters.Length + 1; // this+params
					//var returnType = method.ReturnType;
					//var generics = method.IsGenericMethod ? method.GetGenericArguments() : Type.EmptyTypes;
					//var hasGenerics = generics.Length > 0;
					//if (count > 32)
					//{
					//    throw new ArgumentException("DelegatedProxy only supports instance methods with 31 parameters.", Reflect.GetMemberName(() => method));
					//}
					//if (count == 32 && hasGenerics)
					//{
					//    throw new ArgumentException("DelegatedProxy only supports generic method with 30 parameters", Reflect.GetMemberName(() => method));
					//}
					//if (returnType.IsByRef)
					//{
					//    // technically supported by the CLR but we're not going to support it.
					//    throw new ArgumentException("The return type cannot be by-reference.", Reflect.GetMemberName(() => method));
					//}
					//Type type =  Cache.GetOrAdd(SignatureEncoder.Encode(method, parameters,generics), MakeNewCustomInvocation);

					//var types = new Type[count + Convert.ToInt32(returnType != typeof(void))];
					//types[0] = method.DeclaringType;
					//parameters.ConvertAll(p => p.ParameterType.ContainsGenericParameters ?
					//    typeof(object) :
					//    p.ParameterType.IsByRef ? p.ParameterType.GetElementType() :
					//        p.ParameterType).CopyTo(types, 1);
					//if (returnType != typeof(void)) types[types.Length - 1] = returnType.ContainsGenericParameters ? typeof(object) : returnType;
					//return  type.MakeGenericType(types);
				}

				/// <summary>
				/// Makes the new custom argument type.
				/// </summary>
				/// <param name="SignatureEncoder">The SignatureEncoder.</param>
				/// <returns></returns>
				private static Type MakeNewCustomInvocation(SignatureEncoder SignatureEncoder)
				{
					return new InvocationBuilder(SignatureEncoder).GetInvocationType();
				}

				class InvocationBuilder
				{
					readonly TypeBuilder builder;
					readonly GenericTypeParameterBuilder[] genericParams;
					readonly bool returnsVoid;
					readonly int arguments;
					readonly int generics;
					readonly FieldBuilder[] fields;
					readonly FieldBuilder tokensField;
					Type _result;
					public Type GetInvocationType()
					{
						if (_result == null)
						{
							this.MakeCtor();
							this.MakeGetArgumentMethod();
							this.MakeSetArgumentMethod();
							this.MakeHasReturnValueProperty();
							this.MakeReturnValueProperty();
							this.MakeInstanceProperty();
							this.MakeCountProperty();
							this.MakeTypeTokensProperty();
							_result = builder.CreateType();
						}
						return _result;
					}

					private void MakeCtor()
					{
						Type[] parameters = Type.EmptyTypes;
 						if(generics > 0) parameters = new[]{typeof(GenericToken[])};
						parameters = genericParams.Concat(parameters).ToArray();

						var ctor = builder.DefineConstructor(
							MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public |
							MethodAttributes.SpecialName,
							CallingConventions.Standard,
							parameters
							);
						var ilgen = ctor.GetILGenerator();

						for (int i = 0; i < arguments; i++)
						{
							ilgen
								.ldarg_0()
								.ldarg_opt(i + 1)
								.stfld(fields[i]);
						}
						if (generics > 0) ilgen.ldarg_0().ldarg_opt(parameters.Length).stfld(tokensField);
						ilgen.ret();
					}
					private void MakeTypeTokensProperty()
					{
						var typeTokensProperty = builder.DefineProperty("TypeTokens", PropertyAttributes.None, typeof(GenericToken[]), Type.EmptyTypes);
						var getter = builder.DefineMethod("get_TypeTokens",
														  InterfaceMethodAttribute | MethodAttributes.SpecialName,
														  CallingConventions.HasThis,
														  typeof(GenericToken[]),
														  Type.EmptyTypes
										);
						if (tokensField == null)
						{
							getter.GetILGenerator()
								.ldsfld(TypeTokensEmptyField)
								.ret();
						}
						else
						{
							getter.GetILGenerator()
								.ldarg_0()
								.ldfld(tokensField)
								.ret();
						}
						typeTokensProperty.SetGetMethod(getter);
					}

					private void MakeInstanceProperty()
					{
						var instanceProperty = builder.DefineProperty("Instance", PropertyAttributes.None, typeof(object), Type.EmptyTypes);
						var getter = builder.DefineMethod("get_Instance",
														  InterfaceMethodAttribute | MethodAttributes.SpecialName,
														  CallingConventions.HasThis,
														  typeof(object),
														  Type.EmptyTypes
							);
						getter.GetILGenerator()
							.ldarg_0()
							.ldfld(fields[0])
							.ret();
						instanceProperty.SetGetMethod(getter);
					}
					private void MakeHasReturnValueProperty()
					{
						var property = builder.DefineProperty("HasReturnValue", PropertyAttributes.None, typeof(bool), Type.EmptyTypes);
						var getter = builder.DefineMethod("get_HasReturnValue",
																	InterfaceMethodAttribute | MethodAttributes.SpecialName,
																	CallingConventions.HasThis,
																	typeof(bool),
																	Type.EmptyTypes
							);
						getter.GetILGenerator().ldc(!returnsVoid).ret();
						property.SetGetMethod(getter);
					}

					private void MakeCountProperty()
					{
						var property = builder.DefineProperty("Count", PropertyAttributes.None, typeof(int), Type.EmptyTypes);
						var getter = builder.DefineMethod("get_Count",
														 InterfaceMethodAttribute | MethodAttributes.SpecialName,
														 CallingConventions.HasThis,
														  typeof(int),
														  Type.EmptyTypes
							);
						getter.GetILGenerator().ldc_i4_opt(arguments-1).ret();
						property.SetGetMethod(getter);
					}

					private void MakeGetArgumentMethod()
					{
						var getArgumentMethod = builder.DefineMethod("GetArgument",
																	 InterfaceMethodAttribute,
																	 CallingConventions.HasThis,
																	 typeof(Object),
																	 new[] { typeof(Int32) }
							);
						var ilgen = getArgumentMethod.GetILGenerator();

						var labels = Enumerable.Range(0, arguments - 1).Select(f => ilgen.DefineLabel()).ToArray();
						var endLabel = ilgen.DefineLabel();
						ilgen
							.ldarg_1()
							.switch_(labels)
							.br_s(endLabel);
						for (int i = 0; i < labels.Length; i++)
						{
							ilgen
								.mark_label(labels[i])
								.ldarg_0()
								.ldfld(fields[i + 1])
								.box(fields[i + 1].FieldType)
								.ret();
						}
						ilgen
							.mark_label(endLabel)
							.ldstr("index")
							.ldarg_1()
							.box<int>()
							.ldstr("Method only has " + labels.Length + " parameters")
							.newobj<ArgumentOutOfRangeException>(typeof(String), typeof(object), typeof(string))
							.throw_();
					}
					private void MakeReturnValueProperty()
					{
						var retValProperty = builder.DefineProperty("RetVal", PropertyAttributes.None, typeof(object), Type.EmptyTypes);
						var getter = builder.DefineMethod("get_RetVal",
													  InterfaceMethodAttribute | MethodAttributes.SpecialName,
													  CallingConventions.HasThis,
													  typeof(object),
													  Type.EmptyTypes
							);
						var setter = builder.DefineMethod("set_RetVal",
									  InterfaceMethodAttribute | MethodAttributes.SpecialName,
									  CallingConventions.HasThis,
									  typeof(void),
									  new[] { typeof(object) }
		);
						if (returnsVoid)
						{
							getter.GetILGenerator()
								.ldstr(@"Method returns void")
								.newobj<InvalidOperationException>(typeof(string))
								.throw_();
							setter.GetILGenerator()
								.ldstr(@"Method returns void")
								.newobj<InvalidOperationException>(typeof(string))
								.throw_();
						}
						else
						{
							var returnField = this.fields[this.fields.Length - 1];
							//build getter
							getter.GetILGenerator()
								.ldarg_0()
								.ldfld(returnField)
								.box(returnField.FieldType)
								.ret();
							// build setter
							setter.GetILGenerator()
								.ldarg_0()
								.ldarg_1()
								.unbox_any(returnField.FieldType)
								.stfld(returnField)
								.ret();
						}
						// set the properties back. 
						retValProperty.SetGetMethod(getter);
						retValProperty.SetSetMethod(setter);

					}

					private void MakeSetArgumentMethod()
					{
						var setArgumentMethod = builder.DefineMethod("SetArgument",
																	 InterfaceMethodAttribute,
																	 CallingConventions.HasThis,
																	 typeof(void),
																	 new[] { typeof(Int32), typeof(object) }
							);
						var ilgen = setArgumentMethod.GetILGenerator();
						var labels = Enumerable.Range(0, arguments - 1).Select(f => ilgen.DefineLabel()).ToArray();
						var endLabel = ilgen.DefineLabel();
						ilgen
							.ldarg_1()
							.switch_(labels)
							.br_s(endLabel);
						for (int i = 0; i < labels.Length; i++)
						{
							ilgen
								.mark_label(labels[i])
								.ldarg_0()
								.ldarg_2()
								.unbox_any(fields[i + 1].FieldType)
								.stfld(fields[i + 1])
								.ret();
						}
						ilgen
							.mark_label(endLabel)
							.ldstr("index")
							.ldarg_1()
							.box<int>()
							.ldstr("Method only has " + labels.Length + " parameters")
							.newobj<ArgumentOutOfRangeException>(typeof(string), typeof(object), typeof(string))
							.throw_();
					}

					public InvocationBuilder(SignatureEncoder encoder)
					{
						this.builder = ModuleBuilder.DefineType(
							"DelegatedProxy.Invocations" + encoder.EncodedValue,
							TypeAttributes.AnsiClass | TypeAttributes.Sealed | TypeAttributes.SequentialLayout,
							typeof (System.ValueType),
							new[] {typeof (IInvocation)}
							);
						this.returnsVoid = encoder.ReturnsVoid;
						int arrity = this.returnsVoid ? encoder.Arity : encoder.Arity + 1;
						this.genericParams = builder.DefineGenericParameters(Enumerable.Range(0, arrity).Select(i => "T" + i).ToArray());
						this.arguments = encoder.Arity;
						this.fields=this.genericParams.ConvertAll((t, i) => builder.DefineField("field" + i, t, FieldAttributes.Public));
						this.generics = encoder.GenericArity;
						if (generics > 0) this.tokensField = builder.DefineField("tokens", typeof(GenericToken[]), FieldAttributes.Private);
					}
				}
			}
	}
}
