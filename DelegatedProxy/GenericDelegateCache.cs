﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Utilities.Extensions;

namespace DelegatedProxy
{
	public partial class ProxyGenerator
	{
		internal static Type GetOpenDelegate(MethodInfo method)
		{
			return GenericDelegateCache.GetOpenDelegate(method);
		}


		internal static Type GetOpenDelegate(SignatureEncoder encoder)
		{
			return GenericDelegateCache.GetOpenDelegate(encoder);
		}

		/// <summary>
		/// A get or add cache that generates a custom delegate type based on the type parameters.
		/// </summary>
		private static class GenericDelegateCache
		{
			#region Private Static Variables
			private static readonly ModuleBuilder ModuleBuilder = AssemblyGen.GenModule;
			private static readonly Type[] DelegateCtorSignature = new[] { typeof(object), typeof(IntPtr) };
			private static readonly ConcurrentDictionary<SignatureEncoder, Type> Cache = new ConcurrentDictionary<SignatureEncoder, Type>();


			#endregion Private Static Variables

			#region Private Static Methods
			// this is merely there to make reflector happy.
			// if you don't include this reflector infers the type name has it and then can't
			// locate it. As a result it'll display the invoke method of a delegate and it's ctor.
			const char GENERIC_DELIMITER = '`';
			/// <summary>
			/// Makes the new custom delegate by decoding the <see cref="SignatureEncoder"/>
			/// </summary>
			/// <param name="encoder">The decoder.</param>
			/// <returns></returns>
			private static Type MakeNewCustomDelegateType(SignatureEncoder encoder)
			{
				bool returnsVoid = encoder.ReturnsVoid;
				SimpleBitVector32 vector = encoder.RefValueLocations;
				var paramCount = encoder.Arity;
				int genericCount = returnsVoid ? paramCount : paramCount + 1;
				TypeBuilder builder = ModuleBuilder.DefineType(
					String.Concat(returnsVoid ? "DelegatedProxy.Action" : "DelegatedProxy.Func",(int)encoder.RefValueLocations,GENERIC_DELIMITER,genericCount),
					TypeAttributes.AutoClass | TypeAttributes.Sealed | TypeAttributes.Public,
					typeof(MulticastDelegate)
				);


				GenericTypeParameterBuilder[] genericParams = builder.DefineGenericParameters(Enumerable.Range(0, genericCount).Select(i => "T" + i).ToArray());
				var parameters = new Type[paramCount];
				Type returnType;

				if (returnsVoid)
				{
					returnType = typeof(void);
				}
				else
				{
					genericParams[paramCount].SetGenericParameterAttributes(GenericParameterAttributes.Covariant);
					returnType = genericParams[paramCount];
				}

				for (int i = 0; i < paramCount; i++)
				{
					if (vector[i])
					{
						// byref parameters cannot be contravariant. 
						parameters[i] = genericParams[i].MakeByRefType();
					}
					else
					{
						genericParams[i].SetGenericParameterAttributes(GenericParameterAttributes.Contravariant);
						parameters[i] = genericParams[i];
					}
				}
				builder.DefineMethod(
					"Invoke",
					 MethodAttributes.VtableLayoutMask | MethodAttributes.HideBySig | MethodAttributes.Virtual | MethodAttributes.Public,
					 returnType,
					 parameters
				).SetImplementationFlags(MethodImplAttributes.Runtime);

				builder.DefineConstructor(
					MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public,
					CallingConventions.Standard,
					DelegateCtorSignature
				).SetImplementationFlags(MethodImplAttributes.Runtime);


				return builder.CreateType();
			}

			#endregion Private Static Methods

			#region Public Static Methods

			public static Type GetOpenDelegate(SignatureEncoder encoder)
			{
				encoder.Arity = (byte)(encoder.GenericArity + encoder.Arity);
				encoder.GenericArity = 0;
				switch (encoder.EncodedValue)
				{
					/* Generated code
					 * A semi-exhuastive list of "normal delegates".
					 * Normal delegates are delegates with up to 16 normal parameters
					 * Or delegates to handle up to 4 by ref parameters in the first 4 positions
					 * as well as handling my strange encoding of generic tokens.
					*/
					case 0: return typeof(Func<>);
					case 1: return typeof(Func<,>);
					case 2: return typeof(Func<,,>);
					case 3: return typeof(Func<,,,>);
					case 4: return typeof(Func<,,,,>);
					case 5: return typeof(Func<,,,,,>);
					case 6: return typeof(Func<,,,,,,>);
					case 7: return typeof(Func<,,,,,,,>);
					case 8: return typeof(Func<,,,,,,,,>);
					case 9: return typeof(Func<,,,,,,,,,>);
					case 10: return typeof(Func<,,,,,,,,,,>);
					case 11: return typeof(Func<,,,,,,,,,,,>);
					case 12: return typeof(Func<,,,,,,,,,,,,>);
					case 13: return typeof(Func<,,,,,,,,,,,,,>);
					case 14: return typeof(Func<,,,,,,,,,,,,,,>);
					case 15: return typeof(Func<,,,,,,,,,,,,,,,>);
					case 16: return typeof(Func<,,,,,,,,,,,,,,,,>);
					case 256: return typeof(Action);
					case 257: return typeof(Action<>);
					case 258: return typeof(Action<,>);
					case 259: return typeof(Action<,,>);
					case 260: return typeof(Action<,,,>);
					case 261: return typeof(Action<,,,,>);
					case 262: return typeof(Action<,,,,,>);
					case 263: return typeof(Action<,,,,,,>);
					case 264: return typeof(Action<,,,,,,,>);
					case 265: return typeof(Action<,,,,,,,,>);
					case 266: return typeof(Action<,,,,,,,,,>);
					case 267: return typeof(Action<,,,,,,,,,,>);
					case 268: return typeof(Action<,,,,,,,,,,,>);
					case 269: return typeof(Action<,,,,,,,,,,,,>);
					case 270: return typeof(Action<,,,,,,,,,,,,,>);
					case 271: return typeof(Action<,,,,,,,,,,,,,,>);
					case 272: return typeof(Action<,,,,,,,,,,,,,,,>);
					case 8589934594: return typeof(Func2<,,>);
					case 8589934595: return typeof(Func2<,,,>);
					case 8589934596: return typeof(Func2<,,,,>);
					case 8589934597: return typeof(Func2<,,,,,>);
					case 8589934598: return typeof(Func2<,,,,,,>);
					case 8589934599: return typeof(Func2<,,,,,,,>);
					case 8589934600: return typeof(Func2<,,,,,,,,>);
					case 8589934601: return typeof(Func2<,,,,,,,,,>);
					case 8589934850: return typeof(Action2<,>);
					case 8589934851: return typeof(Action2<,,>);
					case 8589934852: return typeof(Action2<,,,>);
					case 8589934853: return typeof(Action2<,,,,>);
					case 8589934854: return typeof(Action2<,,,,,>);
					case 8589934855: return typeof(Action2<,,,,,,>);
					case 8589934856: return typeof(Action2<,,,,,,,>);
					case 8589934857: return typeof(Action2<,,,,,,,,>);
					case 17179869187: return typeof(Func4<,,,>);
					case 17179869188: return typeof(Func4<,,,,>);
					case 17179869189: return typeof(Func4<,,,,,>);
					case 17179869190: return typeof(Func4<,,,,,,>);
					case 17179869191: return typeof(Func4<,,,,,,,>);
					case 17179869192: return typeof(Func4<,,,,,,,,>);
					case 17179869193: return typeof(Func4<,,,,,,,,,>);
					case 17179869443: return typeof(Action4<,,>);
					case 17179869444: return typeof(Action4<,,,>);
					case 17179869445: return typeof(Action4<,,,,>);
					case 17179869446: return typeof(Action4<,,,,,>);
					case 17179869447: return typeof(Action4<,,,,,,>);
					case 17179869448: return typeof(Action4<,,,,,,,>);
					case 17179869449: return typeof(Action4<,,,,,,,,>);
					case 25769803779: return typeof(Func6<,,,>);
					case 25769803780: return typeof(Func6<,,,,>);
					case 25769803781: return typeof(Func6<,,,,,>);
					case 25769803782: return typeof(Func6<,,,,,,>);
					case 25769803783: return typeof(Func6<,,,,,,,>);
					case 25769803784: return typeof(Func6<,,,,,,,,>);
					case 25769803785: return typeof(Func6<,,,,,,,,,>);
					case 25769804035: return typeof(Action6<,,>);
					case 25769804036: return typeof(Action6<,,,>);
					case 25769804037: return typeof(Action6<,,,,>);
					case 25769804038: return typeof(Action6<,,,,,>);
					case 25769804039: return typeof(Action6<,,,,,,>);
					case 25769804040: return typeof(Action6<,,,,,,,>);
					case 25769804041: return typeof(Action6<,,,,,,,,>);
					case 34359738372: return typeof(Func8<,,,,>);
					case 34359738373: return typeof(Func8<,,,,,>);
					case 34359738374: return typeof(Func8<,,,,,,>);
					case 34359738375: return typeof(Func8<,,,,,,,>);
					case 34359738376: return typeof(Func8<,,,,,,,,>);
					case 34359738377: return typeof(Func8<,,,,,,,,,>);
					case 34359738628: return typeof(Action8<,,,>);
					case 34359738629: return typeof(Action8<,,,,>);
					case 34359738630: return typeof(Action8<,,,,,>);
					case 34359738631: return typeof(Action8<,,,,,,>);
					case 34359738632: return typeof(Action8<,,,,,,,>);
					case 34359738633: return typeof(Action8<,,,,,,,,>);
					case 42949672964: return typeof(Func10<,,,,>);
					case 42949672965: return typeof(Func10<,,,,,>);
					case 42949672966: return typeof(Func10<,,,,,,>);
					case 42949672967: return typeof(Func10<,,,,,,,>);
					case 42949672968: return typeof(Func10<,,,,,,,,>);
					case 42949672969: return typeof(Func10<,,,,,,,,,>);
					case 42949673220: return typeof(Action10<,,,>);
					case 42949673221: return typeof(Action10<,,,,>);
					case 42949673222: return typeof(Action10<,,,,,>);
					case 42949673223: return typeof(Action10<,,,,,,>);
					case 42949673224: return typeof(Action10<,,,,,,,>);
					case 42949673225: return typeof(Action10<,,,,,,,,>);
					case 51539607556: return typeof(Func12<,,,,>);
					case 51539607557: return typeof(Func12<,,,,,>);
					case 51539607558: return typeof(Func12<,,,,,,>);
					case 51539607559: return typeof(Func12<,,,,,,,>);
					case 51539607560: return typeof(Func12<,,,,,,,,>);
					case 51539607561: return typeof(Func12<,,,,,,,,,>);
					case 51539607812: return typeof(Action12<,,,>);
					case 51539607813: return typeof(Action12<,,,,>);
					case 51539607814: return typeof(Action12<,,,,,>);
					case 51539607815: return typeof(Action12<,,,,,,>);
					case 51539607816: return typeof(Action12<,,,,,,,>);
					case 51539607817: return typeof(Action12<,,,,,,,,>);
					case 60129542148: return typeof(Func14<,,,,>);
					case 60129542149: return typeof(Func14<,,,,,>);
					case 60129542150: return typeof(Func14<,,,,,,>);
					case 60129542151: return typeof(Func14<,,,,,,,>);
					case 60129542152: return typeof(Func14<,,,,,,,,>);
					case 60129542153: return typeof(Func14<,,,,,,,,,>);
					case 60129542404: return typeof(Action14<,,,>);
					case 60129542405: return typeof(Action14<,,,,>);
					case 60129542406: return typeof(Action14<,,,,,>);
					case 60129542407: return typeof(Action14<,,,,,,>);
					case 60129542408: return typeof(Action14<,,,,,,,>);
					case 60129542409: return typeof(Action14<,,,,,,,,>);
					case 68719476741: return typeof(Func16<,,,,,>);
					case 68719476742: return typeof(Func16<,,,,,,>);
					case 68719476743: return typeof(Func16<,,,,,,,>);
					case 68719476744: return typeof(Func16<,,,,,,,,>);
					case 68719476745: return typeof(Func16<,,,,,,,,,>);
					case 68719476997: return typeof(Action16<,,,,>);
					case 68719476998: return typeof(Action16<,,,,,>);
					case 68719476999: return typeof(Action16<,,,,,,>);
					case 68719477000: return typeof(Action16<,,,,,,,>);
					case 68719477001: return typeof(Action16<,,,,,,,,>);
					case 77309411333: return typeof(Func18<,,,,,>);
					case 77309411334: return typeof(Func18<,,,,,,>);
					case 77309411335: return typeof(Func18<,,,,,,,>);
					case 77309411336: return typeof(Func18<,,,,,,,,>);
					case 77309411337: return typeof(Func18<,,,,,,,,,>);
					case 77309411589: return typeof(Action18<,,,,>);
					case 77309411590: return typeof(Action18<,,,,,>);
					case 77309411591: return typeof(Action18<,,,,,,>);
					case 77309411592: return typeof(Action18<,,,,,,,>);
					case 77309411593: return typeof(Action18<,,,,,,,,>);
					case 85899345925: return typeof(Func20<,,,,,>);
					case 85899345926: return typeof(Func20<,,,,,,>);
					case 85899345927: return typeof(Func20<,,,,,,,>);
					case 85899345928: return typeof(Func20<,,,,,,,,>);
					case 85899345929: return typeof(Func20<,,,,,,,,,>);
					case 85899346181: return typeof(Action20<,,,,>);
					case 85899346182: return typeof(Action20<,,,,,>);
					case 85899346183: return typeof(Action20<,,,,,,>);
					case 85899346184: return typeof(Action20<,,,,,,,>);
					case 85899346185: return typeof(Action20<,,,,,,,,>);
					case 94489280517: return typeof(Func22<,,,,,>);
					case 94489280518: return typeof(Func22<,,,,,,>);
					case 94489280519: return typeof(Func22<,,,,,,,>);
					case 94489280520: return typeof(Func22<,,,,,,,,>);
					case 94489280521: return typeof(Func22<,,,,,,,,,>);
					case 94489280773: return typeof(Action22<,,,,>);
					case 94489280774: return typeof(Action22<,,,,,>);
					case 94489280775: return typeof(Action22<,,,,,,>);
					case 94489280776: return typeof(Action22<,,,,,,,>);
					case 94489280777: return typeof(Action22<,,,,,,,,>);
					case 103079215109: return typeof(Func24<,,,,,>);
					case 103079215110: return typeof(Func24<,,,,,,>);
					case 103079215111: return typeof(Func24<,,,,,,,>);
					case 103079215112: return typeof(Func24<,,,,,,,,>);
					case 103079215113: return typeof(Func24<,,,,,,,,,>);
					case 103079215365: return typeof(Action24<,,,,>);
					case 103079215366: return typeof(Action24<,,,,,>);
					case 103079215367: return typeof(Action24<,,,,,,>);
					case 103079215368: return typeof(Action24<,,,,,,,>);
					case 103079215369: return typeof(Action24<,,,,,,,,>);
					case 111669149701: return typeof(Func26<,,,,,>);
					case 111669149702: return typeof(Func26<,,,,,,>);
					case 111669149703: return typeof(Func26<,,,,,,,>);
					case 111669149704: return typeof(Func26<,,,,,,,,>);
					case 111669149705: return typeof(Func26<,,,,,,,,,>);
					case 111669149957: return typeof(Action26<,,,,>);
					case 111669149958: return typeof(Action26<,,,,,>);
					case 111669149959: return typeof(Action26<,,,,,,>);
					case 111669149960: return typeof(Action26<,,,,,,,>);
					case 111669149961: return typeof(Action26<,,,,,,,,>);
					case 120259084293: return typeof(Func28<,,,,,>);
					case 120259084294: return typeof(Func28<,,,,,,>);
					case 120259084295: return typeof(Func28<,,,,,,,>);
					case 120259084296: return typeof(Func28<,,,,,,,,>);
					case 120259084297: return typeof(Func28<,,,,,,,,,>);
					case 120259084549: return typeof(Action28<,,,,>);
					case 120259084550: return typeof(Action28<,,,,,>);
					case 120259084551: return typeof(Action28<,,,,,,>);
					case 120259084552: return typeof(Action28<,,,,,,,>);
					case 120259084553: return typeof(Action28<,,,,,,,,>);
					case 128849018885: return typeof(Func30<,,,,,>);
					case 128849018886: return typeof(Func30<,,,,,,>);
					case 128849018887: return typeof(Func30<,,,,,,,>);
					case 128849018888: return typeof(Func30<,,,,,,,,>);
					case 128849018889: return typeof(Func30<,,,,,,,,,>);
					case 128849019141: return typeof(Action30<,,,,>);
					case 128849019142: return typeof(Action30<,,,,,>);
					case 128849019143: return typeof(Action30<,,,,,,>);
					case 128849019144: return typeof(Action30<,,,,,,,>);
					case 128849019145: return typeof(Action30<,,,,,,,,>);
					default: return Cache.GetOrAdd(encoder, MakeNewCustomDelegateType);
				}
			}
			public static Type GetOpenDelegate(MethodInfo method)
			{
				var paramTypes = method.GetParameters().ConvertAll(p => p.ParameterType);
				var byRefParams = new SimpleBitVector32();
				for (int i = 0; i < paramTypes.Length; i++)
				{
					byRefParams[i] = paramTypes[i].IsByRef;
				}
				var encoder = new SignatureEncoder()
				{
					Arity = (byte)paramTypes.Length,
					GenericArity = 0,
					ReturnsVoid = method.ReturnType == typeof(void),
					RefValueLocations = byRefParams,
				};
				return GetOpenDelegate(encoder);
			}
		}

			#endregion Public Static Methods
	}
}



