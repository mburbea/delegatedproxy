﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using DelegatedProxy.InvocationHandlers;
using Reflective;
using Utilities;
using Utilities.Extensions;

namespace DelegatedProxy
{
	public partial class ProxiedTypeBuilder
	{
		/// <summary>
		/// Updates the generics constraints.
		/// </summary>
		/// <param name="methodGenerics">The generics.</param>
		private static void UpdateGenericsConstraints(Type[] genericArguments, GenericTypeParameterBuilder[] methodGenerics, GenericTypeParameterBuilder[] typeGenerics)
		{
			GenericTypeParameterBuilder[] genericsToUpdate = null;
			for (int i = 0; i < genericArguments.Length; i++)
			{
				genericsToUpdate = genericsToUpdate ?? (genericArguments[0].DeclaringMethod != null ? methodGenerics : typeGenerics);
				genericsToUpdate[i].SetGenericParameterAttributes(genericArguments[i].GenericParameterAttributes);
				// we need to replace the constraints generic arguments for cases like
				// where T:U and T:IComparer<T>
				Type[] typeConstraints = genericArguments[i]
					.GetGenericParameterConstraints()
					.ConvertAll(t => ReplaceGenericArgument(t, methodGenerics, typeGenerics));
				if (typeConstraints.Length > 0)
				{
					Type baseType = typeConstraints.FirstOrDefault(t => !t.IsInterface);
					if (baseType != null)
					{
						genericsToUpdate[i].SetBaseTypeConstraint(baseType);
						genericsToUpdate[i].SetInterfaceConstraints(Array.FindAll(typeConstraints, (t => t != baseType)));
					}
					else
					{
						genericsToUpdate[i].SetInterfaceConstraints(typeConstraints);
					}
				}
			}
		}


		/// <summary>
		/// Replaces the generic arguments from the one from the methodToProxy to those of the overload's
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="methodGenerics">The generics.</param>
		/// <returns></returns>
		internal static Type ReplaceGenericArgument(Type type, GenericTypeParameterBuilder[] methodGenerics, GenericTypeParameterBuilder[] typeGenerics)
		{
			if (!type.ContainsGenericParameters)
			{
				return type; // only happens if T wasn't generic.
			}
			if (type.IsGenericParameter)
			{
				return type.DeclaringMethod == null ?
					typeGenerics[type.GenericParameterPosition] :
				 methodGenerics[type.GenericParameterPosition]; // find the corresponding generic
			}
			if (type.IsByRef)
			{
				return ReplaceGenericArgument(type.GetElementType(), methodGenerics, typeGenerics).MakeByRefType();
			}
			if (type.IsArray)
			{
				int rank = type.GetArrayRank();
				bool isVector = rank == 1 && type.Name[type.Name.Length - 2] == '[';
				type = ReplaceGenericArgument(type.GetElementType(), methodGenerics, typeGenerics);
				return isVector ? type.MakeArrayType() : type.MakeArrayType(rank);
			}
			if (type.IsGenericType)
			{
				var typeDef = type.GetGenericTypeDefinition();
				var args = Array.ConvertAll(type.GetGenericArguments(), t => ReplaceGenericArgument(t, methodGenerics, typeGenerics));
				return typeDef.MakeGenericType(args);
			}
			throw new InvalidOperationException("type can't be a pointer");
		}



		const string FORBIDDEN_CHARS = @"+[]*&,\";
		const char SAFE_CHAR = '_';
		// These methods should not be proxied as they are used by the runtime or for debugging purposes.
		private static readonly MethodInfo[] ReservedMethods = new[]
		{ 
			typeof(object).GetMethod("Finalize",BindingFlags.NonPublic | BindingFlags.Instance), 
			typeof(object).GetMethod("GetHashCode",BindingFlags.Public | BindingFlags.Instance), 
			typeof(object).GetMethod("Equals",BindingFlags.Public | BindingFlags.Instance), 
			typeof(object).GetMethod("ToString",BindingFlags.Public | BindingFlags.Instance) 
		};

		private static readonly GenericTypeParameterBuilder[] EmptyGenerics = new GenericTypeParameterBuilder[0];
		private readonly TypeBuilder typeBuilder;
		private readonly ProxiedMethodBuilder[] proxiedMethods;
		private readonly FieldBuilder callBaseHandlers;
		private readonly GenericTypeParameterBuilder[] genericTypes = EmptyGenerics;

		public readonly Type Type;
		public static IHandlerSelector DefaultSelector = new DefaultHandlerSelector();
		static string SanitizeName(string name)
		{

			return FORBIDDEN_CHARS.Aggregate(name.Split('`')[0], (n, c) => n.Replace(c, SAFE_CHAR));
		}
		private static bool CanMethodBeProxied(MethodInfo method)
		{
			return method.IsVirtual
					&& !method.IsFinal
					&& (method.IsPublic || method.IsFamily || method.IsFamilyOrAssembly)
					&& Array.IndexOf(ReservedMethods, method.GetBaseDefinition()) == -1;
		}

		public ProxiedTypeBuilder(Type type)
		{
			if (type == null) throw new ArgumentNullException("type");
			if (type.IsSealed || type.IsValueType) throw new ArgumentException("Type cannot be sealed");
			if (type.IsMarshalByRef) throw new ArgumentException("Type cannot be marshalByRef");
			
			this.typeBuilder =AssemblyGen.GenModule.DefineType(
				SanitizeName(type.FullName) + "Proxy"+(type.IsGenericType ? ('`'+type.GetGenericArguments().Length.ToString()) : ""),
			       TypeAttributes.AutoClass | TypeAttributes.Public | TypeAttributes.BeforeFieldInit);
			Type[] generics = Type.EmptyTypes;
			if (type.IsGenericTypeDefinition)
			{
				genericTypes = 
						typeBuilder.DefineGenericParameters((generics = type.GetGenericArguments()).ConvertAll(g => "p" + g.Name));
			}
			var baseType = generics.Length > 0 ? type.MakeGenericType(genericTypes) : type;
			if(type.IsInterface)
			{
				typeBuilder.AddInterfaceImplementation(baseType);
			}
			else
			{
				typeBuilder.SetParent(baseType);
			}

			
			

			proxiedMethods = (type.IsInterface ?
				(type.GetInterfaces().Concat(new[]{type})).SelectMany(i=>i.GetMethods(BindingFlags.Instance | BindingFlags.Public)) 
				: type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
				).Where(CanMethodBeProxied).Select((m, i) => new ProxiedMethodBuilder(this, new InternalMethodData(m, i))).ToArray();

			callBaseHandlers = typeBuilder.DefineField("_proxy_handlers", typeof(InvocationHandler[]),
				FieldAttributes.Static | FieldAttributes.Private | FieldAttributes.InitOnly);

			var init = typeBuilder.DefineTypeInitializer();
			var il = init.GetILGenerator()
				.variable<InvocationHandler[]>()
				.ldc_i4_opt(proxiedMethods.Length)
				.newarr<InvocationHandler>()
				.stloc_0()
				// for each method run its static init stuff here.
				.EmitFor(proxiedMethods,(gen,method)=>method.StaticInitializer(gen))
				// after thats done we store these handlers.
				.ldloc_0()
				.stsfld(callBaseHandlers)
				.ret();
			
			var ctor = typeBuilder.DefineConstructor(
				MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public,
				CallingConventions.Standard,
				new []{typeof(InvocationHandler[])}
			);
			Label ifNotNull;
			Label arrayIsRightSize;
			ctor.GetILGenerator()
				.define_label(out ifNotNull)
				.define_label(out arrayIsRightSize)
				.ldarg_0()
				.call(type.IsInterface ? typeof(object).GetConstructor(Type.EmptyTypes) : type.GetConstructor(Type.EmptyTypes))
				.ldarg_1()
				.brtrue_s(ifNotNull)

				.ldstr("handlers")
				.newobj<ArgumentNullException>(typeof(string))
				.throw_()
				
				.mark_label(ifNotNull)
				.ldarg_1()
				.ldlen()
				.conv_i4()
				.ldc_i4_opt(proxiedMethods.Length)
				.beq_s(arrayIsRightSize)

				.ldstr(string.Format(CultureInfo.InvariantCulture,"Handlers needs to be an array of {0} parameters.",proxiedMethods.Length))
				.ldstr("handlers")
				.newobj<ArgumentException>(typeof(string),typeof(string))
				.throw_()

				.mark_label(arrayIsRightSize)
				.EmitFor(proxiedMethods, (gen, method) => method.Initializer(gen,callBaseHandlers,1))
				.ret();
			UpdateGenericsConstraints(generics, null, genericTypes);
			this.Type = typeBuilder.CreateType();
			AssemblyGen.Save();
		}

	}
}
