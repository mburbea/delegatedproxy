﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Microsoft.CSharp.RuntimeBinder;
using Reflective;
using Utilities;
using Utilities.Extensions;
using Binder = Microsoft.CSharp.RuntimeBinder.Binder;
using CSharpArgumentInfoFlags = Microsoft.CSharp.RuntimeBinder.CSharpArgumentInfoFlags;
namespace DelegatedProxy
{
	public class ProxiedMethodBuilder
	{
		private static readonly GenericTypeParameterBuilder[] EmptyGenerics = new GenericTypeParameterBuilder[0];
		private static readonly MethodInfo BinderInvokeMember = typeof (Binder).GetMethod("InvokeMember");
		private static readonly MethodInfo CsharpArgumentCreate = typeof (CSharpArgumentInfo).GetMethod("Create");
		private static readonly FieldInfo TokenField = typeof (GenericToken<>).GetField("Token", BindingFlags.Public | BindingFlags.Static);

		private static readonly ConstructorInfo StrongBoxCtor = typeof (StrongBox<>).GetConstructors()[1];
		private static readonly FieldInfo StrongBoxValue = typeof (StrongBox<>).GetField("Value");

		private Type _baseType;
		private readonly TypeBuilder _typeBuilder;
		private readonly MethodData _methodData;
		private readonly FieldBuilder _delegateField;
		private readonly MethodInfo _invokeMethod;
		private readonly MethodBuilder _overrideMethod;
		private readonly MethodBuilder _callBaseMethod;
		private readonly MethodBuilder _callBaseGenericHelperMethod;
		private FieldInfo _callSiteField;

		internal ProxiedMethodBuilder(TypeBuilder typeBuilder, MethodData methodData)
		{
			this._baseType = typeBuilder.BaseType;
			this._typeBuilder = typeBuilder;
			this._methodData = methodData;
			// declare a field for this thing.
			this._delegateField = _typeBuilder.DefineField("$"+this._methodData.MethodIndex+"$"+this._methodData.Method.Name, this._methodData.DelegateType,
			                                               FieldAttributes.Private);
			this._invokeMethod = this._methodData.DelegateType.GetMethod("Invoke");

			// build up to 3 methods for it.
			this._overrideMethod = BuildOverrideMethod();
			this._callBaseMethod = BuildCallBaseMethod();
			this._callBaseGenericHelperMethod = BuildGenericCallBaseHelper();
		}

		/// <summary>
		/// Replaces the generic arguments from the one from the methodToProxy to those of the overload's
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="generics">The generics.</param>
		/// <returns></returns>
		private static Type ReplaceGenericArgument(Type type, GenericTypeParameterBuilder[] generics)
		{
			if (!type.ContainsGenericParameters)
			{
				return type; // only happens if T wasn't generic.
			}
			if (type.IsGenericParameter)
			{
				return generics[type.GenericParameterPosition]; // find the corresponding generic
			}
			if (type.IsByRef)
			{
				return ReplaceGenericArgument(type.GetElementType(), generics).MakeByRefType();
			}
			if (type.IsArray)
			{
				int rank = type.GetArrayRank();
				bool isVector = rank == 1 && type.Name[type.Name.Length - 2] == '[';
				type = ReplaceGenericArgument(type.GetElementType(), generics);
				return isVector ? type.MakeArrayType() : type.MakeArrayType(rank);
			}
			if (type.IsGenericType)
			{
				var typeDef = type.GetGenericTypeDefinition();
				var args = Array.ConvertAll(type.GetGenericArguments(), t => ReplaceGenericArgument(t, generics));
				return typeDef.MakeGenericType(args);
			}
			throw new InvalidOperationException("type can't be a pointer");
		}

		private void UpdateGenericsConstraints(GenericTypeParameterBuilder[] generics)
		{
			for (int i = 0; i < generics.Length; i++)
			{
				generics[i].SetGenericParameterAttributes(_methodData.GenericArguments[i].GenericParameterAttributes);
				var typeConstraints = _methodData.GenericArguments[i].GetGenericParameterConstraints();
				var type = typeConstraints.FirstOrDefault(t => !t.IsInterface);
				if (type != null)
				{
					generics[i].SetBaseTypeConstraint(type);
					generics[i].SetInterfaceConstraints(typeConstraints.Where(t => t != type).ToArray());
				}
				else
				{
					generics[i].SetInterfaceConstraints(typeConstraints);
				}
			}
		}

		/// <summary>
		/// Builds the proxied method. 
		/// </summary>
		private MethodBuilder BuildOverrideMethod()
		{

			var methodattr = _methodData.Method.Attributes & ~(MethodAttributes.NewSlot | MethodAttributes.Abstract);
			var overrideMethod = _typeBuilder.DefineMethod(this._methodData.Method.Name, methodattr, CallingConventions.HasThis);


			var generics = this._methodData.Method.IsGenericMethod
			               	? overrideMethod.DefineGenericParameters(_methodData.GenericArguments.ConvertAll(p => p.Name))
							: EmptyGenerics;

			Type[] paramTypes = _methodData.Parameters.ConvertAll(p => ReplaceGenericArgument(p.ParameterType, generics));
			Type[][] modOptParams = _methodData.Parameters.ConvertAll(p => p.GetOptionalCustomModifiers());
			Type[][] modReqParams = _methodData.Parameters.ConvertAll(p => p.GetRequiredCustomModifiers());
			
			overrideMethod.SetSignature(ReplaceGenericArgument(_methodData.ReturnParameter.ParameterType, generics), 
				_methodData.ReturnParameter.GetRequiredCustomModifiers(), _methodData.ReturnParameter.GetOptionalCustomModifiers(),
				paramTypes, modReqParams, modOptParams);

			// load the delegate
			var il = overrideMethod.GetILGenerator()
				.ldarg_0()
				.ldfld(_delegateField);
			// get all them parameters.
			var localByRefs = new List<Tuple<LocalBuilder,int,Type>>();
			for (int i = 0; i < paramTypes.Length + 1; i++)
			{
				il.ldarg_opt(i);
				if (i != 0 && paramTypes[i - 1].ContainsGenericParameters)
				{
					if (paramTypes[i - 1].IsByRef)
					{
						LocalBuilder lb;
						var elemtype = paramTypes[i - 1].GetElementType();
						il
							.variable(typeof(StrongBox<>).MakeGenericType(elemtype), out lb)
							.EmitIf(elemtype.IsValueType || elemtype.IsGenericParameter, gen => gen.ldobj(elemtype), gen => gen.ldind_ref())
							.newobj(TypeBuilder.GetConstructor(typeof(StrongBox<>).MakeGenericType(elemtype), StrongBoxCtor))
							.stloc(lb)
							.ldloc(lb);
						localByRefs.Add(Tuple.Create(lb,i,elemtype));
					}
					else if (paramTypes[i - 1].IsValueType || paramTypes[i - 1].IsGenericParameter)
					{
						// we must box a T, or a Nullable<T> but we don't have to box a List<T> or ISomething<T>, as its already an object ref. 
						il.box(paramTypes[i - 1]);
					}
				}
			}
			// Foreach generic load a token.
			LocalBuilder retVal=null;
			il
				.EmitFor(generics, (gen, g) => gen
					.ldsfld(TypeBuilder.GetField(typeof(GenericToken<>).MakeGenericType(g), TokenField))
				)
				.callvirt(_invokeMethod)
				.EmitIf(_methodData.Method.ReturnType.ContainsGenericParameters, gen => gen
					.unbox_any(_methodData.Method.ReturnType)
				)
				.EmitIf(localByRefs.Count > 0, gen => gen
					.EmitIf(_methodData.Method.ReturnType != typeof(void),ilg=>ilg
						.variable(_methodData.Method.ReturnType, out retVal)
						.stloc(retVal)
					)
					.EmitFor(localByRefs, (ilg, tuple) =>
						ilg
							.ldarg_opt(tuple.Item2)
							.ldloc(tuple.Item1)
							.ldfld(TypeBuilder.GetField(typeof(StrongBox<>).MakeGenericType(tuple.Item3), StrongBoxValue))
							.EmitIf(tuple.Item3.IsValueType || tuple.Item3.IsGenericParameter, arg => arg.stobj(tuple.Item3), arg => arg.stind_ref())
							.EmitIf(retVal != null,arg=>arg.ldloc(retVal))
					)
				).ret();
			UpdateMaxStack(overrideMethod);
			UpdateGenericsConstraints(generics);
			//overrideMethod.SetImplementationFlags(MethodImplAttributes.Managed);
			return overrideMethod;
		}
		static ILGenerator EmitNewArgumentInfo(ILGenerator il,LocalBuilder array,int index,CSharpArgumentInfoFlags flags)
		{
			return il
				.ldloc(array)
				.ldc_i4_opt(index)
				.ldc(flags)
				.ldnull()
				.call(CsharpArgumentCreate)
				.stelem_ref();
		}

		static CSharpArgumentInfoFlags DetermineArgumentFlags(ParameterInfo parameter)
		{
			return parameter.ParameterType.ContainsGenericParameters
			       	? CSharpArgumentInfoFlags.None
			       	: CSharpArgumentInfoFlags.UseCompileTimeType |
			       	   	  (parameter.IsOut ? CSharpArgumentInfoFlags.IsOut : 
						  parameter.ParameterType.IsByRef ? CSharpArgumentInfoFlags.IsRef : CSharpArgumentInfoFlags.UseCompileTimeType);
		}
		/// <summary>
		/// Builds the initializer for the method. This is usually just assigning the delegate from the parameter.
		/// </summary>
		/// <param name="generator">The generator.</param>
		/// <param name="delegatesField">The delegates field.</param>
		/// <param name="delegateParam">The delegate param.</param>
		internal void Initializer(ILGenerator generator,FieldInfo delegatesField,int delegateParam)
		{
			Label storeIt;
			generator
				.define_label(out storeIt)
				.ldarg_0()
				.ldarg_opt(delegateParam)
				.ldc_i4_opt(this._methodData.MethodIndex)
				.ldelem_ref()
				.dup()
				.brtrue_s(storeIt)

				.pop() // we had null on the stack
				.ldsfld(delegatesField)
				.ldc_i4_opt(this._methodData.MethodIndex)
				.ldelem_ref()

				.mark_label(storeIt)
				.castclass(this._delegateField.FieldType)

				.stfld(this._delegateField);
		}

		/// <summary>
		/// Builds the static init for the methodinfo. This is building the call base delegate.
		/// </summary>
		/// <returns></returns>
		internal void StaticInitializer(ILGenerator generator)
		{
			if(this._methodData.Method.IsGenericMethod)
			{
				LocalBuilder array;
				generator
					.variable<CSharpArgumentInfo[]>(out array)
					.ldc(_methodData.MethodEncoder.ReturnsVoid ? CSharpBinderFlags.ResultDiscarded : CSharpBinderFlags.None)
					.ldstr("<Proxy>" + this._methodData.Method.Name)
					.ldnull()
					.ldc(_typeBuilder)
					.ldc(this._methodData.CallBaseParameterTypes.Length + 1)
					.newarr<CSharpArgumentInfo>()
					.stloc(array)
					// since we are dispatching to a static method the first arg is the type.
					// and the second arg is the this parameter.
					.EmitBlock(gen=>EmitNewArgumentInfo(gen,array,0,CSharpArgumentInfoFlags.IsStaticType | CSharpArgumentInfoFlags.UseCompileTimeType))
					.EmitBlock(gen=>EmitNewArgumentInfo(gen,array, 1, CSharpArgumentInfoFlags.UseCompileTimeType))
				
					.EmitFor(this._methodData.Parameters,(gen,parameter,index)=>
						EmitNewArgumentInfo(gen,array,index+2,DetermineArgumentFlags(parameter)))
					.EmitFor(_methodData.GenericArguments,(gen,parameter,index)=>
						EmitNewArgumentInfo(gen,array,index+2+this._methodData.Parameters.Length,CSharpArgumentInfoFlags.None))
					.ldloc(array)
					.call(BinderInvokeMember)
					.call(this._callSiteField.FieldType.GetMethod("Create", BindingFlags.Public | BindingFlags.Static))
					.stsfld(this._callSiteField);
			}

			generator
				.ldloc_0()
				.ldc_i4_opt(this._methodData.MethodIndex)
				.ldnull()
				.ldftn(this._methodData.Method.IsAbstract ? BuildBlowUpOnAbstract() : 
				this._methodData.Method.IsGenericMethod ? _callBaseGenericHelperMethod : _callBaseMethod)
				.newobj(this._methodData.DelegateType.GetConstructors()[0])
				.stelem_ref();
		}

		private MethodBuilder BuildGenericCallBaseHelper()
		{
			if(_methodData.Method.IsAbstract || !_methodData.Method.IsGenericMethod) return null;

			var delegateType = ProxyGenerator.GetDelegateForCallSite(this._methodData);
			var delegateInvoke = delegateType.GetMethod("Invoke");
			var invokeHelperType =typeof(CallSite<>).MakeGenericType(delegateType);
			this._callSiteField = _typeBuilder.DefineField("<callsite>"+this._methodData.Method.Name,invokeHelperType,
				FieldAttributes.Private | FieldAttributes.Static);

			var methodattr = (_methodData.Method.Attributes & ~(MethodAttributes.Abstract | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.Final | MethodAttributes.NewSlot))
										 | (MethodAttributes.Static);
			var paramTypes = this._invokeMethod.GetParameters().ConvertAll(p=>p.ParameterType);
			var modOptParams = Enumerable.Range(0, paramTypes.Length).Select(i => i < this._methodData.Parameters.Length ? this._methodData.Parameters[i].GetOptionalCustomModifiers() : Type.EmptyTypes).ToArray();
			var modReqParams = Enumerable.Range(0, paramTypes.Length).Select(i => i < this._methodData.Parameters.Length ? this._methodData.Parameters[i].GetRequiredCustomModifiers() : Type.EmptyTypes).ToArray();

			var helper = _typeBuilder.DefineMethod("<ProxyHelper>" + _methodData.Method.Name,
				methodattr,
				CallingConventions.Standard,
				this._invokeMethod.ReturnType,
				this._methodData.ReturnParameter.GetRequiredCustomModifiers(),
				this._methodData.ReturnParameter.GetOptionalCustomModifiers(),
				paramTypes,
				modReqParams,
				modOptParams
			);
			
			var il =helper.GetILGenerator()

				.ldsfld(_callSiteField)
				.ldfld(invokeHelperType.GetField("Target", BindingFlags.Public | BindingFlags.Instance))
				.ldsfld(_callSiteField)
				.ldc(_typeBuilder)
				.ldargs(Enumerable.Range(0, paramTypes.Length))
				.callvirt(delegateInvoke)
				.EmitIf(_invokeMethod.ReturnType != delegateInvoke.ReturnType, gen =>
					_invokeMethod.ReturnType.IsValueType ?
					gen.unbox_any(_invokeMethod.ReturnType)
					: gen.castclass(_invokeMethod.ReturnType)
				)
				.ret();
			UpdateMaxStack(helper);
			return helper;
		}
		/// <summary>
		/// Builds the blow up on abstract.
		/// </summary>
		/// <returns></returns>
		private MethodBuilder BuildBlowUpOnAbstract()
		{
			var methodattr = (_methodData.Method.Attributes & ~(MethodAttributes.Abstract | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.Final | MethodAttributes.NewSlot))
										 | (MethodAttributes.Static);
			var helper = _typeBuilder.DefineMethod("<OnAbstract>" + _methodData.Method.Name,
						methodattr,
						CallingConventions.Standard,
						this._invokeMethod.ReturnType,
						this._invokeMethod.GetParameters().ConvertAll(p => p.ParameterType)
			);
			helper.GetILGenerator()
				.ldstr("Cannot call an abstract method.")
				.newobj<InvalidOperationException>(typeof(string))
				.throw_();
			return helper;
		}


		/// <summary>
		/// Builds the call base method.
		/// </summary>
		private MethodBuilder BuildCallBaseMethod()
		{
			if (_methodData.Method.IsAbstract) return null;

			var methodattr = (_methodData.Method.Attributes & ~(MethodAttributes.Virtual | MethodAttributes.SpecialName|MethodAttributes.Final | MethodAttributes.NewSlot))
							 | (MethodAttributes.Static);
			var callBase = _typeBuilder.DefineMethod("<Proxy>" + _methodData.Method.Name, methodattr, CallingConventions.Standard);
			var il = callBase.GetILGenerator();
			var generics = this._methodData.Method.IsGenericMethod ? 
				callBase.DefineGenericParameters(this._methodData.GenericArguments.ConvertAll(g=>g.Name))
				: EmptyGenerics;
			Type[] paramTypes = this._methodData.CallBaseParameterTypes.ConvertAll(g => ReplaceGenericArgument(g, generics));
			var modOptParams = Enumerable.Range(0, paramTypes.Length).Select(i => i> 0 && i <= this._methodData.Parameters.Length ? this._methodData.Parameters[i-1].GetOptionalCustomModifiers() : Type.EmptyTypes).ToArray();
			var modReqParams = Enumerable.Range(0, paramTypes.Length).Select(i => i> 0 && i <= this._methodData.Parameters.Length ? this._methodData.Parameters[i-1].GetRequiredCustomModifiers() : Type.EmptyTypes).ToArray();
			callBase.SetSignature(
				ReplaceGenericArgument(this._methodData.ReturnParameter.ParameterType, generics),
				this._methodData.ReturnParameter.GetRequiredCustomModifiers(),
				this._methodData.ReturnParameter.GetOptionalCustomModifiers(),
				paramTypes,
				modReqParams,
				modOptParams
			);

			il
				.EmitFor(paramTypes.Where((p,i)=>i<paramTypes.Length-generics.Length),(gen,p,i)=>gen
					// load each parameter except for the tokens.
					.ldarg_opt(i)
					// if its a byref generic it will be strongboxed and needs to be unboxed to send to the real method.
					.EmitIf(i!=0 && this._methodData.Parameters[i-1].ParameterType.ContainsGenericParameters
					&& this._methodData.Parameters[i-1].ParameterType.IsByRef,ilg=>ilg.ldflda(TypeBuilder.GetField(p,StrongBoxValue)))
				)
				.call(generics.Length > 0 ? this._methodData.Method.MakeGenericMethod(generics) : this._methodData.Method)
				.ret();
				
				// load the this parameter.

			//for (int i = 1; i < parameters.Length-generics.Length; i++)
			//// the tokens are to help type inference. Not actually used.
			//{
			//    il.ldarg_opt(i);
			//    // generic byRefs are boxed.
			//    if (this._methodData.Parameters[i-1].ParameterType.ContainsGenericParameters 
			//        && this._methodData.Parameters[i-1].ParameterType.IsByRef)
			//    {
			//        il.ldflda(TypeBuilder.GetField(parameters[i],StrongBoxValue));
			//    }
			//}
			//il
				
			UpdateGenericsConstraints(generics);
			UpdateMaxStack(callBase);
			return callBase;
		}
#if !SILVERLIGHT
		private static Action<ILGenerator> _updateMaxStacksize;
		public static void UpdateMaxStack(MethodBuilder mb)
		{
			if(_updateMaxStacksize == null)
			{
				Label exit;
				var fieldinfo = typeof(ILGenerator).GetField("m_maxStackSize",BindingFlags.NonPublic | BindingFlags.Instance);
				_updateMaxStacksize = CodeGen.CreateDelegate<Action<ILGenerator>>(gen =>
					gen
					.define_label(out exit)
					.ldarg_1()
					.ldfld(fieldinfo)
					.ldc_i4_8()
					.bge_s(exit)
					.ldarg_1()
					.ldc_i4_8()
					.stfld(fieldinfo)
					.mark_label(exit)
					.ret(),restrictedSkipVisibility:true
				);
			}
			_updateMaxStacksize(mb.GetILGenerator());

		}
#endif
	}
}
