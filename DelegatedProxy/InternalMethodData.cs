﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;
using Utilities;
using Utilities.Extensions;

namespace DelegatedProxy
{
	/// <summary>
	/// MethodData store frequently queried data about a method
	/// as reflection is expensive.
	/// </summary>
	internal class InternalMethodData
	{
		Type _invocationType;

		public readonly bool DeclaringTypeIsGeneric;

		public readonly int MethodIndex;

		public readonly MethodInfo Method;

		public readonly ParameterInfo[] Parameters;

		public readonly Type[][] ParametersModReqs;

		public readonly Type[][] ParametersModOpts;

		public readonly ParameterInfo ReturnParameter;

		public readonly Type[] ReturnModOpts;

		public readonly Type[] ReturnModReqs;

		public readonly Type[] CallBaseParameterTypes;

		public readonly Type[] GenericArguments;

		public readonly SignatureEncoder SignatureEncoder;

		public Type DelegateType;

		public readonly Type[] DelegateGenericArguments;

		public Type InvocationType
		{
			get { return _invocationType ?? (_invocationType = ProxyGenerator.GetInvocationType(this)); }
		}

		private static Type SanitizeType(Type type)
		{
			if (type.ContainsMethodGenericParameters())
			{
				return type.IsByRef ? typeof(StrongTypeBox) : typeof(object);
			}
			else if (type.IsByRef)
			{
				return type.GetElementType();
			}
			return type;
		}


		public Type[] SanitizeTypes()
		{
			if (this.ReturnParameter.ParameterType == typeof(void) && this.GenericArguments.Length== 0 && this.SignatureEncoder.RefValueLocations == 0)
			{
				return CallBaseParameterTypes;
			}
			var encoder = this.SignatureEncoder;
			var sanitizedTypes = new Type[encoder.Arity + encoder.GenericArity + Convert.ToInt32(!encoder.ReturnsVoid)];
			this.CallBaseParameterTypes.Select(SanitizeType).CopyTo(sanitizedTypes, 0);
			this.GenericArguments.Select(g => typeof(GenericToken)).CopyTo(sanitizedTypes, encoder.Arity);
			if (!encoder.ReturnsVoid)
			{
				sanitizedTypes[sanitizedTypes.Length - 1] = this.ReturnParameter.ParameterType.ContainsMethodGenericParameters() ? typeof(object) : this.ReturnParameter.ParameterType;
			}
			return sanitizedTypes;
		}

		public InternalMethodData(MethodInfo method,int methodIndex)
		{
			this.Method = method;
			this.MethodIndex = methodIndex;
			
			this.Parameters = method.GetParameters();
			this.ParametersModReqs = this.Parameters.ConvertAll(p => p.GetRequiredCustomModifiers());
			this.ParametersModOpts = this.Parameters.ConvertAll(p => p.GetOptionalCustomModifiers());

			this.ReturnParameter = method.ReturnParameter;
			this.ReturnModReqs = this.ReturnParameter.GetRequiredCustomModifiers();
			this.ReturnModOpts = this.ReturnParameter.GetOptionalCustomModifiers();

			this.GenericArguments = method.IsGenericMethod ? method.GetGenericArguments() : Type.EmptyTypes;

			this.CallBaseParameterTypes = new Type[1+Parameters.Length];
			this.CallBaseParameterTypes[0] = method.DeclaringType;
			Parameters.Select(p=>p.ParameterType).CopyTo(CallBaseParameterTypes, 1);

			this.SignatureEncoder = this.Encode();
			this.DelegateGenericArguments = this.SanitizeTypes();
			this.DeclaringTypeIsGeneric = this.Method.DeclaringType.IsGenericTypeDefinition;
			if (this.SignatureEncoder.Arity+this.SignatureEncoder.GenericArity > 32)
			{
				throw new ArgumentException("DelegatedProxy only supports instance methods with less than 32 parameters.", "method");
			}
			if (this.ReturnParameter.IsOut || this.ReturnParameter.ParameterType.IsByRef)
			{
				throw new ArgumentException("DelegatedProxy does not support ByRef return types", "method");
			}
			this.DelegateType = ProxyGenerator.GetOpenDelegate(this.SignatureEncoder);
		}

		private  SignatureEncoder Encode()
		{
			var vector = new SimpleBitVector32();
			for (int i = 0; i < this.CallBaseParameterTypes.Length; i++)
			{
				vector[i] = this.CallBaseParameterTypes[i].IsByRef && !this.CallBaseParameterTypes[i].ContainsGenericParameters;
			}
			return new SignatureEncoder
			{
				Arity = (byte)CallBaseParameterTypes.Length, 
				RefValueLocations = vector,
				ReturnsVoid = this.ReturnParameter.ParameterType == typeof(void),
				GenericArity = (byte)GenericArguments.Length,
			};
		}
	}
}
