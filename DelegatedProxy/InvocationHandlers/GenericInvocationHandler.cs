﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Reflective;
using Utilities;
using Utilities.Extensions;

namespace DelegatedProxy.InvocationHandlers
{
	public class GenericInvocationHandler<T> : InvocationHandler<T> where T : class
	{
		/// <summary>
		/// Holds the inner state for a invocationHandler
		/// </summary>
		public class CachePage<TDelegate>
			where TDelegate : class
		{
			public GenericInvocationHandler<T> Handler;
			public TDelegate Delegate;
			public CachePage(GenericInvocationHandler<T> handler)
			{
				this.Handler = handler;
			}
		}
		// ReSharper disable InconsistentNaming
		const int HandlerCacheSize = 10;
		protected static class MemberInfoCache
		{
			const BindingFlags Flags = BindingFlags.Instance | BindingFlags.Public;
			public static readonly MethodInfo get_Update = typeof(GenericInvocationHandler<T>).GetMethod("get_Update", Flags);
			public static readonly MethodInfo get_Match = typeof(GenericInvocationHandler<T>).GetMethod("get_Match", Flags);
			public static readonly MethodInfo set_Match = typeof(GenericInvocationHandler<T>).GetMethod("set_Match", Flags);
			public static readonly MethodInfo get_Handlers = typeof(GenericInvocationHandler<T>).GetMethod("get_Handlers", Flags);
			public static readonly MethodInfo AddHandler = typeof(GenericInvocationHandler<T>).GetMethod("AddHandler", Flags);
			public static readonly MethodInfo Promote = typeof(GenericInvocationHandler<T>).GetMethod("Promote", Flags);
			public static readonly MethodInfo CreateDelegateForTokens = typeof(GenericInvocationHandler<T>).GetMethod("CreateDelegateForTokens", Flags);
			public static readonly FieldInfo Target = typeof(GenericInvocationHandler<T>).GetField("Target", Flags);
		}
		// ReSharper restore InconsistentNaming
		private static readonly int FirstGenericToken;
		private static readonly int GenericArity;
		private static readonly bool[] PotentiallyBoxedArguments;
		private static DynamicMethod UpdateDynMethod;

		private readonly T _update;
		private readonly MethodInfo _method;
		private readonly Type _openDelegateType;
		private T[] _handlers;
		static GenericInvocationHandler()
		{
			FirstGenericToken = Array.IndexOf(SignatureCache.RawParameters, typeof(GenericToken), 2);
			GenericArity = SignatureCache.RawParameters.Length - FirstGenericToken;
			PotentiallyBoxedArguments = SignatureCache.RawParameters.ConvertAll(argument => 
				argument == typeof(StrongTypeBox) || argument == typeof(object));
		}

		[Obsolete("Do not call this method directly.", true)]
		public T CreateDelegateForTokens(GenericToken[] tokens)
		{
			MethodInfo method = _method.MakeGenericMethod(tokens.ConvertAll(g => g.Type));
			var parameters = method.GetParameters().ConvertAll(p=>p.ParameterType);
			Type[] generics;
			if (method.ReturnType == typeof(void))
			{
				generics = !parameters.Any(t => t.IsByRef) ?
					parameters :
					parameters.ConvertAll(t => t.IsByRef ? t.GetElementType() : t);
			}
			else
			{
				generics = new Type[parameters.Length + 1];
				parameters.Select(t=>t.IsByRef ? t.GetElementType() : t).CopyTo(generics, 0);
				generics[parameters.Length] = method.ReturnType;
			}
			var funcType = _openDelegateType.MakeGenericType(generics);
			var cacheType = typeof(CachePage<>).MakeGenericType(typeof(T), funcType);
			var cachePage = Activator.CreateInstance(cacheType, this);
			var handlerField = cacheType.GetField("Handler"); 
			var delegateField = cacheType.GetField("Delegate");
			var needUnboxing = new[] { default(Type) }.Concat(generics).Select((p, i) =>
				PotentiallyBoxedArguments[i] && (p != SignatureCache.RawParameters[i] && p !=SignatureCache.RawParameters[i].MakeByRefType()) ? p : null).ToArray();
			Label callUpdate, quitAndReturnDefault, alreadyBuilt;
			LocalBuilder func;
			Action<ILGenerator> genfunc = generator => generator
				.define_label(out callUpdate)
				.define_label(out quitAndReturnDefault)
				.define_label(out alreadyBuilt)
				.variable(funcType,out func)
				.EmitFor(tokens, (gen, token, i) => gen
						.ldarg_opt(FirstGenericToken + i)
						.isinst(token.GetType())
						.brfalse_s(callUpdate)
				)
				.ldarg_0()
				.ldfld(handlerField)
				.ldc_i4_1()
				.callvirt(MemberInfoCache.set_Match)
				.ldarg_0()
				.ldfld(delegateField)
				.dup()
				.brtrue_s(alreadyBuilt)
				.pop()
				.ldarg_0()
				.ldnull()
				.ldftn(method)
				.newobj(funcType.GetConstructors()[0])
				.dup()
				.stloc(func)
				.stfld(delegateField)
				.ldloc(func)
				.mark_label(alreadyBuilt)
				.EmitFor(needUnboxing.Take(FirstGenericToken).Skip(1), (gen, unboxType, i) => gen
					.ldarg_opt(i + 1)
					.EmitIf(unboxType != null, ilg => parameters[i].IsByRef ? 
						ilg.castclass(typeof(StrongTypeBox<>).MakeGenericType(unboxType))
							.ldflda(typeof(StrongTypeBox<>).MakeGenericType(unboxType).GetField("Value"))
						: ilg.unbox_any(unboxType))
				)
				.callvirt(funcType.GetMethod("Invoke"))
				.EmitIf(method.ReturnType != SignatureCache.ReturnType && method.ReturnType.IsValueType, ilg => ilg.box(method.ReturnType))
				.ret()

				.mark_label(callUpdate)
				.ldarg_0()
				.ldfld(handlerField)
				.callvirt(MemberInfoCache.get_Match)
				.brfalse_s(quitAndReturnDefault)
				.ldarg_0()
				.ldfld(handlerField)
				.ldc_i4_0()
				.callvirt(MemberInfoCache.set_Match)
				.ldarg_0()
				.ldfld(handlerField)
				.callvirt(MemberInfoCache.get_Update)
				.ldargs(Enumerable.Range(0, SignatureCache.RawParameters.Length).Skip(1))
				.callvirt(SignatureCache.InvokeMethod)
				.ret()
				.mark_label(quitAndReturnDefault)
				.ld_default(SignatureCache.ReturnType)
				.ret();
			return this.CreateDelegate(genfunc, cachePage,"TargetFor "+method.ToString().Replace("<Proxy>","").Replace("[","$").Replace("]","$"));
		}
		//
		//public T CreateDelegateForTokens(GenericToken[] tokens)
		//{
		//    MethodInfo method = _method.MakeGenericMethod(tokens.ConvertAll(g => g.Type));
		//    var parameters = method.GetParameters().ConvertAll(p => p.ParameterType);
		//    //Type[] generics;
		//    //if (method.ReturnType == typeof(void))
		//    //{
		//    //    generics = parameters;
		//    //}
		//    //else
		//    //{
		//    //    generics = new Type[parameters.Length + 1];
		//    //    parameters.CopyTo(generics, 0);
		//    //    generics[parameters.Length] = method.ReturnType;
		//    //}
		//    //var funcType = _openDelegateType.MakeGenericType(generics);
		//    //var cacheType = typeof(CachePage<>).MakeGenericType(typeof(T), funcType);
		//    //var cachePage = Activator.CreateInstance(cacheType
		//    //    , this);
		//    //var handlerField = cacheType.GetField("Handler");
		//    //var delegateField = cacheType.GetField("Delegate");

		//    var needUnboxing = new[] { default(Type) }.Concat(parameters).Select((p, i) =>
		//        PotentiallyBoxedArguments[i] && p != SignatureCache.RawParameters[i] ? p : null).ToArray();
		//    Label quit, callUpdate, quitAndReturnDefault, alreadyBuilt;
			
		//    Action<ILGenerator> genfunc = generator => generator
		//        .define_label(out callUpdate)
		//        .define_label(out quitAndReturnDefault)
		//        .define_label(out quit)
		//        .EmitFor(tokens, (gen, token, i) => gen
		//                .ldarg_opt(FirstGenericToken + i)
		//                .isinst(token.GetType())
		//                .brfalse_s(callUpdate)
		//        )
		//        .ldarg_0()
		//        .ldc_i4_1()
		//        .callvirt(MemberInfoCache.set_Match)
		//        .EmitFor(needUnboxing.Take(FirstGenericToken).Skip(1), (gen, unboxType, i) => gen
		//            .ldarg_opt(i + 1)
		//            .EmitIf(unboxType != null, ilg => ilg.unbox_any(unboxType))
		//        )
		//        .call(method)
		//        .EmitIf(method.ReturnType != SignatureCache.ReturnType && method.ReturnType.IsValueType, ilg => ilg.box(method.ReturnType))
		//        .ret()

		//        .mark_label(callUpdate)
		//        .ldarg_0()
		//        .callvirt(MemberInfoCache.get_Match)
		//        .brfalse_s(quitAndReturnDefault)
		//        .ldarg_0()
		//        .ldc_i4_0()
		//        .callvirt(MemberInfoCache.set_Match)
		//        .ldarg_0()
		//        .callvirt(MemberInfoCache.get_Update)
		//        .ldargs(Enumerable.Range(0, SignatureCache.RawParameters.Length).Skip(1))
		//        .callvirt(SignatureCache.InvokeMethod)
		//        .ret()
		//        .mark_label(quitAndReturnDefault)
		//        .ld_default(SignatureCache.ReturnType)
		//        .mark_label(quit)
		//        .ret();
		//    return this.CreateDelegate(genfunc, this);
		//}


		[Obsolete("Do not call this method directly.", true)]
		public T[] Handlers
		{
			get { return _handlers; }
		}

		[Obsolete("Do not call this method directly.", true)]
		public T Update
		{
			get { return _update; }
		}

		[Obsolete("Do not call this method directly.", true)]
		public bool Match { get; set; }

		public GenericInvocationHandler(Type type,string methodName) : this(type.GetMethod(methodName, BindingFlags.Public | BindingFlags.Static))
		{
		}
		public GenericInvocationHandler(MethodInfo method)
		{
			this._openDelegateType = ProxyGenerator.GetOpenDelegate(method);
			if (UpdateDynMethod == null)
			{
				CreateUpdate(method, ref UpdateDynMethod);
			}
			this._update = UpdateDynMethod.CreateDelegate(typeof(T), this) as T;
			this._method = method;
			this.Target = _update;
		}

		private static void CreateUpdate(MethodInfo method,ref DynamicMethod dynMethod)
		{

			if (dynMethod != null) return;
			Type[] types = (Type[])SignatureCache.RawParameters.Clone();
			types[0] = typeof(GenericInvocationHandler<T>);
			dynMethod = new DynamicMethod("", SignatureCache.ReturnType, types, true);
			Label test, loopstart, increment, cacheMiss;
			LocalBuilder handlers, index, func, array, retVal = null;
			bool canReturn = SignatureCache.ReturnType != typeof(void);

			Action<ILGenerator> genfunc = generator => generator
				.define_label(out loopstart)
				.define_label(out increment)
				.define_label(out test)
				.define_label(out cacheMiss)
				.variable<int>(out index)
				.variable<T[]>(out handlers)
				.variable<GenericToken[]>(out array)
				.variable<T>(out func)
				.EmitIf(canReturn, gen => gen.variable(SignatureCache.ReturnType, out retVal))

				.ldarg_0()
				.callvirt(MemberInfoCache.get_Handlers)
				.stloc(handlers)
				.ldloc(handlers)
				.brfalse_s(cacheMiss)
				.ldc_i4_0()
				.stloc(index)
				.br_s(test)
				.mark_label(loopstart)
					.ldloc(handlers)
					.ldloc(index)
					.ldelem_ref()
					.stloc(func)
					.ldarg_0()
					.ldfld(MemberInfoCache.Target)
					.ldloc(func)
					.beq_s(increment)
						.ldarg_0()
						.ldloc(func)
						.stfld(MemberInfoCache.Target)
						.ldloc(func)
						.ldargs(Enumerable.Range(0, SignatureCache.RawParameters.Length).Skip(1))
						.callvirt(SignatureCache.InvokeMethod)
						.EmitIf(canReturn, gen => gen.stloc(retVal))
						.ldarg_0()
						.callvirt(MemberInfoCache.get_Match)
						.brfalse_s(increment)
						.ldarg_0()
						.ldloc(index)
						.callvirt(MemberInfoCache.Promote)
						.EmitIf(canReturn, gen => gen.ldloc(retVal))
						.ret()

				.mark_label(increment) // i++
				.ldloc(index)
				.ldc_i4_1()
				.add()
				.stloc(index)

				.mark_label(test)	// i < handlers.Length 
				.ldloc(index)
				.ldloc(handlers)
				.ldlen()
				.conv_i4()
				.blt_s(loopstart)


				.mark_label(cacheMiss)
				.ldc_i4_opt(GenericArity)
				.newarr<GenericToken>()
				.stloc(array)
				.EmitFor(Enumerable.Range(FirstGenericToken, GenericArity), (gen, arg, ix) =>
					gen
						.ldloc(array)
						.ldc_i4_opt(ix)
						.ldarg_opt(arg)
						.stelem_ref()
				).ldarg_0()
				.dup()
				.ldloc(array)
				.callvirt(MemberInfoCache.CreateDelegateForTokens)
				.dup()
				.stloc(func)
				.stfld(MemberInfoCache.Target)
				.ldarg_0()
				.ldloc(func)
				.callvirt(MemberInfoCache.AddHandler)
				.ldloc(func)
				.ldargs(Enumerable.Range(0, SignatureCache.RawParameters.Length).Skip(1))
				.callvirt(SignatureCache.InvokeMethod)
				.ret();
			genfunc(UpdateDynMethod.GetILGenerator());
			CodeGen.BuildGlobalMethodForGenerator(typeof(T), genfunc, "UpdateDelegate"+typeof(T), SignatureCache.ReturnType, types);
			CodeGen.UpdateMaxStack(UpdateDynMethod.GetILGenerator());
		}

		[Obsolete("Do not call this method directly.", true)]
		public void AddHandler(T handler)
		{
			T[] funcs = this._handlers;
			if (funcs == null)
			{
				this._handlers = new[] { handler };
			}
			else
			{
				var results = new T[Math.Min(HandlerCacheSize, funcs.Length + 1)];
				Array.Copy(funcs, 0, results, 1, Math.Min(funcs.Length, HandlerCacheSize - 1));
				results[0] = handler;
				this._handlers = results;
			}
		}
		[Obsolete("Do not call this method directly.", true)]
		public void Promote(int index)
		{
			if (index > 1)
			{
				T[] funcs = this._handlers;
				T func = funcs[index];
				funcs[index] = funcs[index - 1];
				funcs[index - 1] = funcs[index - 2];
				funcs[index - 2] = func;

			}
		}
	}
}
