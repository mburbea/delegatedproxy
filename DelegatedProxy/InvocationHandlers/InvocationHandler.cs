﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using Utilities;
using Utilities.Extensions;

namespace DelegatedProxy.InvocationHandlers
{
// ReSharper disable InconsistentNaming
	public interface InvocationHandler
// ReSharper restore InconsistentNaming
	{
	}

	public class InvocationHandler<T> : InvocationHandler
		where T : class
	{
		public T Target;
		public InvocationHandler()
		{
		}

		public InvocationHandler(T target)
		{
			this.Target = target;
		}

		public InvocationHandler(Expression expression)
		{
			this.Target = CreateDelegate(expression);
		}

		public InvocationHandler(Action<ILGenerator> genfunc)
		{
			this.Target = CreateDelegate(genfunc);
		}

		public InvocationHandler(Action<ILGenerator> genfunc,object target)
		{
			this.Target = CreateDelegate(genfunc,target);
		}



		protected T CreateDelegate(Action<ILGenerator> genFunc, object target = null, string name = "")
		{
			target = target ?? this;
			var types = (Type[])SignatureCache.RawParameters.Clone();
			types[0] = target == null ? typeof(object)
				 : target.GetType().IsValueType ? target.GetType().IsEnum ? typeof(Enum)
				 : typeof(ValueType) : target.GetType();
			var method = new DynamicMethod("", SignatureCache.ReturnType, types, true);
			genFunc(method.GetILGenerator());
			CodeGen.BuildGlobalMethodForGenerator(typeof(T), genFunc, name, SignatureCache.ReturnType,
				types);
			CodeGen.UpdateMaxStack(method.GetILGenerator());
			return method.CreateDelegate(typeof(T), target) as T;
		}

		protected T CreateDelegate(Expression expression)
		{
			return Expression.Lambda<T>(expression, ParameterExpressions).Compile();
		}

		protected ReadOnlyCollection<ParameterExpression> ParameterExpressions
		{
			get { return SignatureCache.ParameterExpressions; }
		}

		protected ReadOnlyCollection<Type> ParameterTypes
		{
			get { return SignatureCache.ParameterTypes; }
		}

		protected Type ReturnType
		{
			get { return SignatureCache.ReturnType; }
		}

		public static class SignatureCache
		{
			public static readonly MethodInfo InvokeMethod;
			public static readonly Type[] RawParameters;
			public static readonly Type ReturnType;

			public static readonly ReadOnlyCollection<Type> ParameterTypes;
			public static readonly ReadOnlyCollection<ParameterExpression> ParameterExpressions;

			static SignatureCache()
			{
				InvokeMethod = typeof (T).GetMethod("Invoke");
				var paraminfos = InvokeMethod.GetParameters();
				ParameterExpressions =new ReadOnlyCollection<ParameterExpression>(
					paraminfos.ConvertAll(p => Expression.Parameter(p.ParameterType))
				);

				RawParameters = new Type[paraminfos.Length + 1];
				RawParameters[0] = typeof (InvocationHandler<T>);
				paraminfos.Select(p => p.ParameterType).CopyTo(RawParameters, 1);
				ParameterTypes = new ReadOnlyCollection<Type>(RawParameters);

				ReturnType = InvokeMethod.ReturnType;
			}

		}
	}
}
