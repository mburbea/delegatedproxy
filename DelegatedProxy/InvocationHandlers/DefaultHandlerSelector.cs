﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DelegatedProxy.InvocationHandlers
{
	internal class DefaultHandlerSelector : IHandlerSelector
	{
		public InvocationHandler<T> Select<T>(MethodData methodData, InvocationHandler<T> baseMethodHandler)
			where T : class
		{
			return methodData.MethodToProxy.IsGenericMethod ?
				new InvocationHandler<T>() :
			    new GenericInvocationHandler<T>(methodData.CallBaseMethod);
		}
	}
}
