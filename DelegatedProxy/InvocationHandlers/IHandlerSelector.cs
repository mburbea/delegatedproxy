﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DelegatedProxy.InvocationHandlers
{
	public interface IHandlerSelector
	{
		InvocationHandler<T> Select<T>(MethodData methodData, InvocationHandler<T> baseMethodHandler)
			where T:class;
	}
}
