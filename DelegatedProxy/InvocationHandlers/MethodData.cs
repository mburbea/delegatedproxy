﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DelegatedProxy.InvocationHandlers
{
	public sealed class MethodData
	{
		public MethodInfo MethodToProxy {  get; private set; }
		public MethodInfo CallBaseMethod { get; private set; }
		public int MethodIndex { get; private set; }

		public MethodData(MethodInfo methodToProxy, MethodInfo callBaseMethod, int methodIndex)
		{
			this.MethodToProxy = methodToProxy;
			this.CallBaseMethod = callBaseMethod;
			this.MethodIndex = methodIndex;
		}
	}
}
