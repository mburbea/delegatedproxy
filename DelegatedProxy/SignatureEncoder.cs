﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace DelegatedProxy
{
	[StructLayout(LayoutKind.Explicit)]
	public struct SignatureEncoder : IEquatable<SignatureEncoder>
	{
		#region Public Variables
		[FieldOffset(0)]
		public long EncodedValue;
		[FieldOffset(0)]
		public byte Arity;
		[FieldOffset(1)]
		public bool ReturnsVoid;
		[FieldOffset(2)]
		public byte GenericArity;
		[FieldOffset(4)]
		public SimpleBitVector32 RefValueLocations;
		#endregion Public Variables

		#region Public Methods

		/// <summary>
		/// Determines whether the specified <see cref="SignatureEncoder"/> is equal to this instance.
		/// </summary>
		/// <param name="other">The other.</param>
		/// <returns>
		/// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
		/// </returns>
		public bool Equals(SignatureEncoder other)
		{
			return other.EncodedValue == this.EncodedValue;
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		/// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public override bool Equals(object obj)
		{
			return obj != null && obj is SignatureEncoder && this.Equals((SignatureEncoder)obj);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode()
		{
			return this.EncodedValue.GetHashCode();
		}
		#endregion Public Methods

		/// <summary>
		/// Implements the operator ==.
		/// </summary>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static bool operator ==(SignatureEncoder left, SignatureEncoder right)
		{
			return left.EncodedValue == right.EncodedValue;
		}

		/// <summary>
		/// Implements the operator !=.
		/// </summary>
		/// <param name="left">The left.</param>
		/// <param name="right">The right.</param>
		/// <returns>
		/// The result of the operator.
		/// </returns>
		public static bool operator !=(SignatureEncoder left, SignatureEncoder right)
		{
			return left.EncodedValue != right.EncodedValue;
		}
	}
}
