﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatedProxy
{
	public abstract class StrongTypeBox
	{
	}
	public sealed class StrongTypeBox<T> : StrongTypeBox
	{
		public StrongTypeBox()
		{
		}
		public StrongTypeBox(T value)
		{
			this.Value = value; 
		}
		public T Value; 
	}
}
