﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Security;
using System.Text;

namespace DelegatedProxy
{
	public static class AssemblyGen
	{
		static readonly AssemblyBuilder AssemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(
			new AssemblyName("ProxyAssembly"),
			AssemblyBuilderAccess.RunAndSave
		);
		public static readonly ModuleBuilder GenModule = AssemblyBuilder.DefineDynamicModule(
			AssemblyBuilder.GetName().Name,
			AssemblyBuilder.GetName().Name+".dll",
			true
		);

		static AssemblyGen()
		{
			//var securityTransparentAttribute = new CustomAttributeBuilder(typeof(SecurityTransparentAttribute).GetConstructor(Type.EmptyTypes), new object[0]);
			//var allowPartiallyTrustedCallers = new CustomAttributeBuilder(typeof(AllowPartiallyTrustedCallersAttribute).GetConstructor(Type.EmptyTypes), new object[0]);
			//var securityRules = new CustomAttributeBuilder(typeof(SecurityRulesAttribute).GetConstructor(new[] { typeof(SecurityRuleSet) }),
			//	new object[] { SecurityRuleSet.Level2 },
			//	new PropertyInfo[] { typeof(SecurityRuleSet).GetProperty("SkipVerificationInFullTrust") },
			//	new object[] { true });

			//AssemblyBuilder.SetCustomAttribute(securityTransparentAttribute);
			//AssemblyBuilder.SetCustomAttribute(allowPartiallyTrustedCallers);
			//AssemblyBuilder.SetCustomAttribute(securityRules);

		}

		[Conditional("DEBUG")]
		public static void Save()
		{
			AssemblyBuilder.Save(AssemblyBuilder.GetName().Name+".dll");
		}

	}
}
